import random
from pprint import pprint

class Point:
	# just to make this cleaner
	def __init__(self, x, y, usedBattery, path=[]):
		self.x = x
		self.y = y
		self.usedBattery = usedBattery
		self.path = path

def findPath(chargingStations):
	# chargingStations should be an NxN array
	n = len(chargingStations)
	start = Point(0, 0, 0)
	visitedMinUsedBattery = [[None for x in xrange(n)] for x in xrange(n)]
	q = [start]
	foundPath = None
	while len(q) > 0 and foundPath is None:
		point = q.pop(0)  # deque
		if point.x == n - 1 and point.y == n - 1:
			foundPath = point.path + [(point.x, point.y)]
		# reset at charging station
		if chargingStations[point.x][point.y] == 1:
			point.usedBattery = 0
		if ((visitedMinUsedBattery[point.x][point.y] is None or
				visitedMinUsedBattery[point.x][point.y] > point.usedBattery) and point.usedBattery < 10):
			# move right
			if point.x + 1 < n:
				nextPath = point.path + [(point.x, point.y)]
				q.append(Point(point.x + 1, point.y, point.usedBattery + 1, nextPath))
			# move down
			if point.y + 1 < n:
				nextPath = point.path + [(point.x, point.y)]
				q.append(Point(point.x, point.y + 1, point.usedBattery + 1, nextPath))
			# move left
			if point.x - 1 > 0:
				nextPath = point.path + [(point.x, point.y)]
				q.append(Point(point.x - 1, point.y, point.usedBattery, nextPath))
			# move up
			if point.y - 1 > 0:
				nextPath = point.path + [(point.x, point.y)]
				q.append(Point(point.x, point.y - 1, point.usedBattery, nextPath))
			visitedMinUsedBattery[point.x][point.y] = point.usedBattery

	if foundPath is not None:
		print foundPath
	else:
		print "No path found"


def getInt():
	x = random.randint(1, 30)
	if x != 1:
		return 0
	else:
		return 1
n = 25
chargingStations = [[getInt() for x in xrange(n)] for x in xrange(n)]
pprint(chargingStations)
findPath(chargingStations)