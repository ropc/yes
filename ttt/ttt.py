class Board(object):
	"""tic tac toe board"""
	def __getitem__(self, key):
		return self.board[key]

	def __setitem__(self, key, value):
		self.board[key] = value

	def __eq__(self, obj):
		return self[0] == obj[0] \
			and self[1] == obj[1] \
			and self[2] == obj[2]

	def __init__(self):
		self.board = [[None for y in range(3)] for x in range(3)]

	def __str__(self):
		string = ''
		# instead of self.board, print out the transpose
		# to make it much more intuitive to work with
		for i, row in enumerate(list(zip(*self.board))):
			for j, position in enumerate(row):
				if position:
					string += ' ' + position + ' '
				else:
					string += '   '
				if j < 2:
					string += '|'

			if i < 2:
				string += '\n-----------\n'
			else:
				string += '\n'

		return string

	def copy(self):
		board = Board()
		for i, row in enumerate(self.board):
			board[i] = row.copy()

		return board

	def score(self, isx, *args):
		if not isinstance(isx, bool):
			raise TypeError
		score = 0

		# checks rows and collumns until a winner is found
		for l in [self.board, list(zip(*self.board))]:
			for row in l:
				if row[0] == row[1] and row[1] == row[2]:
					if row[0] == 'O':
						score = -1
					elif row[0] == 'X':
						score = 1
					else:
						continue
					break

		# checks the two diagonals if neccessary
		if score == 0 and self[1][1]:
			for i in range(2):
				a = 0
				if i == 1:
					a = 2

				if self[0][0+a] == self[1][1] and self[1][1] == self[2][2-a]:
					if row[0] == 'O':
						score = -1
					elif row[0] == 'X':
						score = 1
					else:
						continue
					break

		if not isx:
			score *= -1
		if len(list(args)) > 0:
			score *= list(args)[0]
		return score

	def listOptions(self):
		"""
		returns a list of tuples with positions
		(x, y) that are not taken and valid selections
		"""
		l = []
		for i, row in enumerate(self.board):
			for j, position in enumerate(row):
				if position is None:
					# print("adding position {:d}, {:d}".format(i,j))
					l.append((i, j))
		# print("options length: {:d}".format(len(l)))
		return l

	def place(self, isx, x, y):
		if not isinstance(isx, bool):
			raise TypeError

		if self[x][y] is None:
			mark = 'X'
			if not isx:
				mark = 'O'
			self[x][y] = mark
		else:
			print('cannot place on {:d},{:d}'.format(x,y))

class Player(object):
	"""Players place their options on a board"""
	def __init__(self, isx):
		if not isinstance(isx, bool):
			raise TypeError
		self.isx = isx

	def place(self, board, x, y):
		if not isinstance(board, Board):
			raise TypeError

		if board[x][y] is None:
			mark = 'X'
			if not self.isx:
				mark = 'O'
			board[x][y] = mark
		else:
			print('cannot place on {:d},{:d}'.format(x,y))

class Bot(Player):
	"""A bot is a player after all"""
	def __init__(self, isx):
		super().__init__(isx)

	def chooseBest(self, board):
		s = []
		# b = board.copy()
		# position = self.minimax(board, s)
		# self.place(board, position[0], position[1])
		val = self.minimax(board, [])
		print("best op is", val)
		self.place(board, val[1][0], val[1][1])

	def minimax(self, board, stack):
		mustpop = 0
		best = None
		bop = None

		oplist = board.listOptions()
		if len(oplist) > 0:
			mustpop = 1
			stack.append(board)
		else:
			best = board.score(self.isx)
		# print(board)

		player = self.isx
		if len(stack) % 2 == 0:
			player = not self.isx
		
		for op in oplist:
			b = board.copy()
			
			# self.place(b, op[0], op[1])
			b.place(player, op[0], op[1])
			val = self.minimax(b, stack)
			if isinstance(val, list):
				score = val[0]
			else:
				score = val

			if best is None:
				best = score
				bop = op
			else:
				foo = max
				if player != self.isx:
					foo = min

				if best != foo(best, score):
					best = score
					bop = op
				
		if mustpop == 1:
			# print("best at {:} {:d}".format(len(stack), best))
			stack.pop()
		return [best, bop]

	def memodfs(self, board, stack):
		memo = {}
		def dfs(self, board, stack):
			mustpop = 0
			oplist = board.listOptions()
			if len(oplist) > 0:
				mustpop = 1
				stack.append(board)

			print(board)
			for op in oplist:
				b = board.copy()
				player = self.isx

				if len(stack) % 2 == 0:
					player = not self.isx

				# self.place(b, op[0], op[1])
				b.place(player, op[0], op[1])
				self.dfs(b, stack)

			if mustpop == 1:
				stack.pop()

		self.memodfs.dfs(board, stack)

b = Board()
x = Player(True)
o = Player(False)
bot = Bot(False)

# for i in range(3):
# 	for j in range(3):
# 		if (i + j*3) % 2 == 0:
# 			player = x
# 		else:
# 			player = o
# 		player.place(b, i, j)
print(b)
while b.score(True) == 0 and len(b.listOptions()) > 0:
	op = input("Choose a location: ").split(", ")
	if len(op) != 2:
		print("not valid")
		continue
	op = [int(x) for x in op]

	x.place(b, op[0], op[1])
	bot.chooseBest(b)
	# bot.dfs(b)
	print(b)
	
			
# x.place(b, 0, 2)
# o.place(b, 0, 1)
# # bot.chooseBest(b)
# x.place(b, 1, 1)
# o.place(b, 1, 2)
# # bot.chooseBest(b)
# x.place(b, 2, 2)
# o.place(b, 0, 0)
# x.place(b, 2, 1)
# # a = bot.dfs(b, [])
# bot.chooseBest(b)
# # print(a)
# # bot.chooseBest(b)
# # x.place(b, 0, 0)
# # bot.chooseBest(b)
# # copy = b.copy()
# print(b)
# # print(copy)
# # print(b == copy)
# print(b.score(True))
