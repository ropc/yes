import java.util.*;

public class SpaceSearchState implements SearchState {
    public static final String ADD_CHARACTER_PREFIX = "add character ";
    public static final String ADD_WORD_PREFIX = "add word ";

    final List<String> words;
    final String currentWord;
    final String unknownCharacters;

    private SpaceSearchState(List<String> words, String currentWord, String unknownCharacters) {
        this.words = words;
        this.currentWord = currentWord;
        this.unknownCharacters = unknownCharacters;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (obj == this)
            return true;
        if (!(obj instanceof SpaceSearchState))
            return false;
        SpaceSearchState other = (SpaceSearchState)obj;
        return (this.currentWord.equals(other.currentWord) &&
                this.unknownCharacters.equals(other.unknownCharacters));
    }

    @Override
    public int hashCode() {
        return (currentWord.hashCode() ^
                unknownCharacters.hashCode());
    }

    public static class Builder extends SearchState.Builder {
        @Override
        public SearchState makeInitialState(String characters) {
            characters = LangUtil.join(Arrays.asList(LangUtil.getWords(characters)), "");
            return new SpaceSearchState(new LinkedList<String>(), "", characters);
        }
    }

    @Override
    public boolean isGoal() {
        boolean isGoal = (currentWord.length() == 0 && unknownCharacters.length() == 0);
        if (isGoal)
            System.out.println(words);

        return isGoal;
    }

    @Override
    public Collection<String> getApplicableActions() {
        List<String> actions = new ArrayList<>();
        if (unknownCharacters.length() > 0)
            actions.add(ADD_CHARACTER_PREFIX + unknownCharacters.substring(0, 1));
        if (currentWord.length() > 0)
            actions.add(ADD_WORD_PREFIX + currentWord);
        return actions;
    }

    @Override
    public double getActionCost(String action) {
        if (action.equals(ADD_WORD_PREFIX + currentWord))
            return UnigramModel.getInstance().cost(currentWord);
        else
            return 0;
    }

    @Override
    public SearchState applyAction(String action) {
        if (unknownCharacters.length() > 0 && action.equals(ADD_CHARACTER_PREFIX + unknownCharacters.substring(0, 1))) {
            String newWord = currentWord + unknownCharacters.substring(0,1);
            String newUnknownCharacters = unknownCharacters.substring(1);
            return new SpaceSearchState(words, newWord, newUnknownCharacters);
        } else if (action.equals(ADD_WORD_PREFIX + currentWord)) {
            List<String> wordsCopy = new ArrayList<>(words);
            wordsCopy.add(currentWord);
            return new SpaceSearchState(wordsCopy, "", unknownCharacters);
        }
        return null;
    }
}
