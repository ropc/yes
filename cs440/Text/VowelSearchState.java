import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class VowelSearchState implements SearchState {
    public final String ADD_WORD_PREFIX = "add word ";

    final List<String> words;
    final String prevWord;
    final List<String> unknownWords;

    private VowelSearchState(List<String> words, String prevWord, List<String> unknownWords) {
        this.words = words;
        this.prevWord = prevWord;
        this.unknownWords = unknownWords;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (obj == this)
            return true;
        if (!(obj instanceof VowelSearchState))
            return false;
        VowelSearchState other = (VowelSearchState)obj;
        return (prevWord.equals(other.prevWord) && unknownWords.equals(other.unknownWords));
    }

    @Override
    public int hashCode() {
        return (prevWord.hashCode() ^ unknownWords.hashCode());
    }

    public static class Builder extends SearchState.Builder {
        @Override
        public SearchState makeInitialState(String problem) {
            List<String> newWords = Arrays.asList(LangUtil.getWords(problem));
            return new VowelSearchState(new ArrayList<String>(), LangUtil.SENTENCE_BEGIN, newWords);
        }
    }

    @Override
    public boolean isGoal() {
        boolean isGoal = unknownWords.size() == 0;
        if (isGoal)
            System.out.println(words);
        return isGoal;
    }

    @Override
    public Collection<String> getApplicableActions() {
        List<String> actions = new ArrayList<>();
        if (unknownWords.size() > 0) {
            String nextWord = unknownWords.get(0);
            for (String possibleWord : ExpansionDictionary.getInstance().lookup(nextWord))
                actions.add(ADD_WORD_PREFIX + possibleWord);
        }
        return actions;
    }

    @Override
    public double getActionCost(String action) {
        if (action.substring(0, ADD_WORD_PREFIX.length()).equals(ADD_WORD_PREFIX)) {
            String word = action.substring(ADD_WORD_PREFIX.length());
            return SmoothedBigramModel.getInstance().cost(prevWord, word);
        } else {
            return 0;
        }
    }

    @Override
    public SearchState applyAction(String action) {
        if (action.substring(0, ADD_WORD_PREFIX.length()).equals(ADD_WORD_PREFIX)) {
            String word = action.substring(ADD_WORD_PREFIX.length());
            List<String> newWords = new ArrayList<>(words);
            newWords.add(word);
            List<String> newUnknownWords = unknownWords.subList(1, unknownWords.size());
            return new VowelSearchState(newWords, word, newUnknownWords);
        }
        return null;
    }
}
