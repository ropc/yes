import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class CombinedSearchState implements SearchState {

    public static final String ADD_CHARACTER_PREFIX = "add character ";
    public static final String ADD_WORD_PREFIX = "add word ";

    final List<String> words;
    final String prevWord;
    final String currentWord;
    final String unknownCharacters;

    private CombinedSearchState(List<String> words, String prevWord, String currentWord, String unknownCharacters) {
        this.words = words;
        this.prevWord = prevWord;
        this.currentWord = currentWord;
        this.unknownCharacters = unknownCharacters;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (obj == this)
            return true;
        if (!(obj instanceof CombinedSearchState))
            return false;
        CombinedSearchState other = (CombinedSearchState)obj;
        return (prevWord.equals(other.prevWord) &&
                currentWord.equals(other.currentWord) &&
                unknownCharacters.equals(other.unknownCharacters));
    }

    @Override
    public int hashCode() {
        return (prevWord.hashCode() ^
            currentWord.hashCode() ^
            unknownCharacters.hashCode());
    }

    public static class Builder extends SearchState.Builder {
        @Override
        public SearchState makeInitialState(String problem) {
            problem = LangUtil.join(Arrays.asList(LangUtil.getWords(problem)), "");
            return new CombinedSearchState(new ArrayList<String>(), LangUtil.SENTENCE_BEGIN, "", problem);
        }
    }

    @Override
    public boolean isGoal() {
        boolean isGoal = currentWord.length() == 0 && unknownCharacters.length() == 0;
        if (isGoal)
            System.out.println(words);
        return isGoal;
    }

    @Override
    public Collection<String> getApplicableActions() {
        List<String> actions = new ArrayList<>();
        if (unknownCharacters.length() > 0)
            actions.add(ADD_CHARACTER_PREFIX + unknownCharacters.substring(0, 1));
        if (currentWord.length() > 0) {
            for (String possibleWord : ExpansionDictionary.getInstance().lookup(currentWord))
                actions.add(ADD_WORD_PREFIX + possibleWord);
        }
        return actions;
    }

    @Override
    public double getActionCost(String action) {
        if (action.substring(0, ADD_WORD_PREFIX.length()).equals(ADD_WORD_PREFIX)) {
            String nextWord = action.substring(ADD_WORD_PREFIX.length());
            return SmoothedBigramModel.getInstance().cost(prevWord, nextWord);
        } else {
            return 0;
        }
    }

    @Override
    public SearchState applyAction(String action) {
        if (unknownCharacters.length() > 0 && action.equals(ADD_CHARACTER_PREFIX + unknownCharacters.substring(0, 1))) {
            String newWord = currentWord + unknownCharacters.substring(0, 1);
            String newUnknownCharacters = unknownCharacters.substring(1);
            return new CombinedSearchState(words, prevWord, newWord, newUnknownCharacters);
        } else if (action.substring(0, ADD_WORD_PREFIX.length()).equals(ADD_WORD_PREFIX)) {
            String newWord = action.substring(ADD_WORD_PREFIX.length());
            List<String> newKnownWords = new ArrayList<>(words);
            newKnownWords.add(newWord);
            return new CombinedSearchState(newKnownWords, newWord, "", unknownCharacters);
        }
        return null;
    }
}
