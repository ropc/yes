; Rodrigo Pacheco Curro

#! /usr/local/bin/racket
#lang racket

; 1
(define (echo lst)
  (cond
    ((not (null? (cdr lst)))
      (cons (car lst) (cons (car lst) (echo (cdr lst)))))
    (else
      (cons (car lst) (cons (car lst) '())))))

; (echo '(a (b c)))

; 2
(define (nth i lst)
  (cond
    ((eq? 0 i) (car lst))
    (else (nth (- i 1) (cdr lst)))))

; (nth 2 '(a b (c e) d))

; 3
(define (deep-reverse lst)
  (cond
    ((pair? lst)
      (append (deep-reverse (cdr lst)) (list (deep-reverse (car lst)))))
    (else
      lst)))

; (deep-reverse '(a b (c (e f)) d))
; (deep-reverse '((a b) c (d e f (g h (i j))) k ))

; 4
(define (assoc-all lst a-list)
  (map (lambda (x) (cadr (assoc x a-list))) lst))

; (assoc-all '(a d c d) '((a apple)(b boy)(c cat)(d dog)))

; 5
(define (filter-evens lst)
  (apply append (map (lambda (x) (if (even? x) (list x) null)) lst)))

; (filter-evens '(23 33 44 2 1 8))
