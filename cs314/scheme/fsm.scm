#! /usr/local/bin/racket
#lang racket/base

;;;; finite state machine simulator

;;; representation:
;;; fsm = (start-state accept-states transitions)
;;; transitions =  ((state-name . actions) ...) 
;;; where actions =  ((sym next-state)
;;; 
;;; eg ( s1 (s2) 
;;;      ((s1 (0 s1)(1 s2))
;;;       (s2 (0 s2)(1 s1))))

(define (next-state state sym transitions)
  (let ((actions (cdr (assoc state transitions))))
    (cadr (assoc sym actions))))

(define (run fsm sym-list)
  (let ((init-state (car fsm))
	(accept-states (cadr fsm))
	(transitions (caddr fsm)))
    (run-helper sym-list init-state accept-states transitions)))

(define (run-helper sym-list state accept-states transitions)
  (if (null? sym-list)
      (if (member state accept-states)
          "string accepted\n"
          "string NOT accepted\n")
      (let ((new-state (next-state state (car sym-list) transitions)))
        (display (list "state " state " symbol " (car sym-list) " new state : " new-state))
        (display "\n")
        (run-helper (cdr sym-list)
                    new-state
                    accept-states
                    transitions)        
        )))
  
(define fsm1 '(s1 (s2) ((s1(0 s1)(1 s2))(s2 (0 s2)(1 s1)))))
	 


























































































