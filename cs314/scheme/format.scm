#! /usr/local/bin/racket
#lang racket/base

(define (format commands)
;;  (display (list 'format commands))
  (if (pair? commands)
      (let* ((command (car commands))
	     (fn (lookup-format (car command))))
;;	(display command)
	(apply fn (cdr command))
	(format (cdr commands)))))

;;; formats is an a-list ((cmd cmd-fn) ...) where cmd is a format
;;; and cmd-fn is the function to handle cmd
(define formats '( ))

;;; add fn as the handler for cmd
(define (add-format cmd fn)
  (set! formats (cons (cons cmd fn) formats)))

;;; look up and return the handler for cmd
(define (lookup-format cmd)
  (let ((result (assoc cmd formats)))
    (if result (cdr result)
        (display (list "undefined format command - " cmd)))))

(add-format 'w display)

(define (repeat-char n char)
  (if (> n 0)
      (begin
	(display char)
	(repeat-char (- n 1) char))))

(add-format 'r repeat-char)

(define (donewline)
  (display #\newline))

(add-format 'nl donewline)
