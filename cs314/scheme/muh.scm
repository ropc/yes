#! /usr/local/bin/racket
#lang racket/base

(quote (+ 3 4))

((lambda (x) (+ x x)) 7)

(define yes (+ 8 9))
(define double (lambda (x) (+ x x)))
(define (doobal x) (+ x x))

(double 6)
(doobal 9)