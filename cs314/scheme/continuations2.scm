#! /usr/local/bin/racket
#lang racket/base

;;;; code from tutorial on continuation passing
;;;;   copyright 1991, 1997, 2005 Louis Steinberg

;;; --- tail recursion ---
;;; 
;;;  We will see later that it is useful to be able to write
;;;  code that is tail recursive - the recursive call is the
;;;  last thing a function invocation does.  The invocation
;;;  does the recursive call and then simply returns the value(s)
;;;  that call returns as _its_ values.

;;; traditional factorial
(define (fact n)
  (if (= n 1) 1
      (* n (fact (- n 1)))))

;;; tail recursive version
(define (fact-tr n)
  (fact-tr-helper (- n 1) n))

(define (fact-tr-helper n product-so-far)
  (if (= n 0) product-so-far
      (fact-tr-helper (- n 1)
		(* n product-so-far))))

;;; traditional reverse: O(n^2)
(define (rev l)
  (if (null? l) '( )
      (append (rev (cdr l)) (list (car l)))))

;;; tail recursive reverse: O(n)
(define (rev-tr l)
  (rev-tr-helper l '( )))

(define (rev-tr-helper l rev-so-far)
  (if (null? l) rev-so-far
      (rev-tr-helper
       (cdr l)
       (cons (car l) rev-so-far))))

;;; --- closures ---
;;;
;;; (lambda (...) ....) creates a closure - a data structure
;;; containing the code of the lambda plus the current lexical
;;; bindings

;;; in the following,note that the binding of lst used when the code
;;; (cons q list) is executed is the binding from the let in foo, not
;;; the binding from map

(define (my-map lst fn)
  (if (null? lst) '( )
      (cons (fn (car lst))
            (my-map (cdr lst) fn))))

(define (foo r)
  (let ((lst '(a b c)))
    (my-map '(x y z)
	    (lambda (q) (cons q lst)))))

;;; closures can give you the effect of static variables

(define up '( ))
(define down '( ))

(let ((count 0))
  (set! up (lambda () (set! count (+  count 1))
		   count))
  (set! down (lambda () (set! count (- count 1))
		     count)))

;;; --- continuations ---
;;;
;;; a continuation is a closure that is passed as an argument to a
;;; function, telling that function how the computation should continue
;;; when that function is done.  It is equivalent to the information
;;; placed on the stack telling a function how to return, but it is
;;; explicit rather than implicit in the call/return mechanism.

;;; example of using continuations to give two alternative ways
;;; to continue, in this case a normal return and an error exit.

;;; Lst is a list of numbers  (a b ... ).  Compute harmonic sum
;;; (1/a + 1/b + ... ).  If any number is 0, call
;;; continuation -found-zero- with no arguments.
;;; Else call continuation -continue- with one argument - the sum.
(define (harmonic-sum lst -continue- -found-zero-)
  (hs1 lst 0 -continue- -found-zero-))

(define (hs1 lst sum -continue- -found-zero-)
  (cond ((null? lst) (-continue- sum))
	((= (car lst) 0) (-found-zero-))
	(else  (hs1 (cdr lst) 
		    (+ (/ 1.0 (car lst)) sum)
		    -continue-
		    -found-zero-))))

;;; returns harmonic average of numbers in list, or *infinity* if any
;;; number is 0
(define (harmonic-average lst)
  (harmonic-sum lst (lambda (sum)(/ sum (length lst)))
                (lambda ( ) '*infinity*)))

;;; using continuations to implement co-routines

;;; very simple function
(define (foo x)
  (display x)
  (display #\newline)
  ; suppose we want to suspend it here, then do some other stuff, then 
  ; continue
  (display (list x))
  (display #\newline))

;;; fancier version
(define (foo1 x)
  (display x)
  (display #\newline)
  ;; the lines below do the same thing as in version above, but do it
  ;; in a more round-about way.  They package up a continuation, i.e. a
  ;; closure that says what to do to finish "the rest" of foo.
  ;; Then they immediately call this continuation and thus finish foo.
  ;; However, as we will see in next version, the continuation 
  ;; does not have to be called immediately
  ((lambda () 
     (display (list x))
     (display #\newline))))

;;; Now, let's actually suspend foo and call another function, then restart
;;; foo
(define (foo2 x)
  (display x)
  (display #\newline)
  (other2
   (lambda () 
     (display (list x))
     (display #\newline))))

;;; here's the other function.  its argument, -c- is the continuation
;;; it should call when it wants to restart foo
(define (other2 -c-)
  (display "other")
  (display #\newline)
  (-c-))


;---------------------------------------------------------------

;;; here's a still fancier version - foo runs, pauses and starts other,
;;; other runs, pauses and restarts foo, foo finishes & restarts other.

(define (foo3 x)
  (display x)
  (display #\newline)
  (other3
   (lambda (-c2-)		     ; -c2- is continuation to call to
					; continue with other
     (display (list x))
     (display #\newline)
     (-c2-))))

(define (other3 -c-)
  (display "          other")
  (display #\newline)
  (-c-
   (lambda ()			  ; this closure will be bound to -c2-
					; in foo
     (display "          rest of other")
       (display #\newline))))

;---------------------------------------------------------------
;;; here's still a fancier version, where other reads some data and passes
;;; it back to the second part of foo

(define (foo4 x)
  (display x)
  (display #\newline)
  (other4
   ;; this closure will be bound to -c- in other.
   ;; info is data passed back from first part of other
   ;; -c2- is continuation to call to continue other
   (lambda (info -c2-)	
     (display info)
     (display #\newline)
     (-c2-))))

(define (other4 -c-)
  (display "          other")
  (let ((data (read)))
    (-c-
     data
     (lambda ()			  ; this closure will be bound to -c2-
					; in foo
       (display  "          data in other: ")
       (display data)
       (display #\newline)))))

;---------------------------------------------------------------

;;; Now, some examples with loops

;;; *** first, a simple display loop, in non-coroutine form

;;; one-loop-b displays val, then calls fn to get another val to display and loops
(define (one-loop-b val fn)
  (display val)
  (display #\newline)
  (one-loop-b (fn) fn))

;;; fn to call one-loop-b with
(define counter '( ))
(let ((count 0))
  (set! counter 
	(lambda ( )
	  (set! count (+ 1 count))
	  count)))

;(one-loop-b 1 counter)

;---------------------------------------------------------------
;;; *** now, the same thing in coroutine form

;;; one-loop takes two arguments
;;;  val - the first value to display
;;;  corou - a coroutine to call to get more values  This coroutine takes
;;;      one argument: the continuation to call to continue one-loop
;;; one-loop displays the values it gets one per line
(define (one-loop val corou)		; lambda 0 (implicit in define)
  ;; display the first value on a new line
  (display val)
  (display #\newline)
  ;; now get the next value
  (corou
   ;; when come back, handle the rest of the values
   one-loop))

;;; *** and a coroutine to use with it

;;; three-loop takes two arguments:
;;;  list1 - a list of three-element lists and
;;;  corou1 - a coroutine (i.e. closure) to report values to.  (Technically,
;;;      only the first value is reported to this closure.)  This coroutine
;;;      takes 2 arguments:
;;;       - the value being reported, and
;;;       - a continutation to call to continue three-loop
;;; three-loop "reports" each of the three elements of each sublist of list1
;;; they are "reported" by passing them as arguments to corou1
(define (three-loop list1 corou1)     ; lambda 1  (implicit in define)
  (cond ((null? list1) '())
        ;; else, report three values to coroutine: car, cadr, caddr
        ;; of (car list1)
        (else
         ;; report first value
         (corou1
	  (car (car list1))
	  ;; when come back, do next two values
	  (lambda (corou2)		; lambda 2
	      ;; report second value
	      (corou2
	       (cadr (car list1))
	       ;; when come back do last value
	       (lambda (corou3)	; lambda 3
		   ;; report last value
		   (corou3
		    (caddr(car list1))
		    ;; when come back, handle rest of
		    ;; list1
		    (lambda (corou4)	; lambda 4
			(three-loop (cdr list1)
				    corou4))))))))))

;;; *** here's how to use them together:

;;; three-to-one takes a list of 3-element lists, and displays all those 
;;; elements one per line
(define (three-to-one top-list)
  (three-loop top-list one-loop))

;---------------------------------------------------------------

;;; *** and a more complex example, displaying two per line:

;;; two-loop takes two arguments
;;;  val5 - the first value top display
;;;  corou5 - a coroutine to call to get more values  This coroutine 
;;;    takes one argument: the continuation to call to continue two-loop
;;; two-loop displays the values it gets two per line
(define (two-loop val5 corou5)		; lambda 5 (implicit in define)
  ;; display the first value on a new line
  (display val5)
  (display " ")
  ;; now get the next value
  (corou5
   ;; when come back, display the second value on same line
   (lambda (val6 corou6)	; lambda 6
       (display val6)
       (display #\newline)
       ;; when come back, handle remaining lines.
       (corou6 two-loop))))

;;; Three-to-two takes a list of 3-element lists, and displays all
;;; those elements two per line

(define (three-to-two top-list)
  (three-loop top-list two-loop))

;---------------------------------------------------------------

;;; *** nested do-loops

(define a '#(#(a b c d e) #(f g h i j)))

;;; first a nested loop
(define (display-rows rown array)
  (if (< rown 2)
      (begin
	(display-entry (vector-ref array rown) 0)
	(display-rows (+ rown 1) array))))

(define (display-entry row coln)
  (if (< coln 5)
      (begin
	(display (vector-ref row coln))
	(display #\newline)
	(display-entry row (+ coln 1)))))

(define (array-loop array)
  (display-rows 0 array))

;;; Now, a coroutine version that can take the place of three-loop

;;; Problem: Instead of (display (vector-ref row coln)) the coroutine
;;; version of display-entry needs to call its coroutine argument, passing
;;; it a closure that says how to continue display-entry.  But as written,
;;; continuing display-entry eventually returns and continues display-rows.
;;; How can display-entry construct a closure that knows how to continue
;;; display-rows?

;;; Two solutions:
;;; A)  have display-rows pass display-entry a continuation that _says_ how to
;;;     continue

(define (display-rows-2 rown array)
  (if (< rown 2)
      (begin
	(display-entry-2 (vector-ref array rown)
			 0
			 (lambda () (display-rows-2 (+ rown 1) array))))))

(define (display-entry-2 row coln -c3-)
  (if (< coln 5)
      (begin
	(display (vector-ref row coln))
	(display #\newline)
	(display-entry-2 row (+ coln 1) -c3-))
      (-c3-)))

(define (array-loop-2 array)
  (display-rows-2 0 array))

;;; Now we can make display-entry call the coroutine

(define (display-rows-2c rown array -corou7-)
  (if (< rown 2)
      (begin
	(display-entry-2c (vector-ref array rown)
			 0
			 (lambda ( ) 
			   (display-rows-2c (+ rown 1) array -corou7-))
			 -corou7-))))

(define (display-entry-2c row coln -c3- -corou7-)
  (if (< coln 5)
      (begin
	(-corou7- (vector-ref row coln)
		  (lambda (-corou8- ) 
		    (display-entry-2c row (+ coln 1) -c3- -corou8-))))
      (-c3-)))

(define (array-loop-2c array)
  (display-rows-2c 0 array one-loop))

;;; Solution B:  Use call-with-current-continuation, also know as call/cc
;;; (call/cc takes one argument:  a closure with one argument.
;;; call/cc calls this closure passing it a continuation that represents
;;; what scheme will do if and when call/cc returns.  E.g.,

(define (cc-example x)
  (display 
   (call-with-current-continuation 
    (lambda (-cc-)
      (* 2
	 (cc-example-2 x -cc-))))))

(define (cc-example-2 x -cc-)
  (/ 1
     (- (if (= x 1)
	    (-cc- 'foo)
	    x))))

;;; if cc-example-2 calls -cc-, it will be as if
;;; call-with-current-continuation in cc-example returned foo.
;;; Otherwise, call-with-current-continuation will return whatever the
;;; * returns.

(define (display-rows-3 rown array -corou9-)
  (if (< rown 2)
      (begin
	(display-entry-3 (vector-ref array rown) 0 -corou9-)
	(display-rows-3 (+ rown 1) array -corou9-))))

(define (display-entry-3 row coln -corou9-)
  (if (< coln 5)
      (let ((-corou10-
	     (call-with-current-continuation
	      (lambda (-cc-)
		(-corou9- (vector-ref row coln)
			  -cc-)))))
	(display-entry-3 row (+ coln 1) -corou10-))))

(define (array-loop-3 array)
  (display-rows-3 0 array one-loop))

;---------------------------------------------------------------

;;; *** double recursion, similar to going down a binary tree

;;;; three versions of fibonacci - all with inefficient double
;;;; recursion

;;; normal recursive 
(define (fib1 n)
  (if (< n 3) 1
      (+ (fib1(- n 1))
         (fib1(- n 2)))))

;;; tail recursive (notice that continuations were needed here just to
;;; be tail recursive)
(define (fib n)
  (letrec ((fib-sub 
	    (lambda (n -c-)
	      (if (< n 3) (-c- 1)
		  (fib-sub (- n 1)
			   (lambda (n-1-value) ; 1
			     (fib-sub (- n 2)
				      (lambda (n-2-value) ; 2
					(-c- (+ n-1-value 
						n-2-value))))))))))
    (fib-sub n (lambda (x) x))))


;;; co-routine, presents all intermediate values to a 
;;; consumer co-routine like one-loop
(define (fibr n -corou-)
  (letrec
      ((fib-sub 
	(lambda (n -corou1- -c-)
	  (if (< n 3) (-corou1- 1
				(lambda (-corou5-)
				  (-c- 1 -corou5-)))
	      (fib-sub (- n 1)		; 3
		       -corou1-
		       (lambda (n-1-value -corou2-)
			 (fib-sub (- n 2)
				  -corou2-
				  (lambda (n-2-value -corou3-) ; 4
				    (-corou3-
				     (+ n-1-value 
					n-2-value)
				     (lambda (-corou4-)
				       (-c-
					(+ n-1-value 
					   n-2-value)
					-corou4-)))))))))))
    (fib-sub n -corou- (lambda (val -corou6-) val))))

;;; call fibr and one-loop together
(define (fib-to-one n)
  (fibr n one-loop))

(define test-match ()
  (match '((p q) q)
         '( (*or ((*var x) q)
                 (p (*var x)))
            (*var x) )))

