#! /usr/local/bin/racket
#lang racket/base

(define-syntax unless
  (syntax-rules ( )
    ((_ cond true-branch false-branch)
     (if cond false-branch true-branch))))

(define (save-inverse x)
  (unless (= x 0)
    (/ 1 x)
    '*inf*))

(define-syntax trace
  (syntax-rules ( )
    ((_ label expr)
     (let ((val expr))
       (display 'label)
       (display val)
       val))))

(define (fact n)
  (trace f (if (= n 0) 1
               (* n (fact (- n 1))))))

