#! /usr/local/bin/racket
#lang racket/base

(define (/rule args)
  (list '-
        (list '/
              (deriv (car args))
              (cadr args))
        (list '/ (list '*
                       (car args)
                       (deriv (cadr args)))
              (list '^ 
                    (cadr args)
                    2))))

(add-rule '/ /rule)