#! /usr/local/bin/racket
#lang racket/base

(define deriv-rules '( ))

(define (add-rule operator fn)
  (set! deriv-rules  (cons (list operator fn) deriv-rules)))

(define (lookup-rule operator)
  (car (cdr (assoc operator deriv-rules))))

(define (deriv expr)
  (cond ((number? expr) 0)
	((eq? expr 'x) 1)
	((symbol? expr) 0)
	((list? expr)
	 ((lookup-rule (car expr))
	  (cdr expr)))))
	 
(define (+rule args)
  (list '+	
	(deriv (car args))
	(deriv (car (cdr args)))))

(add-rule '+ +rule)

(define (*rule args)
  (list '+
	(list '*
	      (car args)
	      (deriv (car (cdr args))))
	(list '*
	      (deriv (car args))
	      (car (cdr args)))))

(add-rule '* *rule)