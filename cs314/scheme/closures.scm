#! /usr/local/bin/racket
#lang racket/base

(let ((x 1))
  (let ((y 2))
    (+ x y)))

(let ((x 1))
  ((lambda (y)(+ x y)) 
   2))

(let ((x 1))
  (let ((f (lambda (y)(+ x y))))
    (let ((x 400))
      (f 2))))

(let ((f 
       (let ((x 1))
         (lambda (y) (+ x y)))))
  (let ((x 400))
    (f 2)))

(define f
  (let ((x 1))
    (lambda (y)(+ x y))))

(let ((x 400))
  (f 2))

