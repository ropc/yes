#! /usr/local/bin/racket
#lang racket/base

;;;; simple sideways ascii-based plots

(define (plot-fns fns xmin xmax)
  ;  (display (list 'pf xmin))
  (if (<= xmin xmax)
      (begin
        (plot-line (fnvals fns xmin) xmin 0)
        (plot-fns fns (+ xmin 1) xmax))))

(define (plot-line vals x y)
;  (display (list 'pl vals x y))
  (if (< y 80)
      (begin
        (if (member y vals)
            (display "*")
            (display " "))
        (plot-line vals x (+ y 1)))
      (display #\newline)))

(define (fnvals fns x)
  (map (lambda (f) (f x)) fns))
