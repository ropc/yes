#! /usr/local/bin/racket
#lang racket/base

(define (quad a b c)
    (let ((discr (- (* b b)(* 4 a c)))
          (twoa (* 2 a)))
      (list (/ (- (- b)(sqrt discr))
               (* 2 a))
            (/ (+ (- b)(sqrt discr))
               (* 2 a)))))