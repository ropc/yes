%% likes(bob, sue).
%% eats(sue, peaches).

%% eats(bob, peaches):- likes(bob, pits).
%% eats(bob, Food):- likes(bob, Person), eats(Person, Food).

%% awake(ro).
%% awake(crazyPerson).
%% not(awake(dudeToMyRight)).
%% not(likes(ro, cs314)).
%% not(likes(dudeToMyRight, cs314)).
%% likes(crazyPerson, cs314).
%% boredIn(Person, Class):- not(awake(Person)), not(likes(Person, Class)).

%% %% a -> b, !b -> !a
%% %% a -> !b, b -> !a
%% a(x).
%% not(b(y)).
%% b(X):- not(a(X)).
%% %% idk...

inOrder([]).
inOrder([_]).
inOrder([H1, H2|T]) :- H2 > H1, inOrder([H2|T]).

%% suffix([],[]).
%% suffix([H1|T], L2) :- T = L2; suffix(T, L2).
suffix([H|T], T).
suffix([_|T], L2) :- suffix(T, L2).

%% Bar = baz, Bletch = baz
%% nope
%% A = a, Y = X = A = a, Z = b
odds([],[]).
odds([X], [X]).
odds([H1, H2|Ta], [H1|Tb]) :- odds(Ta, Tb).

insertInOrder([], X, [X]).
insertInOrder([HA|T], Num, [Num, HA|T]) :-
	HA > Num.
insertInOrder([HA|TA], Num, [HA|TB]) :-
	HA < Num, insertInOrder(TA, Num, TB).

last([Elt|[]], Elt).
last([],Elt) :- false.
last([_|T], Elt) :- last(T, Elt).

len([], 0).
%% len([_|T], N) :- N1 is N - 1, len(T, N1).
len([_|T], N) :- len(T, N1), N is N1 + 1.

sum([], 0).
sum([X], X).
sum([X|T], N) :- sum(T, N1), N is N1 + X.

%% sqroot

%% tree(Data, Left, Right).
sumTree(null, 0).
sumTree(tree(N, null, null), N).
sumTree(tree(Data, Left, Right), N) :-
	sumTree(Left, Lsum),
	sumTree(Right, Rsum),
	N is Lsum + Rsum + Data.

neighbors(_, _, []) :- false.
neighbors(N1, N2, [H|_]) :- H is node(N1, _, L), member(N2, L).
neighbors(N1, N2, [_|T]) :- neighbors(N1, N2, T). 

