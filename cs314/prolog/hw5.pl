%% Rodrigo Pacheco Curro

%% 1
inDept(joe, toy).
reportTo(X, Z) :- inDept(X, Y), headOfDept(Z, Y).
headOfDept(sam, toy).
salaryIsLess(X, Y) :- reportTo(X, Y).
salaryIsLess(joe, sam).

%% 2
fib(0, 0).
fib(1, 1).
fib(N, F) :-
	N > 1,
	N1 is N - 1,
	N2 is N - 2,
	fib(N1, F1),
	fib(N2, F2),
	F is F1 + F2.

%% 
%% query: fact(2, X).
%% fact(N, F)
%% 

%% 3
double([],[]).
double([H1|T1], [H1, H1|T2]) :- double(T1, T2).

%% 4
without0([],[]).
without0([H1|T1], L2) :-
	H1 = 0,
	without0(T1, L2).
without0([H1|T1],[H1|T2]) :-
	not(H1 = 0),
	without0(T1, T2).
