#include <stdio.h>
#include <string.h>
#include "sorted-list.h"
#include "hashtable.h"

Hashtable *HTCreate()
{
	Hashtable *ht = (Hashtable*)calloc(1, sizeof(Hashtable));
	// int i;
	// for (i = 0; i < 36; i++)
	// 	ht->buckets[i] = NULL;
	ht->count = 0;
	return ht;
}

void HTDestroy(Hashtable *ht)
{
	if (ht == NULL)
		return;

	int i;
	for (i = 0; i < 36; i++)
	{
		if (ht->buckets[i] != NULL)
			SLDestroy(ht->buckets[i]);
	}
	free(ht);
	
}

int Hash(const char *key)
{
	int i;
	int index = -1;
	
	// will check string for the first alphanumeric character
	for (i = 0; i < strlen(key) && index == -1; i++)
	{
		if ('a' <= key[i] && key[i] <= 'z')
			index = key[i] - 87;
		else if ('0' <= key[i] && key[i] <= '9')
			index = key[i] - 48;
	}

	return index;
}

HTToken *HTAdd(Hashtable *ht, const char *key)
{
	if (ht == NULL || key == NULL || strlen(key) <= 0)
		return NULL;

	int index = Hash(key);

	if (index == -1)
		return NULL;
	// else
	// 	printf("hash function failed.\n");

	if (ht->buckets[index] == NULL)
		ht->buckets[index] = SLCreate(HTTokenCmp, HTTokenDst, NULL);

	// SLInsert will check for duplicates, if a duplicate is found
	// it will destroy the new *data and return the node with the same
	// data that already existed in the list
	struct Node *node = SLInsert(ht->buckets[index], HTTokenCreate(key));
	HTToken *newToken = node->data;

	ht->count++;

	return newToken;
}

HTToken *HTFind(Hashtable *ht, const char *key)
{
	if (ht == NULL || key == NULL || strlen(key) == 0)
	{
		printf("invalid arguments to HTFind\n");
		return NULL;
	}

	int index = Hash(key);

	if (index == -1)
	{
		printf("hash function failed\n");
		return NULL;
	}

	if (ht->buckets[index] == NULL)
		return NULL;

	HTToken *foundToken = NULL;

	struct Node *currentNode = (ht->buckets[index])->head;
	for (; currentNode != NULL; currentNode = currentNode->next)
	{
		HTToken *tempToken = (HTToken*)currentNode->data;
		if (strcmp(tempToken->name, key) == 0)
		{
			foundToken = tempToken;
			break;
		}
	}

	return foundToken;
}

IntHTToken *IntHTAdd(Hashtable *ht, int key)
{
	if (ht == NULL)
		return NULL;

	int index = key % 36;

	if (index < 0)
		return NULL;

	if (ht->buckets[index] == NULL)
		ht->buckets[index] = SLCreate(IntHTTokenCmp, IntHTTokenDst, NULL);

	// SLInsert will check for duplicates, if a duplicate is found
	// it will destroy the new *data and return the node with the same
	// data that already existed in the list
	struct Node *node = SLInsert(ht->buckets[index], IntHTTokenCreate(key));
	IntHTToken *newToken = node->data;

	ht->count++;

	return newToken;
}

IntHTToken *IntHTFind(Hashtable *ht, int key)
{
	if (ht == NULL)
	{
		printf("invalid arguments to IntHTFind\n");
		return NULL;
	}

	int index = key % 36;

	if (index < 0)
	{
		printf("hash function failed\n");
		return NULL;
	}

	if (ht->buckets[index] == NULL)
		return NULL;

	IntHTToken *foundToken = NULL;

	struct Node *currentNode = (ht->buckets[index])->head;
	for (; currentNode != NULL; currentNode = currentNode->next)
	{
		IntHTToken *tempToken = (IntHTToken*)currentNode->data;
		if (tempToken->key == key)
		{
			foundToken = tempToken;
			break;
		}
	}

	return foundToken;
}

void HTWriteTo(Hashtable *ht, FILE *fp)
{
	if (ht == NULL || fp == NULL)
		return;
	
	int i;
	for (i = 0; i < 36; i++)
	{
		if (ht->buckets[i] == NULL)
			continue;
		
		// printf("%d: %x\n", i, (int)ht->buckets[i]);
		struct Node *tokenCursor = (ht->buckets[i])->head;

		for (; tokenCursor != NULL; tokenCursor = tokenCursor->next)
		{
			HTToken *currentToken = (HTToken*)tokenCursor->data;
			fprintf(fp, "<list>%s\n", currentToken->name);
			
			SortedListPtr recordList = (SortedListPtr)currentToken->data;
			struct Node *recordCursor = recordList->head;

			int j = 1;			
			for (;recordCursor != NULL; recordCursor = recordCursor->next)
			{
				char *format = "\t%s";

				if (j % 5 == 0 || recordCursor->next == NULL)
					format = "\t%s\n";

				fprintf(fp, format, ((Record*)recordCursor->data)->filename);
				j++;
			}
			fputs("</list>\n", fp);
		}
		
	}
	fflush(fp);
	printf("Writing to file successful\n");
}

Record *RecordCreate(const char *filename)
{
	Record *r = (Record*)malloc(sizeof(Record));
	if (strlen(filename) > 0)
	{
		r->filename = (char*)malloc(sizeof(char) * (strlen(filename) + 1));
		strcpy(r->filename, filename);
	}
	r->freq = 0;
	return r;
}

int RecordCmp(void *a, void *b)
{
	// return strcmp((char*)((Record*)a)->filename, (char*)((Record*)b)->filename);
	// if comparing against a new node, which should have freq == 0
	// check that the filename is the same
	// else compare thi file frequencies
	if (((Record*)b)->freq == 0 || ((Record*)a)->freq == 0)
		return strcmp((char*)((Record*)a)->filename, (char*)((Record*)b)->filename);
	else
		return ((Record*)b)->freq - ((Record*)a)->freq;
}

void RecordDst(void *a)
{
	if (a == NULL)
		return;

	if (((Record*)a)->filename != NULL)
		free(((Record*)a)->filename);
	free(a);
	
}

void RecordInc(void *a)
{
	if (a != NULL)
		((Record*)a)->freq++;
}

HTToken *HTTokenCreate(const char *name)
{
	// printf("creating token with key: ");
	HTToken *t = (HTToken*)malloc(sizeof(HTToken));
	if (strlen(name) > 0)
	{
		// printf("%s\n", name);
		t->name = (char*)malloc(sizeof(char) * (strlen(name) + 1));
		strcpy(t->name, name);
	}
	t->data = NULL;
	t->destructFunct = NULL;

	return t;
}

int HTTokenCmp(void *a, void *b)
{
	// since sorted list normally sorts from largest to smallest,
	// change the sign on this strcmp function to sort tokens from
	// smallest to largest (alphabetical order)
	return (-1 * strcmp(((HTToken*)a)->name, ((HTToken*)b)->name));
}

void HTTokenDst(void *a)
{
	if (a == NULL)
		return;

	// to make the code legible
	HTToken *t = (HTToken*)a;

	if (t->name != NULL)
		free(t->name);
	if (t->data != NULL)
	{
		if (t->destructFunct != NULL)
			(*t->destructFunct)(t->data);
	}

	free(a);
}

IntHTToken *IntHTTokenCreate(int key)
{
	// printf("creating token with key: ");
	IntHTToken *t = (IntHTToken*)malloc(sizeof(IntHTToken));
	t->key = key;
	t->data = NULL;
	t->destructFunct = NULL;

	return t;
}

int IntHTTokenCmp(void *a, void *b)
{
	// since sorted list normally sorts from largest to smallest,
	// change the sign on this strcmp function to sort tokens from
	// smallest to largest (alphabetical order)
	return (((IntHTToken*)a)->key - ((IntHTToken*)b)->key);
}

void IntHTTokenDst(void *a)
{
	if (a == NULL)
		return;

	// to make the code legible
	IntHTToken *t = (IntHTToken*)a;

	if (t->data != NULL)
	{
		if (t->destructFunct != NULL)
			(*t->destructFunct)(t->data);
		else
			free(t->data);
	}

	free(a);
}
