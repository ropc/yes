#ifndef CONSUMER_H
#define CONSUMER_H
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <errno.h>
#include "share.h"
#include "sorted-list.h"

typedef struct Customer {
	char *name;
	int cID;
	float creditLimit;
	char *address;
	char *state;
	char zip[6];
	sem_t mutex;	// mutex to ensure only transaction at a time is processed
	SortedListPtr successList;
	SortedListPtr failList;
} Customer;

typedef struct SuccessOrder {
	char *title;
	float price;
	float postCredit;
} SuccessOrder;

typedef struct FailOrder {
	char *title;
	float price;
} FailOrder;

/**
 * Consumer thread function
 */
void *consumer(void *arg);

/**
 * Creates a customer struct
 */
Customer *CreateCustomer(const char* name, int cID, float creditLimit, const char *address, const char *state, const char *zip);

/**
 * Frees memory related to a Customer struct
 */
void DestroyCustomer(void *a);

/**
 * Creates a struct that holds all the information
 * needed about a successful order.
 */
SuccessOrder *CreateSuccessOrder(const char *title, float price, float postCredit);

/**
 * Creates a struct that holds all the information
 * needed about a rejected order.
 */
FailOrder *CreateFailOrder(const char *title, float price);

/**
 * Function that prevents sorting in sorted-list.h
 */
int ListNoSort(void *a, void *b);

/**
 * Frees all memory associated with a
 * SuccessOrder struct
 */
void DestroySuccessOrder(void *a);

/**
 * Frees all memory associated with a
 * FailOrder struct
 */
void DestroyFailOrder(void *a);

#endif
