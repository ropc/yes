#include <semaphore.h>
#include "share.h"

void ShareInit(struct share *data)
{
	data->done = 0;
	data->count = 0;
	data->front = 0;
	data->buffsize = BUFFSIZE;
	sem_init(&data->mutex, 1, 1);
	sem_init(&data->dataAvailable, 1, 0);
	sem_init(&data->spaceAvailable, 1, BUFFSIZE);
}

void DestroyShare(struct share *data)
{
	// int i;
	// for (i = 0; i < BUFFSIZE; i++)
	// 	DestroyBookOrder(&data->buff[i]);

	sem_destroy(&data->mutex);
	sem_destroy(&data->dataAvailable);
	sem_destroy(&data->spaceAvailable);
}

int getNextField(char *fieldDest, const char *str, int *position)
{
	int returnInt = 0;
	int i = 0;
	// int startPosition = *position;
	char currentChar;
	// printf("position is %d\n", *lastPosition);
	while ((currentChar = str[*position]) != '\0')
	{
		// printf("chekcing char: %c\n", currentChar);
		(*position)++;
		if (currentChar != '|' && currentChar != '"' && currentChar != '\n')
			fieldDest[i++] = currentChar;
		else if (i > 0)
		{
			fieldDest[i] = '\0';
			returnInt = 1;
			break;
		}
		else
		{
			// printf("currentChar IS: %x\n", currentChar);
			// if (currentChar != '\t' && i <= 0)
			if (*position >= strlen(str))
			{
				fieldDest[0] = '\0';
				break;
			}
		}
	}

	return returnInt;
}
