#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include "hashtable.h"
#include "share.h"
#include "producer.h"
#include "consumer.h"

/**
 * Create a Customer struct out of a line of text
 * in the correct format
 */
Customer *getCustomer(const char *line)
{
	char name[100], addr[150], state[50], zip[6], currentField[150];
	int cID;
	float creditLimit;

	// is this necessary?
	int i;
	for (i = 0; i < strlen(line); i++)
	{
		int fieldNum = 0;
		int j = 0;	// position in the current line
		while (getNextField(currentField, line, &j) == 1)
		{
			if (fieldNum == 0)
				strcpy(name, currentField);
			else if (fieldNum == 1)
				cID = atoi(currentField);
			else if (fieldNum == 2)
				creditLimit = atof(currentField);
			else if (fieldNum == 3)
				strcpy(addr, currentField);
			else if (fieldNum == 4)
				strcpy(state, currentField);
			else if (fieldNum == 5)
				strcpy(zip, currentField);
			else
				break;

			fieldNum++;
		}
	}

	return CreateCustomer(name, cID, creditLimit, addr, state, zip);
}

/**
 * stores customers in the given hashtable
 * and makes a list out of the customers using the same
 * Customer structs.
 * This allows for quick lookup by customer ID and the
 * benefit of already having the customers in a list for
 * printing out the final report.
 */
SortedListPtr storeCustomers(Hashtable *cHT, FILE *fp)
{
	if (cHT == NULL || fp == NULL)
		return NULL;

	char str[200];

	SortedListPtr list = SLCreate(ListNoSort, DestroyCustomer, NULL);

	while (fgets(str, 200, fp) != NULL)
	{
		Customer *customer = getCustomer(str);

		if (SLInsert(list, customer) == NULL)
			printf("could not add customer to list\n");

		IntHTToken *newToken = IntHTAdd(cHT, customer->cID);
		if (newToken == NULL)
			printf("adding to cHT failed\n");
		else
		{
			newToken->data = customer;
			newToken->destructFunct = (void*)DestroyCustomer;
		}

		// printf("read:\"%s\"|%d|%f|\"%s\"|\"%s\"|\"%s\"\n",
		// 	customer->name, customer->cID, customer->creditLimit, customer->address, customer->state, customer->zip);
	}
	return list;
}

/**
 * stores the categories of a given fp in a hashtable
 * each category is marked with a number indicating which
 * number queue is associated with it. Starts count at 0.
 */
Hashtable *storeCategories(FILE *fp)
{
	if (fp == NULL)
		return NULL;

	Hashtable *ht = HTCreate();

	char str[100];
	long int num = 0;	// just to make it fit in the void* space

	while (fgets(str, 100, fp) != NULL)
	{
		int i = 0;
		getNextField(str, str, &i);
		HTToken *addedCat = HTAdd(ht, str);
		addedCat->data = (void*)num;
		// addedCat->destructFunct = free;
		num++;
	}

	return ht;
}

int main(int argc, char const *argv[])
{
	if (argc != 4)
	{
		printf("Need exactly 3 arguments (%d given)\n", argc - 1);
		return 1;
	}

	FILE *database, *orders, *categories;

	if ((database = fopen(argv[1], "r")) == NULL)
	{
		printf("Cannot open '%s': %s\n", argv[1], strerror(errno));
		return 1;
	}

	if ((orders = fopen(argv[2], "r")) == NULL)
	{
		printf("Cannot open '%s': %s\n", argv[2], strerror(errno));
		return 1;
	}

	if ((categories = fopen(argv[3], "r")) == NULL)
	{
		printf("Cannot open '%s': %s\n", argv[3], strerror(errno));
		return 1;
	}
	
	Hashtable *catHT = storeCategories(categories);
	fclose(categories);

	if (catHT == NULL)
	{
		printf("Categories hashtable was not created\n");
		return 1;
	}
	else if (catHT->count <= 0)
	{
		printf("Invalid number of categories in categories file\n");
		return 1;
	}

	// numCategories is also the number of consumer threads
	// and buffers that will be needed for the consumer process
	int numCategories = catHT->count;

	int shmid;
	// creates a shared memory segment the size of a struct share
	// array the length of the number of categories
	if ((shmid = shmget(IPC_PRIVATE, sizeof(struct share) * numCategories, IPC_CREAT | 0666)) == -1)
	{
		printf("Could not create shared memory\n");
		exit(1);
	}

	pthread_t producerThread;

	// attaches current process (Producer) to shared memory segment
	struct share *sharedBlock;
	if ((sharedBlock = shmat(shmid, NULL, 0)) == (void*)-1)
	{
		printf("shmat failed in parent\n");
		exit(1);
	}

	// initializes all semaphores in each
	// buffer in the shared memory segment
	int i;
	for (i = 0; i < numCategories; i++)
		ShareInit(&sharedBlock[i]);

	// create a child process (for consumers)
	pid_t child;
	child = fork();
	if (child < 0)
	{
		printf("fork failed\n");
		exit(1);
	}
	else if (child == 0)
	{
		// Child process
		
		// printf("child:%d\n", getpid());

		struct threadArg consArg[numCategories];
		pthread_t consumers[numCategories];

		struct share *bufferArray = shmat(shmid, NULL, 0);
		if (bufferArray == NULL)
		{
			printf("shmat failed in child\n");
			exit(1);
		}

		Hashtable *customers = HTCreate();
		SortedListPtr customerList = storeCustomers(customers, database);
		
		for (i = 0; i < numCategories; i++)
		{
			consArg[i].data = &bufferArray[i];
			consArg[i].bufferNum = i;
			consArg[i].ht = customers;
			pthread_create(&consumers[i], 0, consumer, (void*)&consArg[i]);
		}

		// wait for all consumer threads to finish
		for (i = 0; i < numCategories; i++)
			pthread_join(consumers[i], 0);

		printf("\n######################\n#### FINAL REPORT ####\n######################\n");
		struct Node *customerCursor = customerList->head;
		for (;customerCursor != NULL; customerCursor = customerCursor->next)
		{
			if (customerCursor != customerList->head)
				printf("\n");

			Customer *customer = customerCursor->data;
			printf("=== BEGIN CUSTOMER INFO ===\n### BALANCE ###\n");
			printf("Customer name: %s\nCustomer ID number: %d\nRemaining credit balance after all purchases (a dollar amount): %.2f\n",
				customer->name, customer->cID, customer->creditLimit);
			printf("### SUCCESSFUL ORDERS ###\n");
			if (customer->successList != NULL)
			{
				struct Node *node = customer->successList->head;
				for (;node != NULL; node = node->next)
				{
					SuccessOrder *order = node->data;
					printf("\"%s\"|%.2f|%.2f\n",
						order->title, order->price, order->postCredit);
				}
			}
			printf("### REJECTED ORDERS ###\n");
			if (customer->failList != NULL)
			{
				struct Node *node = customer->failList->head;
				for (;node != NULL; node = node->next)
				{
					FailOrder *order = node->data;
					printf("\"%s\"|%.2f\n",
						order->title, order->price);
				}
			}
			printf("=== END CUSTOMER INFO ===\n");
		}

		// cleanup
		HTDestroy(customers);
		SLDestroyNoData(customerList);

		shmdt(bufferArray);
		
		exit(0);
	}
	else
	{
		// Parent process
		
		printf("Created process %d to handle consumer threads\n", child);
		
		struct threadArg prodArg;
		prodArg.data = sharedBlock;
		prodArg.ht = catHT;
		prodArg.other = orders;
		pthread_create(&producerThread, 0, producer, (void*)&prodArg);

		// wait for producer thread
		pthread_join(producerThread, 0);

		// wait for consumer process
		wait(0);
	}

	shmdt(sharedBlock);
	fclose(database);
	fclose(orders);
	
	return 0;
}