#ifndef SHARE_H
#define SHARE_H
#include <semaphore.h>
#include "hashtable.h"
#define BUFFSIZE 5

typedef struct BookOrder {
	char title[200];
	float price;
	int cID;
	char category[100];	
} BookOrder;

struct share {
	BookOrder buff[BUFFSIZE];
	int done;
	int count;
	int front;
	int buffsize;
	sem_t mutex;
	sem_t dataAvailable;
	sem_t spaceAvailable;
};

struct threadArg {
	struct share *data;
	int bufferNum;
	Hashtable *ht;
	void *other;
};

/**
 * initializes semaphores in a share struct
 */
void ShareInit(struct share *data);

/**
 * destroys semaphores in a share structs
 */
void DestroyShare(struct share *data);

/**
 * gets the next field delimited by | or "
 * returns the number of fields found (1 on success, 0 on fail)
 */
int getNextField(char *fieldDest, const char *str, int *position);

#endif
