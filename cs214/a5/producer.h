#ifndef PRODUCER_H
#define PRODUCER_H
#include <stdlib.h>
#include "share.h"

/**
 * Producer thread function
 */
void *producer(void *arg);

/**
 * creates a BookOrder out of a line
 * of text in the correct format 
 */
BookOrder *getOrder(const char *line);

/**
 * Creates a BookOrder with given fields
 * can be freed with just free()
 */
BookOrder *CreateBookOrder(const char *title, float price, int cID, char *category);

#endif
