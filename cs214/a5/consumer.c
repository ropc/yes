#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <semaphore.h>
#include <errno.h>
#include "sorted-list.h"
#include "consumer.h"

void *consumer(void *arg)
{
	struct threadArg *a = (struct threadArg*)arg;
	struct share *buffer = a->data;
	Hashtable *customers = a->ht;
	int messagePrinted = 0;

	while (!buffer->done || buffer->count > 0)
	{
		// prevents thread from being blocked in case
		// that buffer->done changes to 1
		if (sem_trywait(&buffer->dataAvailable) != 0)
		{
			if (messagePrinted == 0)
			{
				printf("Consumer %d waits because buffer is empty.\n",
					a->bufferNum);
			}
			messagePrinted = 1;
			continue;
		}

		if (messagePrinted == 1)
			printf("Consumer %d resumes\n", a->bufferNum);
		
		messagePrinted = 0;

		BookOrder currentOrder;

		// retrieve BookOrder from buffer
		sem_wait(&buffer->mutex);
		currentOrder = buffer->buff[buffer->front];
		buffer->front = (buffer->front + 1) % buffer->buffsize;
		buffer->count--;
		printf("Consumer %d takes one order out of buffer\n", a->bufferNum);
		sem_post(&buffer->mutex);
		sem_post(&buffer->spaceAvailable);

		// begin processing orders
		IntHTToken *t = IntHTFind(customers, currentOrder.cID);
		if (t == NULL)
		{
			printf("\tcustomer with cID:%d not found\n",
				currentOrder.cID);
			continue;
		}

		Customer *customer = (Customer*)t->data;
		if (customer == NULL)
		{
			printf("\tcustomer has no data\n");
			continue;
		}

		// initializing some variables out here before use of mutex
		void *list = NULL, *order = NULL;
		struct Node *newNode;

		// use mutex for each customer to deal with transactions
		sem_wait(&customer->mutex);
		if (customer->creditLimit >= currentOrder.price)
		{
			// successful purchase
			customer->creditLimit -= currentOrder.price;
			list = customer->successList;
			order = CreateSuccessOrder(currentOrder.title, currentOrder.price, customer->creditLimit);
			printf("Order confirmation:\tTitle: \"%s\"\tPrice: %.2f\n\tShipping info: Customer name: %s\tAddress:%s, %s, %s\n",
				currentOrder.title, currentOrder.price, customer->name, customer->address, customer->state, customer->zip);
		}
		else
		{
			list = customer->failList;
			order = CreateFailOrder(currentOrder.title, currentOrder.price);
			printf("Order rejected:\tCustomer: %s\tRemaining credit limit: %.2f\n\tOrder info: \"%s\"|%.2f|%d|\"%s\"\n",
				customer->name, customer->creditLimit, currentOrder.title, currentOrder.price, currentOrder.cID, currentOrder.category);
		}
		newNode = SLInsert(list, order);
		sem_post(&customer->mutex);

		if (newNode == NULL)
		{
			printf("%ld: order could not be placed in list: %d, %.10s, %s\n",
				pthread_self(), currentOrder.cID, currentOrder.title, currentOrder.category);
		}
	}

	DestroyShare(buffer);

	return NULL;
}

Customer *CreateCustomer(const char* name, int cID, float creditLimit, const char *address, const char *state, const char *zip)
{
	Customer *newCustomer = (Customer*)malloc(sizeof(Customer));
	newCustomer->name = NULL;
	newCustomer->cID = cID;
	newCustomer->creditLimit = creditLimit;
	sem_init(&newCustomer->mutex, 0, 1);

	if (strlen(name) > 0)
	{
		newCustomer->name = (char*)malloc(sizeof(char) * strlen(name) + 1);
		strcpy(newCustomer->name, name);
	}

	if (strlen(address) > 0)
	{
		newCustomer->address = (char*)malloc(sizeof(char) * strlen(address) + 1);
		strcpy(newCustomer->address, address);
	}

	if (strlen(state) > 0)
	{
		newCustomer->state = (char*)malloc(sizeof(char) * strlen(state) + 1);
		strcpy(newCustomer->state, state);
	}

	if (strlen(zip) == 5)
		strcpy(newCustomer->zip, zip);

	newCustomer->successList = SLCreate(ListNoSort, DestroySuccessOrder, NULL);
	newCustomer->failList = SLCreate(ListNoSort, DestroyFailOrder, NULL);

	return newCustomer;
}

void DestroyCustomer(void *a)
{
	Customer *customer = (Customer*)a;

	if (customer->name != NULL)
		free(customer->name);
	if (customer->address != NULL)
		free(customer->address);
	if (customer->state != NULL)
		free(customer->state);

	SLDestroy(customer->successList);
	SLDestroy(customer->failList);
	sem_destroy(&customer->mutex);

	free(customer);
}

int ListNoSort(void *a, void *b)
{
	return 1;
}

SuccessOrder *CreateSuccessOrder(const char *title, float price, float postCredit)
{
	SuccessOrder *order = (SuccessOrder*)malloc(sizeof(SuccessOrder));
	if (strlen(title) > 0)
	{
		order->title = (char*)malloc(sizeof(char) * strlen(title) + 1);
		strcpy(order->title, title);
	}
	order->price = price;
	order->postCredit = postCredit;

	return order;
}

FailOrder *CreateFailOrder(const char *title, float price)
{
	FailOrder *order = (FailOrder*)malloc(sizeof(FailOrder));
	if (strlen(title) > 0)
	{
		order->title = (char*)malloc(sizeof(char) * strlen(title) + 1);
		strcpy(order->title, title);
	}
	order->price = price;

	return order;
}

void DestroySuccessOrder(void *a)
{
	SuccessOrder *order = (SuccessOrder*)a;
	free(order->title);
	free(order);
}

void DestroyFailOrder(void *a)
{
	FailOrder *order = (FailOrder*)a;
	free(order->title);
	free(order);
}
