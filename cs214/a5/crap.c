#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

struct yes {
	int a;
	char b[20];
};

int main(int argc, char const *argv[])
{
	pid_t child;

	int shmid;
	if ((shmid = shmget(IPC_PRIVATE, sizeof(struct yes), IPC_CREAT | 0666)) == -1)
		printf("something fucked up\n");
	void *shm = shmat(shmid, NULL, 0);
	if (shm == (void*)-1)
	{
		printf("shmat failed in parent\n");
		exit(0);
	}
	struct yes *data = (struct yes*)shm;
	data->a = 0;
	
	child = fork();
	if (child < 0)
	{
		printf("fork failed\n");
		exit(0);
	}
	else if (child == 0)
	{
		printf("child:%d\n", getpid());
		struct yes *childShared = shmat(shmid, NULL, 0);
		if (childShared == NULL)
		{
			printf("shmat failed in child\n");
			exit(0);
		}
		printf("%d\n", childShared->a);
		printf("%d\n", childShared->a);
		shmdt(childShared);
		exit(0);
	}
	else
	{
		printf("parent:%d\n", getpid());
		data->a = 21;

		wait(0);
	}
	shmdt(shm);
	printf("here\n");

	// struct yes a, b;
	// a.a = 1;
	// strcpy(a.b,"hello");

	// b.a = 2;
	// strcpy(b.b, "bye");
	// printf("a: %d\t%s\nb: %d\t%s\n", a.a, a.b, b.a, b.b);

	// b = a;

	// printf("a: %d\t%s\nb: %d\t%s\n", a.a, a.b, b.a, b.b);



	return 0;
}