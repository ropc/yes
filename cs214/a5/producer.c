#include <string.h>
#include "producer.h"

void *producer(void *arg)
{
	struct threadArg *pArg = (struct threadArg*)arg;
	struct share *data = pArg->data;
	Hashtable *catHT = pArg->ht;
	FILE *ordersFp = pArg->other;

	char str[200];
	
	while (fgets(str, 200, ordersFp) != NULL)
	{
		BookOrder *currentOrd = getOrder(str);

		HTToken *currentCategory = HTFind(catHT, currentOrd->category);
		if (currentCategory == NULL)
		{
			printf("failed to find category: %s\n", currentOrd->category);
			continue;
		}

		int catNum = (int)(long int)currentCategory->data;
		struct share *buffer = &data[catNum];
		int messagePrinted = 0;

		if (buffer->count == buffer->buffsize)
		{
			if (messagePrinted == 0)
				printf("Producer waits because buffer %d is full.\n", catNum);
			
			messagePrinted = 1;
		}

		sem_wait(&buffer->spaceAvailable);

		if (messagePrinted == 1)
			printf("Producer resumes on buffer %d\n", catNum);

		

		// sem_wait(&buffer->spaceAvailable);
		sem_wait(&buffer->mutex);
		int back = (buffer->front + buffer->count) % buffer->buffsize;
		buffer->buff[back] = *currentOrd;
		buffer->count++;
		printf("Producer added order to buffer %d.\n", catNum);
		sem_post(&buffer->mutex);
		sem_post(&buffer->dataAvailable);

		// printf("added\t\t\t\t%.10s, %s\n",
		// 	currentOrd->title, currentOrd->category);
		free(currentOrd);
	}

	// tell all the other threads to stop
	int i;
	for (i = 0; i < catHT->count; i++)
		data[i].done = 1;

	HTDestroy(catHT);

	return NULL;
}

BookOrder *getOrder(const char *line)
{
	char name[200], category[200], currentField[200];
	float price;
	int cID;

	int fieldNum = 0;
	
	int j = 0;
	while (getNextField(currentField, line, &j) == 1)
	{
		if (fieldNum == 0)
			strcpy(name, currentField);
		else if (fieldNum == 1)
			price = atof(currentField);
		else if (fieldNum == 2)
			cID = atoi(currentField);
		else if (fieldNum == 3)
			strcpy(category, currentField);
		else
			break;

		fieldNum++;
	}

	return CreateBookOrder(name, price, cID, category);
}

BookOrder *CreateBookOrder(const char *title, float price, int cID, char *category)
{
	BookOrder *newBookOrder = (BookOrder*)malloc(sizeof(BookOrder));
	newBookOrder->price = price;
	newBookOrder->cID = cID;

	if (strlen(title) > 0)
		strcpy(newBookOrder->title, title);

	if (strlen(category) > 0)
		strcpy(newBookOrder->category, category);

	return newBookOrder;
}
