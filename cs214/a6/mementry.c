#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "mementry.h"

static struct MemEntry *head = NULL, *tail = NULL;

void *mymalloc(unsigned int size, int isCalloc, char *file, int line)
{
	static int initialized = 0;
	struct MemEntry *p, *next;
	char *returnPtr = NULL;
	
	if (!initialized)
	{
		initialized = 1;
		atexit(leakCheck);
	}

	p = head;
	while (p != NULL)
	{
		if (p->size < size || !p->isFree)
			p = p->next;
		else if (p->size <= (size + MEMSIZE))
		{
			p->isFree = 0;
			p->pattern = PATTERN;
			returnPtr = (char*)p + MEMSIZE;
			break;
		}
		else
		{
			next = (struct MemEntry*)((char*)p + MEMSIZE);
			next->prev = p;
			next->next = p->next;
			if (p->next != NULL)
				p->next->prev = next;

			p->next = next;
			next->size = p->size - MEMSIZE - size;
			next->isFree= 1;
			next->pattern = PATTERN;
			p->size = size;
			p->isFree = 0;
			p->pattern = PATTERN;
			if (p == tail)
				tail = next;
			returnPtr = (char*)p + MEMSIZE;
			break;
		}
	}

	if (returnPtr == NULL)
	{
		if ((p = (struct MemEntry*)sbrk(MEMSIZE + size)) == (void*)-1)
		{
			printf("Unable to allocate memory:\t%s:%d\n", file, line);
			return NULL;
		}
		else if (tail == NULL)
		{
			p->prev = NULL;
			p->next = NULL;
			p->size = size;
			p->isFree = 0;
			p->pattern = PATTERN;
			head = p;
			tail = p;
			returnPtr = (char*)p + MEMSIZE;
		}
		else
		{
			p->prev = tail;
			p->next = tail->next;
			p->size = size;
			p->isFree = 0;
			p->pattern = PATTERN;
			tail->next = p;
			tail = p;
			returnPtr = (char*)p + MEMSIZE;
		}
	}

	if (returnPtr != NULL && isCalloc)
		memset(returnPtr, 0, size);
	
	return returnPtr;
}

void myfree(void *p, char *file, int line)
{
	struct MemEntry *ptr = (struct MemEntry*)((char*)p - MEMSIZE);
	
	if (head != NULL && tail != NULL && ((long)ptr < (long)head	|| (long)((char*)tail + MEMSIZE + tail->size) < (long)p))
	{
		printf("Pointer not in heap:\t%s:%d\n", file, line);
		return;
	}

	if (ptr->pattern != PATTERN)
	{
		printf("Pointer not returned from malloc():\t%s:%d\n", file, line);
		return;
	}

	if (ptr->isFree == 1)
	{
		printf("Double free:\t%s:%d\n", file, line);
		return;
	}

	struct MemEntry *prev, *next;

	if ((prev = ptr->prev) != NULL && prev->isFree)
	{
		prev->size += MEMSIZE + ptr->size;
		prev->next = ptr->next;
		if (ptr->next != NULL)
			ptr->next->prev = prev;
	}
	else
	{
		ptr->isFree = 1;
		prev = ptr;
	}

	if ((next = ptr->next) != NULL && next->isFree)
	{
		prev->size += MEMSIZE + next->size;
		prev->next = next->next;
		if (next->next != NULL)
			next->next->prev = prev;
	}
}

void *myrealloc(void *p, unsigned int size, char *file, int line)
{
	if (p == NULL)
		return mymalloc(size, 0, file, line);

	struct MemEntry *ptr = (struct MemEntry*)((char*)p - MEMSIZE);
	
	if ((long)ptr < (long)head	|| (long)((char*)tail + MEMSIZE + tail->size) < (long)p)
	{
		printf("Pointer not in heap:\t%s:%d\n", file, line);
		return NULL;
	}

	if (ptr->pattern != PATTERN)
	{
		printf("Pointer not returned from malloc():\t%s:%d\n", file, line);
		return NULL;
	}

	if (ptr->isFree)
		return mymalloc(size, 0, file, line);

	if (ptr->size >= size)
	{
		// try to make it smaller
		if (ptr->size >= size + MEMSIZE)
		{
			// can fit another MemEntry struct
			struct MemEntry *new = (struct MemEntry*)((char*)ptr + MEMSIZE + size);
			new->next = ptr->next;
			new->prev = ptr;
			new->isFree = 1;
			new->size = ptr->size - MEMSIZE - size;
			new->pattern = PATTERN;
			ptr->size = size;
			if (ptr->next != NULL)
				ptr->next->prev = new;
			if (ptr == tail)
				tail = new;
			ptr->next = new;

			// if possible, merge
			if (new->next != NULL && new->next->isFree)
			{
				struct MemEntry *next = new->next;
				new->size += MEMSIZE + next->size;
				new->next = next->next;
				if (next->next != NULL)
					next->next->prev = new;
			}
		}
		// if can't fit another struct, give back same pointer
		// which already has access to the necessary memory space

		return (char*)ptr + MEMSIZE;
	}
	else
	{
		if (ptr->next != NULL && ptr->size + MEMSIZE + ptr->next->size >= size)
		{
			if (ptr->size + ptr->next->size >= size)
			{
				// can fit another MemEntr struct
				// keep info of next in case it is overwritten
				struct MemEntry tempNext = *ptr->next;
				struct MemEntry *new = (struct MemEntry*)((char*)ptr + MEMSIZE + size);
				new->size = ptr->size + tempNext.size - size;
				new->next = tempNext.next;
				new->prev = ptr;
				new->isFree = 1;
				new->pattern = PATTERN;
				if (new->next != NULL)
					new->next->prev = new;
				else
					tail = new;
				ptr->size = size;
				ptr->next = new;
			}
			else
			{
				ptr->size = ptr->size + MEMSIZE + ptr->next->size;
				ptr->next = ptr->next->next;
				if (ptr->next->next != NULL)
					ptr->next->next->prev = ptr;
			}

			return (char*)ptr + MEMSIZE;
			
		}
		else if (ptr == tail)
		{
			// if its the last MemEntry struct, expand it
			if (sbrk(size - ptr->size) == (void*)-1)
			{
				printf("Unable to allocate memory:\t%s:%d\n", file, line);
				return NULL;
			}
			else
			{
				ptr->size = size;
				return (char*)ptr + MEMSIZE;
			}
		}
		else
		{
			// can't fit it here, need to allocate a new block
			struct MemEntry *new = mymalloc(size, 0, file, line);
			memcpy((char*)new + MEMSIZE, p, ptr->size);
			myfree(ptr, file, line);
			return (char*)new + MEMSIZE;
		}
	}
}

void leakCheck()
{
	struct MemEntry *cursor = head;
	int totalLeaked = 0;
	while (cursor != NULL)
	{
		if (cursor->pattern != PATTERN)
			printf("Memory at %ld currupt.\n", (long)cursor);

		if (!cursor->isFree)
		{
			printf("Memory leak at %ld of size: %d\n", (long)cursor, cursor->size);
			totalLeaked += cursor->size;
		}
		cursor = cursor->next;
	}

	if (totalLeaked > 0)
		printf("Total memory leaked: %d\n", totalLeaked);

}