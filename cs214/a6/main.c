#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "mementry.h"

int main(int argc, char const *argv[])
{
	int x;
	free(&x);

	char *p = (char*)malloc(200);
	free(p + 10);

	free(p);
	free(p);

	p = (char*)malloc(100);
	free(p);

	p = (char*)malloc(100);
	free(p);

	int *a = (int*)malloc(8);
	printf("%d, %d\n", a[0], a[1]);
	a = realloc(a, 4);
	printf("%d, %d\n", a[0], a[1]);
	a = realloc(a, 12);
	printf("%d, %d, %d\n", a[0], a[1], a[2]);
	a = realloc(a, 12);
	printf("%d, %d, %d\n", a[0], a[1], a[2]);
	free(a);

	int *b = (int*)calloc(8);
	printf("%d, %d\n", b[0], b[1]);
	
	return 0;
}