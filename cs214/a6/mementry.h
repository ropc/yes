#ifndef MEMENTRY_H
#define MEMENTRY_H
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#define MEMSIZE sizeof(struct MemEntry)
#define PATTERN 'r' * 16777216 + 'a'* 65536 + 'p' * 256 + 'r' + 'a' + 'p'
#define malloc( x ) mymalloc( x , 0 , __FILE__ , __LINE__ )
#define calloc( x ) mymalloc( x , 1 , __FILE__ , __LINE__ )
#define realloc( x , y ) myrealloc( x , y , __FILE__ , __LINE__ )
#define free( x ) myfree( x , __FILE__ , __LINE__ )

struct MemEntry {
	struct MemEntry *prev, *next;
	unsigned int size;
	unsigned int isFree : 1;
	unsigned int pattern : 31;
};

/**
 * Allocates a block in the heap
 */
void *mymalloc(unsigned int size, int isCalloc, char *file, int line);

/**
 * Frees a block in the heap
 */
void myfree(void *p, char *file, int line);

/**
 * Reallocates a block in the heap to a new size
 */
void *myrealloc(void *p, unsigned int size, char *file, int line);

/**
 * Checks the heap for any leaks
 */
void leakCheck();

#endif