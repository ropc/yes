#include <stdlib.h>
#include <stdio.h>
#include "sorted-list.h"

SortedListPtr SLCreate(CompareFuncT cf, DestructFuncT df, IncrementFuncT incf)
{
	SortedListPtr list = (SortedListPtr)malloc(sizeof(struct SortedList));
	list->head = NULL;
	list->compareFunction = cf;
	list->destructFunction = df;
	list->incrementFunction = incf;
	return list;
}

/**
 * SLDestroy will go through each node in the list and free
 * the data using the list->destructFunction, free the node
 * object itself, and then finally free the SortedList object
 */
void SLDestroy(SortedListPtr list)
{
	if (list != NULL)
	{
		if (list->head != NULL)
		{
			struct Node *cursor, *next;
			for (cursor = list->head; cursor != NULL; cursor = next)
			{
				// store the next value of the cursor
				next = cursor->next;
				(list->destructFunction)(cursor->data);
				free(cursor);
			}
		}
		free(list);
	}
}

void SLSwitch(struct Node *a, struct Node *b)
{
	if (a != NULL && b != NULL)
	{
		if (a->prev != NULL)
			a->prev->next = b;

		if (b->next != NULL)
			b->next->prev = a;

		b->prev = a->prev;
		a->prev = b;
		a->next = b->next;
		b->next = a;
	}
}

void SLSort(SortedListPtr list, struct Node *currentNode)
{
	if (list != NULL && currentNode != NULL)
	{
		if (currentNode->prev != NULL)
		{
			// if in incorrect order, switch and recheck
			if ((list->compareFunction)(currentNode->prev->data, currentNode->data) > 0)
			{
				if (currentNode->prev == list->head)
					list->head = currentNode;
				SLSwitch(currentNode->prev, currentNode);
				SLSort(list, currentNode);
			}
		}
	}
}

struct Node *SLInsert(SortedListPtr list, void *newObj)
{
	if (list == NULL)
		printf("WHATTTTTTTuTTTT\n");

	struct Node *returnNode = NULL;

	if (list->head == NULL)
	{
		list->head = (struct Node*)malloc(sizeof(struct Node));
		list->head->referenceCount = 1;
		list->head->next = NULL;
		list->head->data = newObj;
		if (list->incrementFunction != NULL)
			(list->incrementFunction)(list->head->data);
		returnNode = list->head;
	}
	else
	{
		struct Node* prev = NULL;
		struct Node* cursor = list->head;

		int nodePositionFound = 0;
		
		// while the cursor is not null and the data within cursor is
		// greater than the data in the new object
		int comparison = (list->compareFunction)(cursor->data, newObj);
		if (list->incrementFunction == NULL && comparison < 0)
			nodePositionFound = 1;
		// while (cursor != NULL && comparison >= 0)
		while (cursor != NULL && comparison != 0 && nodePositionFound == 0)
		{
			prev = cursor;
			cursor = cursor->next;
			
			if (cursor != NULL)
			{
				comparison = (list->compareFunction)(cursor->data, newObj);
				if (list->incrementFunction == NULL && comparison < 0)
					nodePositionFound = 1;
			}
		}

		// if repeated node is found
		if (comparison == 0 && nodePositionFound == 0)
		{
			// this destruct assumes that the new Obj given has been malloc'd
			(list->destructFunction)(newObj);
			returnNode = cursor;

			if (list->incrementFunction != NULL)
			{
				(list->incrementFunction)(cursor->data);
				SLSort(list, cursor);
			}
		}
		else
		{
			struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));
			newNode->referenceCount = 1;
			newNode->next = cursor;
			newNode->data = newObj;

			if (list->incrementFunction != NULL)
				(list->incrementFunction)(newNode->data);

			// if replacing head
			// if (cursor->prev == NULL)
			if (prev == NULL)
				list->head = newNode;
			else
			{
				newNode->prev = prev;
				prev->next = newNode;
			}
			
			if (cursor != NULL)
				cursor->prev = newNode;

			returnNode = newNode;
		}
	}
	return returnNode;
}

int SLRemove(SortedListPtr list, void *newObj)
{
	int removed = 0;
	if (list->head != NULL)
	{
		struct Node* cursor = list->head;
		// struct Node* prev = NULL;
		int comparison = (list->compareFunction)(cursor->data, newObj);

		while (cursor != NULL && comparison != 0)
		{
			// prev = cursor;
			cursor = cursor->next;
			if (cursor != NULL)
				comparison = (list->compareFunction)(cursor->data, newObj);
		}

		// if the element was found, the
		// compare fuction should've returned 0
		if (comparison == 0)
		{
			// decrement the reference count of
			// the node that will be removed
			cursor->referenceCount--;

			// if head should be removed, advance head
			// if (prev == NULL)
			if (cursor->prev == NULL)
				list->head = list->head->next;
			else
			{
				// prev->next = cursor->next;
				cursor->prev->next = cursor->next;

				// if cursor->next pointed to an existing node,
				// increment that node's referenceCount
				if (cursor->next != NULL)
				{
					cursor->next->prev = cursor->prev;
					cursor->next->referenceCount++;
				}
				
			}

			if (cursor->referenceCount == 0)
			{
				// if the current cursor was pointing to a node
				// and it was not previously the head (to make
				// sure that reference count is not decremented if
				// the head head was advanced)
				// if (cursor->next != NULL && prev != NULL)
				if (cursor->next != NULL && cursor->prev != NULL)
					cursor->next->referenceCount--;
				(list->destructFunction)(cursor->data);
				free(cursor);
			}

			removed = 1;
		}
	}
	return removed;
}

SortedListIteratorPtr SLCreateIterator(SortedListPtr list)
{
	SortedListIteratorPtr iterator = (SortedListIteratorPtr)malloc(sizeof(struct SortedListIterator));
	iterator->node = list->head;
	if (iterator->node != NULL)
		iterator->node->referenceCount++;

	iterator->destructFunction = list->destructFunction;
	return iterator;
}

void SLDestroyIterator(SortedListIteratorPtr iter)
{
	if (iter != NULL)
	{
		if(iter->node != NULL)
		{
			iter->node->referenceCount--;

			if (iter->node->referenceCount == 0)
			{
				(iter->destructFunction)(iter->node->data);
				free(iter->node);
			}
		}
		free(iter);
	}
}

void * SLGetItem( SortedListIteratorPtr iter )
{
	void* iterData = 0;
	if (iter != NULL)
	{
		if (iter->node != NULL)
		{
			iterData = iter->node->data;
		}
	}
	return iterData;
}

void * SLNextItem(SortedListIteratorPtr iter)
{
	void *item = NULL;
	if (iter != NULL)
	{
		if (iter->node != NULL)
		{
			// to hold the pointer to the previous node after
			// advancing the iterator (in case we need to free)
			struct Node* prevNode = iter->node;

			// advance iterator
			iter->node = iter->node->next;

			// decrement the reference count of previous
			// node because iterator has advanced and is
			// no longer pointing to that node
			prevNode->referenceCount--;

			// if the iterator was advanced and
			// still points to an existing node
			if (iter->node != NULL)
			{
				iter->node->referenceCount++;
				item = iter->node->data;
			}

			// check if the previous node needs to be freed
			if (prevNode->referenceCount == 0)
			{
				// decrement the reference count of this node since
				// deleting a node that points to it
				// don't need to do this if iterator points to NULL
				if (iter->node != NULL)
					iter->node->referenceCount--;
				// deleting node
				(iter->destructFunction)(prevNode->data);
				free(prevNode);
			}
		}
	}
	return item;
}
