#ifndef TOKENIZER_H
#define TOKENIZER_H
#include <stdlib.h>
#include <stdio.h>
#include "hashtable.h"

/**
 * tokenizeFile takes in a file pointer, the current filename, the
 * max size of the file and the Hashtable pointer that the tokens
 * will be passed to.
 */
int tokenizeFile(FILE *fp, const char *filename, size_t max_size, Hashtable *ht);

#endif