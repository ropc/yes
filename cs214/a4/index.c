#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include "hashtable.h"
#include "tokenizer.h"
#include "sorted-list.h"

int recurseDir(const char *dir, Hashtable *ht, const char *path, SortedListPtr fileList)
{
	int numFiles = 0;
	DIR *dp;
	struct stat buffer;

	if ((dp = opendir(dir)) == NULL)
	{
		if (errno == ENOTDIR)
		{
			// printf("%s is not a directory\n", dir);
			FILE *file = fopen(dir, "r");
			lstat(dir, &buffer);
			if (file != NULL)
				tokenizeFile(file, dir, buffer.st_size, ht);
			else
				printf("cannot open %s\n", dir);

		}
		else
			printf("%s: %s\n", dir, strerror(errno));
	}
	else
	{
		// printf("a:%d, z:%d, A:%d, Z:%d, 0:%d, 9:%d\n",
		// 	(int)'a', (int)'z', (int)'A', (int)'Z', (int)'0', (int)'9');
		chdir(dir);
		struct dirent *entry;
		
		while ((entry = readdir(dp)) != NULL)
		{
			lstat(entry->d_name, &buffer);
			if (S_ISDIR(buffer.st_mode) &&
				strcmp(entry->d_name, ".") != 0 &&
				strcmp(entry->d_name, "..") != 0)
			{
				// printf("dir: %s\n", entry->d_name);
				if (path == NULL)
					numFiles += recurseDir(entry->d_name, ht, entry->d_name, fileList);
				else
				{
					char newPath[strlen(path) + strlen(entry->d_name) + 2];
					strcpy(newPath, path);
					newPath[strlen(path)] = '/';
					newPath[strlen(path) + 1] = '\0';
					strcat(newPath, entry->d_name); 
					numFiles += recurseDir(entry->d_name, ht, dir, fileList);
				}
			}
			else if (S_ISREG(buffer.st_mode))
			{
				// printf("file: %s size: %ld\n",
				// 	entry->d_name, buffer.st_size);
				FILE *currentFile = fopen(entry->d_name, "r");
				if (currentFile != NULL && buffer.st_size > 0)
				{
					if (path == NULL)
					{
						int numTokens = tokenizeFile(currentFile, entry->d_name, buffer.st_size, ht);

						if (numTokens > 0)
						{
							HTToken *newFileToken = HTTokenCreate(entry->d_name);
							SLInsert(fileList, newFileToken);
							numFiles++;
						}
					}
					else
					{
						char filePath[strlen(path) + strlen(entry->d_name) + 2];
						strcpy(filePath, path);
						filePath[strlen(path)] = '/';
						filePath[strlen(path) + 1] = '\0';
						strcat(filePath, entry->d_name);
						// printf("working on %s\n", filePath);
						int numTokens = tokenizeFile(currentFile, filePath, buffer.st_size, ht);

						if (numTokens > 0)
						{
							HTToken *newFileToken = HTTokenCreate(filePath);
							SLInsert(fileList, newFileToken);
							numFiles++;
						}
					}
				}
				fclose(currentFile);
			}
		}
		chdir("..");
	}
	closedir(dp);
	return numFiles;
}

FILE *checkFile(const char *filename)
{
	// check if the file that you're trying to write to exists
	FILE *fp = NULL;
	int overwrite = 0;
	struct stat buffer;
	lstat(filename, &buffer);
	if (S_ISREG(buffer.st_mode))
	{
		char answer;
		int vaildAnswer = 0;
		printf("%s is an existing file, do you want to overwrite this file? (y/n)\n",
			filename);

		scanf("%c", &answer);
		if (answer == 'y' || answer == 'Y' || answer == 'n' || answer == 'N')
			vaildAnswer = 1;

		while (vaildAnswer == 0)
		{
			printf("%c is not a valid option.Do you want to overwrite %s? (y/n)\n",
				answer, filename);

			// to get one char at a time
			if (1 == scanf(" %c", &answer))
			{
				if (answer == 'y' || answer == 'Y' || answer == 'n' || answer == 'N')
					vaildAnswer = 1;
			}
		}

		if (answer == 'y' || answer == 'Y')
			overwrite = 1;
		else
			printf("Will not overwrite file: %s. Exiting\n", filename);
	}
	
	if(errno == ENOENT || overwrite == 1)
	{
		fp = fopen(filename,"w+");
		if (fp == NULL)
			printf("error opening/ creating file\n");
	}
	return fp;
}

void WriteStats(FILE *fp, SortedListPtr fileList, int numFiles)
{
	int i = 0;
	HTToken *currentFile;
	struct Node *currentNode = fileList->head;

	fprintf(fp, "<fileCount>%d</fileCount>\n", numFiles);

	for (currentNode = fileList->head; currentNode != NULL; currentNode = currentNode->next)
	{
		currentFile = (HTToken*)currentNode->data;
		fprintf(fp, "<file>%s\n%d</file>\n", currentFile->name, i++);
	}
	fflush(fp);
}

int main(int argc, char const *argv[])
{
	if (argc == 3)
	{
		FILE *writeToFile = checkFile(argv[1]);
		if (writeToFile != NULL)
		{
			Hashtable *ht = HTCreate();
			SortedListPtr fileList = SLCreate(HTTokenCmp, HTTokenDst, NULL);
			int numFiles = recurseDir(argv[2], ht, NULL, fileList);
			// HTPrint(ht);
			WriteStats(writeToFile, fileList, numFiles);
			HTWriteTo(ht, writeToFile);
			SLDestroy(fileList);
			HTDestroy(ht);
			fclose(writeToFile);
		}
	}
	else
	{
		printf("Need exactly 2 arguments (%d given)\n", argc - 1);
	}
	return 0;
}