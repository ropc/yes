#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include "mask.h"

// do i really need this???
void not(Mask *m)
{
	if (m == NULL)
		return;

	int bytes = m->bits / 8;

	if (m->bits % 8 > 0)
		bytes++;

	int i;
	for (i = 0; i < bytes; i++)
		m->mask[i] = ~(m->mask[i]);
}

char or(char *a, char *b)
{
	return (*a) | (*b);
}

char and(char *a, char *b)
{
	return (*a) & (*b);
}

char xor(char *a, char *b)
{
	return (*a) ^ (*b);
}

Mask *bitOpMask(const Mask *m1, const Mask *m2, const bitOp op)
{
	if (m1 == NULL || m2 == NULL || op == NULL)
		return NULL;

	if (m1->bits != m2->bits)
		return NULL;

	int bytes = m1->bits / 8;

	if (m1->bits % 8 > 0)
		bytes++;

	Mask *result = (Mask*)malloc(sizeof(Mask));
	result->mask = (char*)malloc(bytes);
	result->bits = m1->bits;

	int i;
	for (i = 0; i < bytes; i++)
		result->mask[i] = (*op)(&(m1->mask[i]), &(m2->mask[i]));
	
	return result;
}

void bitOperation(Mask *dest, Mask *m, const bitOp op)
{
	if (dest == NULL || m == NULL || op == NULL)
		return;

	if (dest->bits != m->bits)
		return;

	int bytes = dest->bits / 8;

	if (dest->bits % 8 > 0)
		bytes++;

	int i;
	for (i = 0; i < bytes; i++)
		dest->mask[i] = (*op)(&(dest->mask[i]), &(m->mask[i]));
}

// for testing
char *realand(const char *bitArray1, const char *bitarray2, int size)
{
	char *result = (char*)malloc(size);
	int i;
	for (i = 0; i < size; i++)
		result[i] = bitArray1[i] & bitarray2[i];
	
	return result;
}

// for testing
void showBits(const Mask *m)
{
	if (m == NULL)
		return;

	int bytes = m->bits / 8;

	if (m->bits % 8 > 0)
		bytes++;

	int i;
	for (i = 0; i < bytes; i++)
	{
		int j;
		for (j = (sizeof(char) * 8) - 1; j >= 0; j--)
		{
			if ((m->mask[i] >> j) & 1)
				printf("1");
			else
				printf("0");
		}
		printf(" ");
	}
	printf("\n");
}

char twoPower(int power)
{
	int i;
	unsigned char result = 1;
	for (i = 0; i < power; i++)
		result *= 2;

	return result;
}

Mask *CreateCleanMask(int bits)
{
	Mask *newMask = (Mask*)malloc(sizeof(Mask));
	newMask->bits = bits;

	int bytes = newMask->bits / 8;

	if (newMask->bits % 8 > 0)
		bytes++;

	newMask->mask = (char*)calloc(bytes, sizeof(char));

	return newMask;
}

Mask *CreateMask(int number, int bits)
{
	Mask *newMask = (Mask*)malloc(sizeof(Mask));
	newMask->bits = bits;

	int bytes = newMask->bits / 8;

	if (newMask->bits % 8 > 0)
		bytes++;

	newMask->mask = (char*)calloc(bytes, sizeof(char));
	// printf("maxfiles:%d, size:%d\n", maxFiles, size);

	// i is the current byte starting from the least significant (rightmost)
	int i;
	for (i = 1; i <= bytes; i++)
	{
		if (number / (8 * i) == 0)
		{
			newMask->mask[bytes - i] = twoPower(number % 8);
			break;
		}
	}

	return newMask;
}

Mask *CopyMask(const Mask *m)
{
	Mask *newMask = (Mask*)malloc(sizeof(Mask));
	newMask->bits = m->bits;

	int bytes = newMask->bits / 8;

	if (newMask->bits % 8 > 0)
		bytes++;

	newMask->mask = (char*)malloc(sizeof(char) * bytes);
	
	int i;
	for (i = 0; i < bytes; i++)
		newMask->mask[i] = m->mask[i];

	return newMask;
}

void DestroyMask(Mask *m)
{
	if (m->mask != NULL)
		free(m->mask);

	free(m);
}
