#ifndef SEARCH_H
#define SEARCH_H
#include <stdlib.h>
#include <stdio.h>
#include "hashtable.h"
#include "sorted-list.h"

// void readIndex(FILE *indexfp, Hashtable *fileHT);

int getFilename(char *filenameDest, const char *str, int *lastPosition);

#endif