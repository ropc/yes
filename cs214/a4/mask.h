#ifndef MASK_H
#define MASK_H

typedef struct mask {
	char *mask;
	unsigned int bits;	// # of useful/revelent bits in mask
} Mask;

typedef char (*bitOp)(const char *a, const char *b);

Mask *CreateMask(int number, int maxFiles);

Mask *CreateCleanMask(int maxFiles);

Mask *CopyMask(const Mask *m);

void DestroyMask(Mask *m);

void bitOperation(Mask *dest, Mask *m, const bitOp op);

Mask *bitOpMask(const Mask *m1, const Mask *m2, const bitOp op);

void not(Mask *m);

char or(char *a, char *b);

char and(char *a, char *b);

char xor(char *a, char *b);

void showBits(const Mask *m);

char twoPower(int power);

#endif