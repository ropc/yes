#include <errno.h>
#include <string.h>
#include "search.h"
#include "hashtable.h"
#include "sorted-list.h"
#include "mask.h"

int getFilename(char *filenameDest, const char *str, int *lastPosition)
{
	int returnInt = 0;
	int i = 0;
	// int startPosition = *lastPosition;
	char currentChar;
	// printf("position is %d\n", *lastPosition);
	while ((currentChar = str[*lastPosition]) != '\0')
	{
		// printf("chekcing char: %c\n", currentChar);
		(*lastPosition)++;
		if (currentChar != '\t' && currentChar != '\n')
			filenameDest[i++] = currentChar;
		else if (i > 0)
		{
			filenameDest[i] = '\0';
			returnInt = 1;
			break;
		}
		else
		{
			// printf("currentChar IS: %x\n", currentChar);
			// if (currentChar != '\t' && i <= 0)
			if (*lastPosition >= strlen(str))
			{
				filenameDest[0] = '\0';
				break;
			}
		}
	}

	return returnInt;
}

int getWord(char *nameDest, const char *str, int *lastPosition)
{
	int returnInt = 0;
	int i = 0;
	// int startPosition = *lastPosition;
	char currentChar;
	// printf("position is %d\n", *lastPosition);
	while ((currentChar = str[*lastPosition]) != '\0')
	{
		// printf("chekcing char: %c\n", currentChar);
		(*lastPosition)++;
		if (currentChar != ' ' && currentChar != '\n')
			nameDest[i++] = currentChar;
		else if (i > 0)
		{
			nameDest[i] = '\0';
			returnInt = 1;
			break;
		}
		else
		{
			// printf("currentChar IS: %x\n", currentChar);
			// if (currentChar != '\t' && i <= 0)
			if (*lastPosition >= strlen(str))
			{
				nameDest[0] = '\0';
				break;
			}

		}
	}

	return returnInt;
}

// can add lstat info here
char **readIndex(FILE *indexfp, Hashtable *wordHT, int *numFiles)
{
	if (indexfp == NULL || wordHT == NULL || numFiles == NULL)
		return NULL;

	char **fileNameArray;
	int fileCount = 0;
	int inFilenameBlock = 0;
	char *str = (char*)malloc(sizeof(char) * 100);
	char *filename = (char*)malloc(sizeof(char) * 100);
	char *word = (char*)malloc(sizeof(char) * 100);

	HTToken *currentWord = NULL;
	Hashtable *fileHT = HTCreate();

	// int maskSize = 0;

	while (fgets(str, 100, indexfp))
	{
		if (inFilenameBlock == 1)
		{
			int fileNum;
			sscanf(str, "%d</file>", &fileNum);
			// printf("filenumber: %d\n", fileNum);

			// printf("filename: %s, mask: %s\n", filename, mask);
			HTToken *file = HTAdd(fileHT, filename);
			file->data = CreateMask(fileNum, fileCount);
			file->destructFunct = (void*)DestroyMask;

			char *nameCopy = (char*)malloc(sizeof(char) * strlen(filename) + 1);
			strcpy(nameCopy, filename);
			fileNameArray[fileNum] = nameCopy;
			inFilenameBlock = 0;
		}
		else if (strncmp(str, "<file>", 6) == 0)
		{
			// sscanf(str, "<file>%s", filename);
			// printf("filename: %s\n", filename);
			int i = 6;
			getFilename(filename, str, &i);
			inFilenameBlock = 1;
		}
		else if (strncmp(str, "<list>", 6) == 0)
		{
			sscanf(str, "<list>%s", word);
			currentWord = HTAdd(wordHT, word);
			currentWord->data = CreateCleanMask(fileCount);
			currentWord->destructFunct = (void*)DestroyMask;
			// showBits(mask);
			// printf("word:'%s'\n", word);
		}
		else if (strncmp(str, "<fileCount>", 11) == 0)
		{
			sscanf(str, "<fileCount>%d</fileCount>\n", &fileCount);
			// if ((fileCount % 8) > 0)
			// 	maskSize = (fileCount / 8) + 1;
			// else
			// 	maskSize = fileCount / 8;
			fileNameArray = (char **)calloc(fileCount, sizeof(char*));
			// printf("file count: %d\n", fileCount);
		}
		else if (strncmp(str, "</list>", 7) != 0)
		{
			int j = 0;
			while (getFilename(filename, str, &j) > 0)
			{
				// printf("FILENAME:'%s'\n", filename);
				HTToken *fileT = HTFind(fileHT, filename);
				if (fileT != NULL)
					bitOperation(currentWord->data, fileT->data, (bitOp)or);
			}
		}
	}
	*numFiles = fileCount;

	HTDestroy(fileHT);
	free(word);
	free(filename);
	free(str);
	return fileNameArray;
}

void printFilenames(const Mask *mask, char **fileNameArray)
{
	int bytes = mask->bits / 8;

	if (mask->bits % 8 > 0)
		bytes++;

	int num = 0;

	int i;
	for (i = 0; i < bytes; i++)
	{
		int j = (sizeof(char) * 8) - 1;

		// gets the file #, if that is higher than fileCount, then decrese j
		// this allows for fewer checks and prevents segfaults
		if ((8*(bytes - 1 - i)) + j > mask->bits)
			j = ((mask->bits - 1) % 8);

		for (; j >= 0; j--)
		{
			if ((mask->mask[i] >> j) & 1)
			{
				printf("%s\n", fileNameArray[(8*(bytes - 1 - i)) + j]);
				num++;
			}
		}
	}

	if (num == 0)
		printf("No files satisfy query.\n");
}

int main(int argc, char const *argv[])
{
	if (argc != 2)
	{
		printf("Need exactly 1 argument (%d given)\n", argc - 1);
		return 1;
	}
	
	int fileCount;
	FILE *indexfp = fopen(argv[1], "r");

	if (indexfp == NULL)
	{
		printf("%s: %s\n", argv[1], strerror(errno));
		return 1;			
	}

	Hashtable *wordHT = HTCreate();
	
	char **fileNameArray = readIndex(indexfp, wordHT, &fileCount);

	// int i;
	// for (i = 0; i < 10; ++i)
	// {
	// 	printf("filename:%s\n", fileNameArray[]);
	// }

	// start checking for input from user
	char *input = (char*)malloc(sizeof(char) * 100);
	while (1)
	{
		printf("Please enter a query:\n");
		fgets (input, 100, stdin);
		// printf("your input:%s\n", input);
		
		// j is the starting position on the input string
		int j = 3;
		int isNot = 0;
		bitOp operation = NULL;

		if (strncmp(input, "sa ", 3) == 0)
			operation = (bitOp)and;
		else if (strncmp(input, "so ", 3) == 0)
			operation = (bitOp)or;
		else if (strncmp(input, "snand ", 6) == 0)
		{
			operation = (bitOp)and;
			j = 6;
			isNot = 1;
		}
		else if (strncmp(input, "snor ", 5) == 0)
		{
			operation = (bitOp)or;
			j = 5;
			isNot = 1;
		}
		else if (strncmp(input, "sxor ", 5) == 0)
		{
			operation = (bitOp)xor;
			j = 5;
		}
		else if(strncmp(input, "q", 1) == 0)
			break;
		else
		{
			printf("Invalid command, enter 'q' to quit.\n");
			continue;
		}

		// only valid operations make it this far
		
		// 	
		char *word = (char*)malloc(sizeof(char) * 100);
		Mask *mask = NULL;
		// while (sscanf(input, "%s", word) == 1)
		while (getWord(word, input, &j) > 0)
		{
			// printf("word:%s\n", word);
			HTToken *foundWord = HTFind(wordHT, word);
			if (foundWord != NULL)
			{
				// maskSize = foundWord->maskSize;
				if (mask == NULL)
					mask = CopyMask(foundWord->data);
				else
					bitOperation(mask, foundWord->data, operation);

				// printf("after word:%s mask:", word);
				// showBits(mask);
			}
			else 
			{
				Mask *tempMask = CreateCleanMask(fileCount);
				if (mask != NULL)
				{
					// if mask is not null, then simulate
					// (*operation)(mask, 0)
					bitOperation(mask, tempMask, operation);
					DestroyMask(tempMask);
				}
				else
					mask = tempMask;
			}
			// 	printf("cound not find word: %s\n", word);
		}

		if (mask != NULL)
		{
			if (isNot == 1)
				not(mask);

			// printf("final mask: ");
			// showBits(mask);

			printFilenames(mask, fileNameArray);
			DestroyMask(mask);
		}
		else
			printf("No files satisfy query.\n");

		free(word);

		// int a;
		// for (a = 0; a < fileCount; a++)
		// 	printf("file: %s\n", fileNameArray[a]);
	
	}
	free(input);

	HTDestroy(wordHT);

	int i;
	for (i = 0; i < fileCount; i++)
		free(fileNameArray[i]);
	free(fileNameArray);

	fclose(indexfp);	

	return 0;
}
