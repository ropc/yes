/*
 * tokenizer.c
 */
#include <stdio.h>
#include <stdlib.h>
//#include <assert.h>
#include <string.h>

/*
 * Tokenizer type.  You need to fill in the type as part of your implementation.
 */

struct TokenizerT_ {
	char *delimiters;
	char *stream;
	int streamLocation;
};

typedef struct TokenizerT_ TokenizerT;

/*
 * strEscape takes in a char array and returns a pointer to a malloc'd
 * piece of memory containing the escaped string.
 * This is done by going through each character, copying, and looking
 * for escape sequences in the string. if it finds an escape sequence
 * that was listed in the assignment document, then it replaces it
 * with the corresponding escaped character and shinks down the space
 * needed to hold the char array (since escape sequences are 2 chars
 * long and escape characters only take up 1 char in the array).
 * 
 * if the given string does not have an strlen() > 0
 * then a malloc'd piece of memory containing a single terminating
 * character ('\0') will be returned
 *
 * The pointer must be freed by the caller
 */
char *strEscape(char *text)
{
	char *str = (char *)malloc(sizeof(char) * (strlen(text) + 1));

	// if the input char array has a length greater
	// than 0, parse the string
	// if not, return a char* with just the terminating character
	if (strlen(text) > 0)
	{
		// flag to keep track of escapes ('\') at each location
		// 0 denotes no changed needed
		// 1 denotes that an escape sequence was found
		// 2 denotes that the character was replaced
		int escape[strlen(text)];
		// make sure that escape array is initialized at 0
		memset(escape, 0, sizeof(escape));

		// flag that will keep track of how many
		// changes to the char array need to be made
		int changes = 0;

		// will go through the char *text and find and replace
		// the listed escape characters
		int i;
		for (i = 0; *(text + i) != '\0'; i++)
		{
			// if the previous character was an escape character
			if (i > 0 && escape[i - 1] == 1)
			{
				// flag to see if a listed escape character
				// is found and memory removed
				escape[i] = 2;
				changes++;
				if (*(text + i) == 'n')
				{
					*(str + i) = '\n';
				}
				else if (*(text + i) == 't')
				{
					*(str + i) = '\t';
				}
				else if (*(text + i) == 'v')
				{
					*(str + i) = '\v';
				}
				else if (*(text + i) == 'b')
				{
					*(str + i) = '\b';
				}
				else if (*(text + i) == 'r')
				{
					*(str + i) = '\r';
				}
				else if (*(text + i) == 'f')
				{
					*(str + i) = '\f';
				}
				else if (*(text + i) == 'a')
				{
					*(str + i) = '\a';
				}
				else if (*(text + i) == '\\')
				{
					*(str + i) = '\\';
				}
				else if (*(text + i) == '\"')
				{
					*(str + i) = '\"';
				}
				else
				{
					escape[i] = 0;
					changes--;
				}

			}
			else if (*(text + i) == '\\')
			{
				escape[i] = 1;
				changes++;
			}
			
			// every character will be copied over on the first run
			// through the array, escape array will be used to
			// determine what can be removed from the array
			if (escape[i] != 2)
			{
				*(str + i) = *(text + i);
			}
		}

		// make the last character the terminating character
		*(str + strlen(text)) = '\0';

		// if any changes were made, go through the string again
		// to shink it down to its correct size
		if (changes > 0)
		{
			// reset changes to store the offset the current
			// character needs to be correctly shunken
			changes = 0;

			// go through each character and offset the characters
			// by one after an escape position (denoted by 1)
			// has been found.
			// this will essentially shift the characters over
			// to where the escape characters used to be
			for (i = 0; i <= strlen(text); i++)
			{
				// if an offset is needed
				if (changes > 0)
				{
					// if it is not the last character,
					// shift over the character
					// if it is the last character, ensure
					// the terminating character is placed
					if (i < strlen(text))
					{
						*(str + i - changes) = *(str + i);
					}
					else
					{
						*(str + i - changes) = '\0';
					}
				}
				// if the current character is an escape
				// character that needs to be removed
				if (escape[i] == 1)
				{
					changes++;
				}
			}
			// once done shifting everything, reallocate the string
			// to its correct size (shinks down to correct length)
			str = (char *)realloc(str, sizeof(char) * (strlen(text) + 1 - changes));
		}
	}
	else
	{
		// str should already be the correct size
		str[0] = '\0';
	}
	
	return str;
}

/**
 * checkEscape will check if a single chararcter (the second in an
 * escape sequence) is an escape character if it is, then the hex
 * representation of the escape character will be returned as a
 * dynamically allocated string.
 * If the character is not recognized to be one of the escape sequences,
 * then it will simply return a null pointer.
 *
 * The (non-null) pointer returned must be freed by the caller.
*/
char *checkEscape(char *character)
{
	char *hex;

	// using two layers of if statements prevents the need to
	// malloc and then free memory if the character is not
	// a recognized escape character
	if (*character == '\n' || *character == '\t' || *character == '\v' ||
		*character == '\b' || *character == '\r' || *character == '\f' ||
		*character == '\a' || *character == '\\' || *character == '\"')
	{
		hex = (char *)malloc(sizeof(char) * 7);
		if (*character == '\n')
		{
			strcpy(hex, "[0x0a]");
		}
		else if(*character == '\t')
		{
			strcpy(hex, "[0x09]");
		}
		else if(*character == '\v')
		{
			strcpy(hex, "[0x0b]");
		}
		else if(*character == '\b')
		{
			strcpy(hex, "[0x08]");
		}
		else if(*character == '\r')
		{
			strcpy(hex, "[0x0d]");
		}
		else if(*character == '\f')
		{
			strcpy(hex, "[0x0c]");
		}
		else if(*character == '\a')
		{
			strcpy(hex, "[0x07]");
		}
		else if(*character == '\\')
		{
			strcpy(hex, "[0x5c]");
		}
		else if(*character == '\"')
		{
			strcpy(hex, "[0x22]");
		}
	}
	else
	{
		hex = NULL;
	}
	return hex;
}


/*
 * TKCreate creates a new TokenizerT object for a given set of separator
 * characters (given as a string) and a token stream (given as a string).
 * 
 * TKCreate should copy the two arguments so that it is not dependent on
 * them staying immutable after returning.  (In the future, this may change
 * to increase efficiency.)
 *
 * If the function succeeds, it returns a non-NULL TokenizerT.
 * Else it returns NULL.
 *
 * You need to fill in this function as part of your implementation.
 */

TokenizerT *TKCreate(char *separators, char *ts) {
	// allocate memory for the TokenizerT object and its char* fields
	TokenizerT *tokenizerPtr = (TokenizerT *)malloc(sizeof(TokenizerT));
	tokenizerPtr->delimiters = strEscape(separators);
	tokenizerPtr->stream = strEscape(ts);
	tokenizerPtr->streamLocation = 0;

	// if tokenizerPtr and its pointer contents are NULL,
	// add data to the TokenizerT object.
	// if not, then it will return the NULL pointer
	if (tokenizerPtr == NULL ||
		tokenizerPtr->delimiters == NULL ||
		tokenizerPtr->stream == NULL ||
		tokenizerPtr->streamLocation != 0)
	{
		free(tokenizerPtr);
		tokenizerPtr = NULL;
	}

	// return pointer to TokenizerT object
	return tokenizerPtr;
}

/*
 * TKDestroy destroys a TokenizerT object.  It should free all dynamically
 * allocated memory that is part of the object being destroyed.
 *
 * You need to fill in this function as part of your implementation.
 */

void TKDestroy(TokenizerT *tk) {
	free(tk->delimiters);
	free(tk->stream);
	free(tk);
}


/*
 * TKGetNextToken returns the next token from the token stream as a
 * character string.  Space for the returned token should be dynamically
 * allocated.  The caller is responsible for freeing the space once it is
 * no longer needed.
 *
 * If the function succeeds, it returns a C string (delimited by '\0')
 * containing the token.  Else it returns 0.
 *
 * You need to fill in this function as part of your implementation.
 */

char *TKGetNextToken(TokenizerT *tk) {
	char *token;
	// assume the streamEnd of the token is at the end of the string
	// plus one to give space for the terminating character
	int streamEnd = strlen(tk->stream) + 1;
	// the longest token size will be the length of the stream
	// that has not yet been parsed (assuming no escape characters found)
	// also includes the space for the escape character
	int tokenLength = streamEnd - tk->streamLocation;
	// to keep track of the current location of the token
	int tokenLocation = 0;

	// check if there is anything left in the stream to parse
	// if not return 0
	if ((streamEnd - tk->streamLocation) > 0)
	{
		// at most, the token will be the length of the rest of the string,
		// assuming no need to display escaped characters in hexadecimal
		char *tempToken = calloc(tokenLength, sizeof(char));
		int numSeparators = strlen(tk->delimiters);

		// flag for delimiter
		// 1 denotes that a delimiter was found and
		// 		need to increment tokenLocation
		// 2 denotes that a delimiter was found at the
		// 		beginning of a token and therefore 
		int delimiterFound = 0;

		// checks for every character after the last location known
		for (;tk->streamLocation < streamEnd && delimiterFound == 0; (tk->streamLocation)++)
		{
			if (tk->stream[tk->streamLocation] != '\0')
			{
				// checks for each separator
				int i;
				for (i = 0; i < numSeparators && delimiterFound == 0; i++)
				{
					// if a separator is found, it is the streamEnd of the token
					// also gets out of for and while loops
					if (tk->delimiters[i] == tk->stream[tk->streamLocation])
					{
						// no need to continue going through the separators
						delimiterFound = 1;
					}
				}
			}
			else
			{
				delimiterFound = 2;
			}

			if (delimiterFound == 0)
			{
				if (tokenLength == 1)
				{
					free(tempToken);
					tempToken = NULL;
					delimiterFound = 1;
				}
				else if ((tokenLocation + 1) == tokenLength)
				{
					tempToken[tokenLocation] = '\0';
				}
				else
				{
					char *hexString = checkEscape(&(tk->stream[tk->streamLocation]));

					if (hexString != NULL)
					{
						// replacing one escaped char (1)
						// with its hex representation (6)
						tokenLength += 5;
						tempToken = (char *)realloc(tempToken, tokenLength);
						strcat(tempToken, hexString);
						free(hexString);
						tokenLocation += 5;
					}
					else
					{
						tempToken[tokenLocation] = tk->stream[tk->streamLocation];
					}
				}
			}
			else
			{
				// if found a delimiter at the beginning of the string, continue
				// going through the rest of the stream
				if (tokenLocation == 0)
				{
					if (delimiterFound != 2)
					{
						delimiterFound = 0;
						continue;
					}
					else
					{
						free(tempToken);
						tempToken = NULL;
					}
				}
				else
				{
					// if not at the last character, reallocate to make
					// the string fit correctly
					if ((tokenLocation + 1) != tokenLength)
					{
						tokenLength = tokenLocation + 1;
						tempToken = (char *)realloc(tempToken,
							sizeof(char) * tokenLength);
					}
					tempToken[tokenLength] = '\0';
				}
			}

			// if a delimiter was not found, increment the tokenLocation
			if (delimiterFound == 0)
			{
				tokenLocation++;
			}
		}
		// if the tempToken was not null, return it,
		// otherwise return 0
		if (tempToken != NULL)
		{
			token = tempToken;
		}
		else
		{
			token = 0;
		}
	}
	else
	{
		token = 0;
	}

	return token;
}

/*
 * TKreset resets the counter (streamLocation)
 * in the TokenizerT struct to allow further use of the
 * tokenizer after the tokenizer object has returned all
 * of its tokens.
 * This allows the user to retrieve the tokens more than once
 */
void TKreset(TokenizerT *tk)
{
	tk->streamLocation = 0;
}

/*
 * main will have two string arguments (in argv[1] and argv[2]).
 * The first string contains the separator characters.
 * The second string contains the tokens.
 * Print out the tokens in the second string in left-to-right order.
 * Each token should be printed on a separate line.
 */

int main(int argc, char **argv) {

	if (argc == 3)
	{
		TokenizerT *tokenizer = TKCreate(argv[1], argv[2]);

		char* token = TKGetNextToken(tokenizer);
		while (token != NULL)
		{
			printf("%s\n", token);
			free(token);
			token = TKGetNextToken(tokenizer);
		}
		TKDestroy(tokenizer);
	}
	else
	{
		printf("Need exactly 2 arguments. (%d given)\n", (argc - 1));
	}

  return 0;
}
