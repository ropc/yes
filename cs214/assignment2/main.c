/*
 * sorted-list.c
 */
#include	<stdio.h>
#include	<string.h>
#include	"sorted-list.h"

int compareInts(void *p1, void *p2)
{
	int i1 = *(int*)p1;
	int i2 = *(int*)p2;

	return i1 - i2;
}

int compareDoubles(void *p1, void *p2)
{
	double d1 = *(double*)p1;
	double d2 = *(double*)p2;

	return (d1 < d2) ? -1 : ((d1 > d2) ? 1 : 0);
}

int compareStrings(void *p1, void *p2)
{
	char *s1 = p1;
	char *s2 = p2;

	return strcmp(s1, s2);
}

//Destructor functions
void destroyBasicTypeAlloc(void *p){
	//For pointers to basic data types (int*,char*,double*,...)
	//Use for allocated memory (malloc,calloc,etc.)
	free(p);
}

void destroyBasicTypeNoAlloc(void *p) {
	//For pointers to basic data types (int*,char*,double*,...)
	//Use for memory that has not been allocated (e.g., "int x = 5;SLInsert(mylist,&x);SLRemove(mylist,&x);")
	return;
}

int main()
{
	SortedListPtr list1 = SLCreate(compareInts, destroyBasicTypeAlloc);

	// adding things to list 1
	int *a1 = (int*)malloc(sizeof(int));
	*a1 = 5;
	SLInsert(list1, (void *)a1);
	int *b1 = (int*)malloc(sizeof(int));
	*b1 = 4;
	SLInsert(list1, (void *)b1);
	int *c1 = (int*)malloc(sizeof(int));
	*c1 = 1;
	SLInsert(list1, (void *)c1);
	int *d1 = (int*)malloc(sizeof(int));
	*d1 = 3;
	SLInsert(list1, (void *)d1);
	int *e1 = (int*)malloc(sizeof(int));
	*e1 = 2;
	SLInsert(list1, (void *)e1);
	int *f1 = (int*)malloc(sizeof(int));
	*f1 = 6;
	SLInsert(list1, (void *)f1);
	int *g1 = (int*)malloc(sizeof(int));
	*g1 = 7;
	SLInsert(list1, (void *)g1);
	int *h1 = (int*)malloc(sizeof(int));
	*h1 = 1;
	SLInsert(list1, (void *)h1);
	
	SortedListIteratorPtr iter1 = SLCreateIterator(list1);
	SortedListIteratorPtr iter2 = SLCreateIterator(list1);
	SLNextItem(iter1);
	SLNextItem(iter2);
	SLNextItem(iter2);
	SLNextItem(iter2);

	printf("list created and adding numbers 5, 4, 1, 3, 2, 6, 7, 1\n");

	printf("list after 2 iterators have been made and advanced\n(the first is advanced once and the second advanced 3 times):\n");
	struct Node *cursor;
	for (cursor = list1->head; cursor != NULL; cursor = cursor->next)
	{
		printf("data:%d, reference count:%d\n",
			*(int*)cursor->data, cursor->referenceCount);
	}

	printf("removing 7, 4, and 1\n");
	SLRemove(list1, (void *)g1);
	SLRemove(list1, (void *)b1);
	SLRemove(list1, (void *)c1);

	printf("list after removes:\n");
	for (cursor = list1->head; cursor != NULL; cursor = cursor->next)
	{
		printf("data:%d, reference count:%d\n",
			*(int*)cursor->data, cursor->referenceCount);
	}

	printf("printing list from iterator1:\n");
	int *data;
	for (data = (int*)SLGetItem(iter1); data != NULL; data = (int*)SLNextItem(iter1))
		printf("data: %d\n", *data);

	printf("printing list from iterator2:\n");
	// reset data
	data = NULL;
	for (data = (int*)SLGetItem(iter2); data != NULL; data = (int*)SLNextItem(iter2))
		printf("data: %d\n", *data);

	printf("list after 2 iterators have gone through the rest of the list:\n");
	for (cursor = list1->head; cursor != NULL; cursor = cursor->next)
	{
		printf("data:%d, reference count:%d\n",
			*(int*)cursor->data, cursor->referenceCount);
	}

	SLDestroyIterator(iter1);
	SLDestroyIterator(iter2);
	SLDestroy(list1);

	return 0;
}
