void *mymalloc2(unsigned int size)
{
	static struct mementry *root = 0, *last = 0;
	struct mementry *p, *succ;

	p = root;
	while(p != 0)
	{
		if (p->size <size || !p->isfree)
			p = p->succ;
		else if (p->size <(size + memsize))
		{
			p->isfree = 0;
			return (char*)p + memsize;
		}
		else
		{
			succ = (struct memenetry*)((char*)p + memsize + size)
			succ->prev = p;
			succ->succ = p->succ;
			if (p->succ != 0)
				p->succ->prev = succ;
			p->succ = succ;
			succ->size = p->size - memsize - size
			succ->isfree = 1;
			p->size = size;
			p->isfree 0 ;
			last = (p == last)?succ:last;
			return (char*)p + memsize;
		}

	}
}

#include <unistd.h>

void * sbrk(intptr_t incr)

// this would go in some function that will increase the heap size
{
	if ((p = (struct mementry*)sbrk(memsize + size)) == (void*)-1)
		return 0;
	else if (last == 0)
	{
		p->prev = p->succ = 0;
		p->size = size;
		p->isfree 0;
		root = last = p;
		return (char*)p + memsize;
	}
	else
	{
		p->prev = last;
		p->succ = last->succ;		// should be 0
		p->size = size;
		p->isfree = 0;
		last->succ = p;
		last = p;
		return (char*)p + memsize;
	}
}