// #include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <dirent.h>
// #include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "hashtable.h"
// #include "functions.h"
#include "tokenizer.h"

void recurseDir(const char *dir, Hashtable *ht, const char *path)
{
	DIR *dp;
	struct stat buffer;

	if ((dp = opendir(dir)) == NULL)
	{
		if (errno == ENOTDIR)
		{
			// printf("%s is not a directory\n", dir);
			FILE *file = fopen(dir, "r");
			lstat(dir, &buffer);
			if (file != NULL)
				tokenizeFile(file, dir, buffer.st_size, ht);
			else
				printf("cannot open %s\n", dir);

		}
		else
			printf("%s: %s\n", dir, strerror(errno));
	}
	else
	{
		// printf("a:%d, z:%d, A:%d, Z:%d, 0:%d, 9:%d\n",
		// 	(int)'a', (int)'z', (int)'A', (int)'Z', (int)'0', (int)'9');
		chdir(dir);
		struct dirent *entry;
		
		while ((entry = readdir(dp)) != NULL)
		{
			lstat(entry->d_name, &buffer);
			if (S_ISDIR(buffer.st_mode) &&
				strcmp(entry->d_name, ".") != 0 &&
				strcmp(entry->d_name, "..") != 0)
			{
				// printf("dir: %s\n", entry->d_name);
				if (path == NULL)
					recurseDir(entry->d_name, ht, entry->d_name);
				else
				{
					char newPath[strlen(path) + strlen(entry->d_name) + 2];
					strcpy(newPath, path);
					newPath[strlen(path)] = '/';
					newPath[strlen(path) + 1] = '\0';
					strcat(newPath, entry->d_name); 
					recurseDir(entry->d_name, ht, dir);
				}
			}
			else if (S_ISREG(buffer.st_mode))
			{
				// printf("file: %s size: %ld\n",
				// 	entry->d_name, buffer.st_size);
				FILE *currentFile = fopen(entry->d_name, "r");
				if (currentFile != NULL && buffer.st_size > 0)
				{
					if (path == NULL)
						tokenizeFile(currentFile, entry->d_name, buffer.st_size, ht);
					else
					{
						char filePath[strlen(path) + strlen(entry->d_name) + 2];
						strcpy(filePath, path);
						filePath[strlen(path)] = '/';
						filePath[strlen(path) + 1] = '\0';
						strcat(filePath, entry->d_name);
						// printf("working on %s\n", filePath);
						tokenizeFile(currentFile, filePath, buffer.st_size, ht);
					}						
				}
				fclose(currentFile);
			}
		}
		chdir("..");
	}
	closedir(dp);
}

FILE *checkFile(const char *filename)
{
	// check if the file that you're trying to write to exists
	FILE *fp = NULL;
	int overwrite = 0;
	struct stat buffer;
	lstat(filename, &buffer);
	if (S_ISREG(buffer.st_mode))
	{
		char answer;
		int vaildAnswer = 0;
		printf("%s is an existing file, do you want to overwrite this file? (y/n)\n",
			filename);

		scanf("%c", &answer);
		if (answer == 'y' || answer == 'Y' || answer == 'n' || answer == 'N')
			vaildAnswer = 1;

		while (vaildAnswer == 0)
		{
			printf("%c is not a valid option.Do you want to overwrite %s? (y/n)\n",
				answer, filename);

			// to get one char at a time
			if (1 == scanf(" %c", &answer))
			{
				if (answer == 'y' || answer == 'Y' || answer == 'n' || answer == 'N')
					vaildAnswer = 1;
			}
		}

		if (answer == 'y' || answer == 'Y')
			overwrite = 1;
		else
			printf("Will not overwrite file: %s. Exiting\n", filename);
	}
	
	if(errno == ENOENT || overwrite == 1)
	{
		fp = fopen(filename,"w+");
		if (fp == NULL)
			printf("error opening/ creating file\n");
	}
	return fp;
}

int main(int argc, char const *argv[])
{
	if (argc == 3)
	{
		FILE *writeToFile = checkFile(argv[1]);
		if (writeToFile != NULL)
		{
			Hashtable *ht = HTCreate();
			recurseDir(argv[2], ht, NULL);
			// HTPrint(ht);
			HTWriteTo(ht, writeToFile);
			HTDestroy(ht);
			fclose(writeToFile);
		}
	}
	else
	{
		printf("Need exactly 2 arguments (%d given)\n", argc - 1);
	}
	return 0;
}