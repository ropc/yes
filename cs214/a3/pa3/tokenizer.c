#include "tokenizer.h"
#include "hashtable.h"

void tokenizeFile(FILE *fp, const char *filename, size_t max_size, Hashtable *ht)
{
	// the max size of a string in a file is the size of the file itself
	// this char array is used again and again to store tokens. it will only
	// store one token at a time, which is passed to HTAdd() along with the
	// given Hashtable* and char *filename
	char *str = (char*)malloc(max_size);
	if (str == NULL)
		printf("Out of memory.\n");
	else
	{
		int currentChar = 0;
		long i = 0;

		// if (ferror(fp)) printf("wut\n");

		while ((currentChar = fgetc(fp)) != EOF)
		{
			if (('a' <= currentChar && currentChar <= 'z') ||
				('A' <= currentChar && currentChar <= 'Z') ||
				('0' <= currentChar && currentChar <= '9'))
			{
				str[i] = currentChar;

				// turn caps into lowercase
				if ('A' <= currentChar && currentChar <= 'Z')
					str[i] = str[i] + ('a' - 'A');

				i++;
			}
			else
			{
				str[i] = '\0';
				if (strlen(str) > 0)
				{
					// printf("here str: %s\n", str);
					HTAdd(ht, str, filename);
				}
				// to make sure above isn't executed twice
				str[0] = '\0';
				i = 0;
			}
		}
		free(str);
	}
}