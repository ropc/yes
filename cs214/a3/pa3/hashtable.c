#include <stdio.h>
#include <string.h>
#include "sorted-list.h"
#include "hashtable.h"

Hashtable *HTCreate()
{
	Hashtable *ht = (Hashtable*)malloc(sizeof(Hashtable));
	return ht;
}

void HTDestroy(Hashtable *ht)
{
	if (ht != NULL)
	{
		int i;
		for (i = 0; i < 36; i++)
		{
			if (ht->buckets[i] != NULL)
				SLDestroy(ht->buckets[i]);
		}
		free(ht);
	}
}

int Hash(const char *key)
{
	int index;
	if (strlen(key) > 0)
	{
		if ('a' <= key[0] && key[0] <= 'z')
			index = key[0] - 87;
		else if ('0' <= key[0] && key[0] <= '9')
			index = key[0] - 48;
		else
			index = -1;
	}
	else
		index = -1;

	return index;
}

void HTAdd(Hashtable *ht, const char *key, const char *file)
{
	if (ht != NULL && key != NULL && file != NULL && strlen(key) > 0)
	{
		int index = Hash(key);

		if (index != -1)
		{
			if (ht->buckets[index] == NULL)
				ht->buckets[index] = SLCreate(TokenCmp, TokenDst, NULL);

			Token *newToken = TokenCreate(key);
			struct Node *tokenNode = SLInsert(ht->buckets[index], (void *)newToken);

			newToken = (Token*)(tokenNode->data);
			if (newToken->records == NULL)
				newToken->records = SLCreate(RecordCmp, RecordDst, RecordInc);

			SLInsert(newToken->records, RecordCreate(file));
		}
		// else
		// 	printf("hash function failed.\n");
	}
}

void HTWriteTo(Hashtable *ht, FILE *fp)
{
	if (ht != NULL && fp != NULL)
	{
		int i;
		for (i = 0; i < 36; i++)
		{
			if (ht->buckets[i] != NULL)
			{
				// printf("%d: %x\n", i, (int)ht->buckets[i]);
				struct Node *tokenCursor = ht->buckets[i]->head;

				for (; tokenCursor != NULL; tokenCursor = tokenCursor->next)
				{
					fprintf(fp, "<list> %s\n", ((Token*)tokenCursor->data)->name);
					int j = 1;
					struct Node *recordCursor = ((Token*)tokenCursor->data)->records->head;
					for (;recordCursor != NULL; recordCursor = recordCursor->next)
					{
						char *format = " %s %d";

						if (j % 5 == 1)
						{
							if (recordCursor->next == NULL)
								format = "%s %d\n";
							else
								format = "%s %d";
						}
						else if (j % 5 == 0 || recordCursor->next == NULL)
							format = " %s %d\n";

						fprintf(fp, format, ((Record*)recordCursor->data)->filename,
							((Record*)recordCursor->data)->freq);
						j++;
					}
					fputs("</list>\n", fp);
				}
			}
		}
		fflush(fp);
		printf("Writing to file successful\n");
	}
}

Token *TokenCreate(const char *name)
{
	Token *t = (Token*)malloc(sizeof(Token));
	if (strlen(name) > 0)
	{
		t->name = (char*)malloc(sizeof(char) * (strlen(name) + 1));
		strcpy(t->name, name);
	}
	t->records = NULL;

	return t;
}

int TokenCmp(void *a, void *b)
{
	// since sorted list normally sorts from largest to smallest,
	// change the sign on this strcmp function to sort tokens from
	// smallest to largest (alphabetical order)
	return (-1 * strcmp((char*)((Token*)a)->name, (char*)((Token*)b)->name));
}

void TokenDst(void *a)
{
	if (a != NULL)
	{
		if (((Token*)a)->name != NULL)
			free(((Token*)a)->name);
		if (((Token*)a)->records != NULL)
			SLDestroy(((Token*)a)->records);
		free(a);
	}
}

Record *RecordCreate(const char *filename)
{
	Record *r = (Record*)malloc(sizeof(Record));
	if (strlen(filename) > 0)
	{
		r->filename = (char*)malloc(sizeof(char) * (strlen(filename) + 1));
		strcpy(r->filename, filename);
	}
	r->freq = 0;
	return r;
}

int RecordCmp(void *a, void *b)
{
	// return strcmp((char*)((Record*)a)->filename, (char*)((Record*)b)->filename);
	// if comparing against a new node, which should have freq == 0
	// check that the filename is the same
	// else compare thi file frequencies
	if (((Record*)b)->freq == 0 || ((Record*)a)->freq == 0)
		return strcmp((char*)((Record*)a)->filename, (char*)((Record*)b)->filename);
	else
		return ((Record*)b)->freq - ((Record*)a)->freq;
}

void RecordDst(void *a)
{
	if (a != NULL)
	{
		if (((Record*)a)->filename != NULL)
			free(((Record*)a)->filename);
		free(a);
	}
}

void RecordInc(void *a)
{
	if (a != NULL)
		((Record*)a)->freq++;
}
