#ifndef HASHTABLE_H
#define HASHTABLE_H
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sorted-list.h"

/**
 * Hashtable struct will hold lists of tokens,
 * each starting with the same letter or number.
 */
typedef struct hashtable
{
	SortedListPtr buckets[36];
} Hashtable;

/**
 * Record struct will hold the information for a filename
 * and the number of occurrences (freq). These will be in a
 * list within each Token.
 */
typedef struct Record
{
	char *filename;
	int freq;
} Record;

/**
 * Token struct will hold the name of the token and a pointer
 * to the list of records related to the token
 */
typedef struct Token
{
	char *name;
	SortedListPtr records;
} Token;

// create a Hashtable struct
Hashtable *HTCreate();

/**
 * HTDestroy frees the hashtable and calls
 * SLDestroy to every list within the hashtable
 */
void HTDestroy(Hashtable *ht);

/**
 * Hash takes in a string and generates a hash index for use
 * with the hashtable. It generates a number between 0-35 if
 * successful, or a -1 if unsuccessful
 */
int Hash(const char *key);

/**
 * HTAdd takes in a Hastable pointer, and char *key and *file
 * pointers to insert tokens with their related filename
 * into the Hashtable.
 */
void HTAdd(Hashtable *ht, const char *key, const char *file);

/**
 * HTWriteTo will taken in a Hashtable pointer and a FILE pointer
 * and will write out the given Hashtable into the FILE pointer
 * using the format given by the assignment
 */
void HTWriteTo(Hashtable *ht, FILE *fp);

/**
 * tokenCreate will allocate and initialize all the memory
 * associated with a Token struct
 */
Token *TokenCreate(const char *name);

/**
 * function to compare tokens
 */
int TokenCmp(void *a, void *b);

/**
 * function to destroy tokens
 */
void TokenDst(void *a);

/**
 * recordCreate will allocate and initialize all the memory
 * associated with a Token struct
 */
Record *RecordCreate(const char *filename);

/**
 * function to compare Records
 * since all records are initialized with 0 frequency,
 * it uses this knowledge to know when to compare by filename
 * or by frequency (filename to find duplicates and 
 * frequency to figure out the correct order in the list)
 */
int RecordCmp(void *a, void *b);

/**
* function to destroy records
 */
void RecordDst(void *a);

/**
 * function to increment records
 */
void RecordInc(void *a);

#endif