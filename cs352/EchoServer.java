import java.net.Socket;
import java.net.ServerSocket;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class EchoServer {

    public static void main(String[] args) throws Exception {
        ServerSocket serverSocket = null;
        if (args.length == 1) {
            int port = Integer.parseInt(args[0]);
            try {
                serverSocket = new ServerSocket(port);
                System.out.println("Started server on port " + port);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("EchoServer takes exactly 1 argument: port number");
        }

        // repeatedly wait for connections, and process
        if (serverSocket != null) {
            // a "blocking" call which waits until a connection is requested
            Socket clientSocket = serverSocket.accept();
            System.out.println("client connected");
            // open up IO streams
            BufferedReader input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            PrintWriter output = new PrintWriter(clientSocket.getOutputStream(), true);
            // waits for data and reads it in until connection dies
            String text = null;
            try {
                while ((text = input.readLine()) != null) {
                    // System.out.println(text);
                    output.println((new StringBuffer(text)).reverse());
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            System.out.println("client disconnected");
            // close IO streams, then socket
            input.close();
            output.close();
            clientSocket.close();
            serverSocket.close();
            System.out.println("server closed");
        }
    }
}

