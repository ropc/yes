#include "dictstat.h"
#include "trie.h"

Trie *dictTrie;

int fileAction(FILE *fp, trieAction action)
{
	char *str = (char*)calloc(100, sizeof(char));
	int fileRead = 0;
	while (fscanf(fp, "%s", str) != EOF)
	{
		fileRead = 1;
		int start = 0;
		int wordFound = 0;
		int i;

		for (i = 0; i <= strlen(str); i++)
		{
			// make all uppercase letters lower case
			if ('A' <= str[i] && str[i] <= 'Z')
				str[i] = str[i] + ('a' - 'A');
			
			// if it is a letter a-z, start keeping
			// track of the indexes.
			if ('a' <= str[i] && str[i] <= 'z')
			{
				if (wordFound == 0)
				{
					start = i;
					wordFound = 1;
				}
			}
			else if (wordFound == 1)
			{
				
				int j;
				int wordLength = i - start;
				char *word = (char*)calloc(wordLength, sizeof(char));
				
				for (j = 0; j < wordLength; j++)
				{
					word[j] = str[start];
					start++;
				}

				if (dictTrie == NULL)
					dictTrie = TrieCreate();
				
				(*action)(dictTrie, word);

				free(word);
				wordFound = 0;
			}
		}
		free(str);
		str = (char*)calloc(100, sizeof(char));
	}
	free(str);
	return fileRead;
}

void readDict(FILE *dict_file)
{
	int wasRead = fileAction(dict_file, (trieAction)TrieAdd);
	if (wasRead == 0)
		printf("empty dictionary\n");
}

void scanData(FILE *data_file)
{
	fileAction(data_file, (trieAction)TrieIsMember);
}

void printStats()
{
	InterfaceNode *list, *cursor;
	list = TrieGetKeyNodes(dictTrie);
	for (cursor = list; cursor != NULL; cursor = cursor->next)
	{
		printf("%s %d %d %d\n",
			cursor->key, cursor->occurrence, cursor->prefix, cursor->superWord);
	}
	INodeListDestroy(list);
}

void clearDict()
{
	TrieDestroy(dictTrie);
}