#include "dictstat.h"

int main(int argc, char const *argv[])
{
	/**
	 * note: put this crap in th readme:
	 * run
	 * list
	 * break
	 * print
	 * x
	 * info registers
	 * run file1 file2
	 * print 
	 */
	if (argc == 3)
	{
		FILE *dict_file, *data_file;
		dict_file = fopen(argv[1], "r");
		data_file = fopen(argv[2], "r");

		readDict(dict_file);

		scanData(data_file);

		printStats();

		clearDict();

		fclose(dict_file);
		fclose(data_file);
	}
	else
		printf("invalid input\n");

	return 0;
}
