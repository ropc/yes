#ifndef TRIE_H
#define TRIE_H
#include <stdlib.h>
#include <string.h>

/**
 * InterfaceNode will provide a way for the caller to take a
 * look at a specific word (or key) in a Trie and its related
 * statistics without being able to actually modify anything
 * within the actual trie.
 */
typedef struct InterfaceNode {
	char *key;
	int occurrence;
	int prefix;
	int superWord;
	struct InterfaceNode *next;
} InterfaceNode;

/**
 * Stats will provide a location to store statistcs for a node
 * without needing to allocate extra space for each statistic
 * in everyone (since it will not always be needed)
 */
typedef struct Stats {
	int occurrence;
	int prefix;
	int superWord;
} Stats;

/**
 * Node is a struct used by the trie. it keeps track of the key
 * (as a char) and pointers to the next sibling node (next),
 * and its first child (children) as well as the statistics
 * for the related node, if applicable.
 */
typedef struct Node {
	char key;
	struct Node *next, *children;
	Stats *nodeStats;
} Node;

/**
 * Trie points to the root node of the Trie made up of Node stucts
 */
typedef struct Trie {
	Node *root;
} Trie;

/**
 * INodeCreate will allocate space for an IntefaceNode
 * stuct and return a pointer pointing to the allocated
 * memory associated with the struct created.
 *
 * If a key is given, it will initialize the InterfaceNode
 * with that key, else the key will be an empty char*
 */
InterfaceNode *INodeCreate(const char *key);

/**
 * INodeDestroy will free the memory associated to a given
 * InterfaceNode struct.
 */
void INodeDestroy(InterfaceNode *node);

/**
 * NodeCreate simply allocates the memory for a single
 * node conating a char key and then returns a pointer
 * to the initialized Node object.
 */
Node *NodeCreate(const char key);

/**
 * NodeRecursiveDestroy will take in a node object
 * and recursively free the memory for all the next
 * and children nodes from that node. This will be 
 * used by TrieDestroy.
 */
void NodeRecursiveDestroy(Node *currentNode);

/**
 * TrieCreate will allocate space for a Trie struct
 * and return a pointer to that struct
 */
Trie *TrieCreate();

/**
 * Trie Destroy will call NodeRecursiveDestroy()
 * on the root node of the Trie in order to destroy the Trie
 */
void TrieDestroy(Trie *triePtr);

/**
 * StatsInitialize wil allocate space for a Stats struct
 * and return a pointer to that struct
 */
Stats *StatsInitialize();

/**
 * TrieAdd will take in a Trie* and will add
 * the given key to the trie.
 * It will keep the keys of the Trie in alphabetical
 * order.
 */
void TrieAdd(Trie *triePtr, const char* key);

/**
 * TrieIsMember will take in a key and update the
 * statistics of the Nodes in the Trie in accordance
 * to the given key. So if it finds that the key is
 * also a key in the Trie, it will increment the
 * number of occurrences for that given node.
 * 
 * It will return 0 if the given key
 * cannot be found in the given trie.
 * 
 * If the given key is found, it will return some
 * positive number indicating the value for that key
 * 
 * If the given key is a prefix of an actual key in
 * the trie, TrieIsMember will return -1.
 *
 * If the given key is a superword of an actual key
 * in the trie, TrieIsMember will return -2.
 *
 */
int TrieIsMember(Trie *triePtr, const char *key);

/**
 * NodeIncrementPrefix will recursively increment its own
 * prefix counter, if it exists, and that of all its child nodes,
 * since if a prefix is found, then it is also a prefix to all the
 * superwords in the dictionary the key that called this function
 */
void NodeIncrementPrefix(Node *currentNode);

/**
 * TrieGetKeyAndStats will take in a Trie pointer
 * and a pointer to a char *keyString where the
 * full key string will be outputted for the
 * corresponding Stats object that is retured.
 * The keyString will have to be deallocated by the caller.
 *
 * It creates the list of nodes using RecurseKeys
 */
InterfaceNode *TrieGetKeyNodes(Trie *triePtr);

/**
 * RecurseKeys will recursively go through the entire Trie
 * and construct a list of InterfaceNodes each with their
 * entire keys and associated statistics. it only returns
 * the head pointer for the list
 */
InterfaceNode *RecurseKeys(Node *currentNode, InterfaceNode *lastNode, const char *key);

/**
 * INodeListDestroy will destroy a list of InterfaceNodes
 * and free all the associated memory.
 */
void INodeListDestroy(InterfaceNode *head);

#endif