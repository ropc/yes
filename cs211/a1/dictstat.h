#ifndef DICTSTAT_H
#define DICTSTAT_H
/**
 * dictstat.h
 */
#include <stdlib.h>
#include <stdio.h>
#include "trie.h"

/**
 * function pointer to a trie action
 * that will be used by fileAction
 */
typedef void (*trieAction)(Trie *triePtr, char *word);

/**
 * file action will parse through a given file pointer,
 * find each word (any sequence of characters A-Z or
 * a-z) within that file and perform the given action
 * to that word in conjunction to the dictTrie pointer
 * that will be accessable from within dictstat.c
 *
 * This helps with code reuse since both the dict and
 * data files will be parsed through the same mechanism
 * and will perform one action using one of the
 * trie's functions.
 *
 * it returns 0 if the given file was empty
 */
int fileAction(FILE *fp, trieAction action);

/**
 * readDict will execute fileAction() with arguments
 * dict_file and TrieAdd as an action
 * if fileAction returns 0, it denotes that the
 * dict_file given was empty. in this case it prints
 * out that the dictionary is empty
 * otherwise, it will run normally and put each word
 * found in the dictionary in the dictTrie using TrieAdd()
 */
void readDict(FILE *dict_file);

/**
 * scanData will execute fileAction() with arguments
 * dict_file and TrieIsMember as an action
 * TrieIsMember will take care of tabulating the stats
 * (occurrences, prefixes, superwords) for each unique
 * word in the dictionary (which should have been added
 * by readDict)
 */
void scanData(FILE *data_file);

/**
 * printStats will call TrieGetKeyNodes() in order to
 * utilize the IntefaceNode of Trie.h to print out
 * the unique words in the dictionary and their
 * corresponding statistics in the specified
 * format for this assignment.
 *
 * After printing out the statistics, it will free the
 * memory used by the list of InterfaceNode sturcts using
 * the function INodeListDestroy() defined in trie.h
 */
void printStats();

/**
 * clearDict will run TrieDestroy() on the dictTrie
 * that is used throughout dictstat.c, which will free
 * all the associtated dynamically allocated memory.
 */
void clearDict();

#endif