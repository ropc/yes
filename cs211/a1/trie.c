#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "trie.h"

Stats *StatsInitialize()
{
	Stats *statObj = (Stats*)calloc(1, sizeof(Stats));
	statObj->occurrence = 0;
	statObj->prefix = 0;
	statObj->superWord = 0;
	return statObj;
}

InterfaceNode *INodeCreate(const char *key)
{
	InterfaceNode *newNode = (InterfaceNode*)calloc(1, sizeof(InterfaceNode));
	newNode->next = NULL;

	if (key != NULL && strlen(key) > 0)
	{
		newNode->key = (char*)calloc(strlen(key), sizeof(char));
		strcpy(newNode->key, key);
	}
	else
		newNode->key = (char*)calloc(1, sizeof(char));

	return newNode;
}

void INodeDestroy(InterfaceNode *node)
{
	if (node != NULL)
	{
		if (node->key != NULL)
			free(node->key);
		free(node);
	}
}

Node *NodeCreate(const char key)
{
	Node *newNode = (Node*)calloc(1, sizeof(Node));
	newNode->key = key;
	newNode->next = NULL;
	newNode->children = NULL;
	newNode->nodeStats = NULL;

	return newNode;
}

Trie *TrieCreate()
{
	Trie *triePtr = (Trie*)calloc(1, sizeof(Trie));
	triePtr->root = NULL;
	return triePtr;
}

void NodeRecursiveDestroy(Node *currentNode)
{
	if (currentNode != NULL)
	{
		if (currentNode->children != NULL)
			NodeRecursiveDestroy(currentNode->children);

		if (currentNode->next != NULL)
			NodeRecursiveDestroy(currentNode->next);

		if (currentNode->nodeStats != NULL)
			free(currentNode->nodeStats);

		free(currentNode);
	}
}

void TrieDestroy(Trie *triePtr)
{
	if (triePtr != NULL)
	{
		NodeRecursiveDestroy(triePtr->root);
		free(triePtr);
	}
}

void TrieAdd(Trie *triePtr, const char* key)
{
	int keyLength = strlen(key);
	if (keyLength > 0)
	{
		/**
		 * here for use across entire function
		 * also can be used to figure out depth of current node
		 */
		int i = 0;

		if (triePtr->root == NULL)
		{
			triePtr->root = NodeCreate(key[0]);
			i = 1;
		}
		
		/**
		 * if there are more characters to go through
		 * i = 1 only after adding root, otherwise i = 0
		 */
		if ((keyLength - i) > 0)
		{
			char letter;
			Node *prev = NULL;
			Node *parentNode = NULL;
			Node *currentNode = triePtr->root;

			if (i == 1)
			{
				prev = triePtr->root;
				parentNode = triePtr->root;
				currentNode = triePtr->root->next;
			}

			for (letter = key[i]; i < keyLength; letter = key[++i])
			{
				if (currentNode == NULL)
				{
					currentNode = NodeCreate(letter);
					parentNode->children = currentNode;
				}
				else
				{
					// letter found will be 1 if a node contaning
					// the correct letter is found.
					// 2 if it was not found but the position in the
					// list where it should go is found,
					// and 0 if it goes at the end of the list and
					// it was not found
					int letterFound = 0;
					while (currentNode != NULL && letterFound == 0)
					{
						if (currentNode->key < letter)
						{
							prev = currentNode;
							currentNode = currentNode->next;
						}
						else if (currentNode->key == letter)
							letterFound = 1;
						else if (currentNode->key > letter)
							letterFound = 2;
					}

					if (letterFound == 0)
					{
						currentNode = NodeCreate(letter);
						prev->next = currentNode;
					}
					else if (letterFound == 2)
					{
						// position is found, but node isnt
						Node *next = currentNode;
						currentNode = NodeCreate(letter);
						currentNode->next = next;
						if (prev == NULL || (prev != NULL && prev == parentNode))
						{
							if (parentNode != NULL)
								parentNode->children = currentNode;
							else
								triePtr->root = currentNode;
						}
						else
							prev->next = currentNode;
					}
				}

				if (i != keyLength - 1)
				{
					prev = currentNode;
					parentNode = currentNode;
					currentNode = currentNode->children;
				}
			}

			if (currentNode != NULL)
				currentNode->nodeStats = StatsInitialize();
		}
		else if (triePtr->root->key == key[0])
		{
			if (triePtr->root->nodeStats == NULL)
				triePtr->root->nodeStats = StatsInitialize();
		}
	}
}

int TrieIsMember(Trie *triePtr, const char *key)
{
	int keyValue = 0;

	int keyLength = strlen(key);

	if (keyLength > 0 && triePtr != NULL)
	{
		Node *currentNode = triePtr->root;
		char letter;
		/* assuming the first letter is found to initialize loop */
		int letterFound = 1;
		Node *superWordNode = NULL;

		int i = 0;
		for (letter = key[i];
			i < keyLength && currentNode != NULL && letterFound == 1;
			letter = key[++i])
		{
			/* reset letterFound */
			letterFound = 0;
			
			while (currentNode != NULL && letterFound == 0)
			{
				if (letter == currentNode->key)
					letterFound = 1;
				else
					currentNode = currentNode->next;
			}

			/**
			 * if the letter is found, continue going through trie
			 * else, currentNode = NULL because key doesnt exist in trie
			 */

			if (letterFound == 1)
			{
				if (currentNode->nodeStats != NULL)
				{
					if (superWordNode != NULL)
						superWordNode->nodeStats->superWord++;
					superWordNode = currentNode;
				}

				if (i != keyLength - 1)
					currentNode = currentNode->children;
			}
			else
				currentNode = NULL;
		}

		/**
		 * If the given key is found in the trie, the currentNode
		 * will not be NULL. If the value at that node is not 0,
		 * then that value will be returned, otherwise return -1
		 * to indicate that they key given is a prefix of a key in the trie.
		 * 
		 * If the key is not found but some beginning part of the key
		 * was found, then the superWord flag will be 1. In this case
		 * TrieIsMember will return -2. Otherwise if it is not found at all,
		 * the function will simply return 0;
		 */

		if (currentNode != NULL)
		{
			if (currentNode->nodeStats != NULL)
			{
				// given key is an occurrence of a key in trie
				if (superWordNode != currentNode)
					superWordNode->nodeStats->superWord++;

				Node *child;
				for (child = currentNode->children; child != NULL; child = child->next)
					NodeIncrementPrefix(child);

				currentNode->nodeStats->occurrence++;
				keyValue = 1;
			}
			else
			{
				// given key is a prefix of a key in trie
				if (superWordNode != NULL)
					superWordNode->nodeStats->superWord++;
				
				keyValue = -1;
				NodeIncrementPrefix(currentNode);
			}
		}
		else
		{
			if (superWordNode != NULL)
			{
				superWordNode->nodeStats->superWord++;
				keyValue = -2;
			}
			else
				keyValue = 0;
		}
	}

	return keyValue;
}

void NodeIncrementPrefix(Node *currentNode)
{
	if (currentNode != NULL)
	{
		/* if any children exist, recursively go through each */
		if (currentNode->children != NULL)
		{
			Node *cursor;
			for (cursor = currentNode->children; cursor != NULL; cursor = cursor->next)
				NodeIncrementPrefix(cursor);
		}

		if (currentNode->nodeStats != NULL)
			currentNode->nodeStats->prefix++;
	}
}

/**
 * String these interfaceNodes along with themselves. this will create
 * a list for the nodes and so this function will return all the keys
 * and their values. No more need for keyNum in the struct.
 * In order to also have it in order, must change around Add.
 * also *might* want to implement queue for creating the char *key
 */
InterfaceNode *TrieGetKeyNodes(Trie *triePtr)
{
	InterfaceNode *headNode = NULL;

	if (triePtr != NULL && triePtr->root != NULL)
	{
		headNode = RecurseKeys(triePtr->root, headNode, NULL);
	}

	return headNode;
}

/**
 * should return the first node from the list that it knows
 * key is the last known parent key
 */
InterfaceNode *RecurseKeys(Node *currentNode, InterfaceNode *lastNode, const char *key)
{
	InterfaceNode *returnNode = NULL;

	if (currentNode != NULL)
	{
		int lastNodeUnique = 0;
		int keyAllocated = 0;
		char *prevKey = NULL;

		if (lastNode == NULL)
		{
			lastNode = INodeCreate(NULL);
			returnNode = lastNode;
		}

		if (key != NULL)
			prevKey = (char*)key;
		else if (strlen(lastNode->key) > 0)
		{
			prevKey = (char*)calloc(strlen(lastNode->key) + 1, sizeof(char));
			strcpy(prevKey, lastNode->key);
			keyAllocated = 1;
		}

		if (currentNode->nodeStats != NULL)
		{
			lastNode->occurrence = currentNode->nodeStats->occurrence;
			lastNode->prefix = currentNode->nodeStats->prefix;
			lastNode->superWord = currentNode->nodeStats->superWord;
			lastNodeUnique = 1;
		}

		if (strlen(lastNode->key) > 0)
		{
			int incrementLength = 1;
			if (lastNodeUnique == 1)
				incrementLength = 2;
			lastNode->key = (char*)realloc(lastNode->key,
				sizeof(char) * (strlen(lastNode->key) + incrementLength));
		}

		lastNode->key[strlen(lastNode->key)] = currentNode->key;

		if (lastNodeUnique == 1)
			lastNode->key[strlen(lastNode->key)] = '\0';

		if (currentNode->children != NULL)
		{
			if (lastNodeUnique == 1)
			{
				lastNode->next = INodeCreate(lastNode->key);
				lastNode = lastNode->next;
			}
			RecurseKeys(currentNode->children, lastNode, NULL);
		}

		if (currentNode->next != NULL)
		{
			if (lastNodeUnique == 1 || currentNode->children != NULL)
			{
				// checking if nodes were added when
				// going down the children nodes
				while (lastNode->next != NULL)
					lastNode = lastNode->next;

				lastNode->next = INodeCreate(prevKey);
				lastNode = lastNode->next;
			}

			RecurseKeys(currentNode->next, lastNode, prevKey);
		}

		if (keyAllocated == 1)
			free(prevKey);
	}

	return returnNode;
}

void INodeListDestroy(InterfaceNode *head)
{
	InterfaceNode *nextNode = NULL;
	for (InterfaceNode *cursor = head; cursor != NULL; cursor = nextNode)
	{
		nextNode = cursor->next;
		INodeDestroy(cursor);
	}
}
