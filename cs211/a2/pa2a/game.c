#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "conway.h"

void printusage(){
    printf("Usage: game <width> <height>\n");
}

void printboard(int **board, int width, int height){
    int i, j;

    for(i = 0; i < height; i++){
        for(j = 0; j < width; j++){
            printf("%d ", board[i][j]);
        }
        printf("\n");
    }

}

void fupdate(int **board, int width, int height)
{
    int neighborCount = 0;
    int counteri;
    
    for (counteri = height; counteri > 0; counteri--) {
        int i = counteri - 1;
        
        int counterj;
        for (counterj = width; counterj > 0; counterj--) {
            int j = counterj - 1;
            int countera = i;
            int counterb = j;
            
            /*printf("counterj: %d, j: %d\n", counterj, j);*/

            if (countera < height - 1)
                countera++;
            if (counterb < width - 1)
                counterb++;
            /*printf("countera: %d, counterb: %d\n", countera, counterb);*/
            countera++;
            counterb++;
            for (; countera >= i && countera > 0; countera--)
            {
                int a = countera - 1;
                int startB = counterb;
                /*printf("countera: %d, a: %d, \n", countera, a);*/
                for (; counterb >= j && counterb > 0; counterb--) {
                    /*printf("a: %d, b: %d\n", a, b);*/
                    int b = counterb - 1;
                    if (!(a == i && b == j) && (board[a][b] == 1 || board[a][b] == -1 ))
                        neighborCount++;
                }
                counterb = startB;
            }
            
            if (board[i][j] == 1)
            {
                if (neighborCount < 2)
                    board[i][j] = -1;
                else if (neighborCount > 3)
                    board[i][j] = -1;
            }
            else
            {
                if (neighborCount == 3)
                    board[i][j] = -2;
            }
            neighborCount = 0;
        }
    }

    for (counteri = height; counteri > 0; counteri--) {
        int i = counteri - 1;
        int counterj;
        for (counterj = width; counterj > 0; counterj--) {
            int j = counterj - 1;
            if (board[i][j] == -1)
                board[i][j] = 0;
            else if (board[i][j] == -2)
                board[i][j] = 1;
        }
    }
}


int main(int argc, char *argv[]){
    int width, height;
    int **board;
    int i;

    if(argc != 3){
        printusage();
        return 1;
    }
    if(sscanf(argv[1], "%d", &width) != 1){
        printusage();
        return 1;
    }

    if(sscanf(argv[2], "%d", &height) != 1){
        printusage();
        return 1;
    }

    /*allocate space for the board*/
    board = malloc(sizeof(int *) * height);
    for(i = 0; i < height; i++){
        board[i] = malloc(sizeof(int) * width);
    }

    /*initialize the board to be in a random state*/
    srand(time(NULL));

    for(i = 0; i < height; i++){
        int j;
        for(j = 0; j < width; j++){
            board[i][j] = rand() % 2;
        }
    }

    printboard(board, width, height);

    /*call your update method*/
    update(board, width, height);

    printf("\n");

    printboard(board, width, height);

    /*clean things up*/
    for(i = 0; i < height; i++){
        free(board[i]);
    }
    free(board);

    return 0;
}
