    .file   "conway.c"
    .text
.globl update
    .type   update, @function
update:
	pushl	%ebp
	movl	%esp, %ebp
	pushl	%ebx				# save ebx since callee 
	pushl	$0					# initializing neighborCount = 0 = -8(%ebp)
	# height loop 1 setup
	movl	16(%ebp), %ecx		# ecx = counteri = height
	pushl	%ecx				## store counteri = -12(%ebp)
	cmpl	$0, %ecx			# compare counteri:0
	jl		.EndHeightLoop1		# counteri < 0 == !(counteri >= 0)
.HeightLoop1:
	movl 	%ecx, -12(%ebp)		# save any changes of counteri to stack
	decl	%ecx				# i-- to make counteri useful
	pushl	%ecx				## store i in stack, i = -16(%ebp)
	# width loop setup
	movl	12(%ebp), %ecx		# j = width
	pushl	%ecx				## store counterj = -20(%ebp)
	cmpl	$0, %ecx			# compare counterj:0
	jl		.EndWidthLoop1		# counterj < 0 == !(counterj >= 0)
.WidthLoop1:
	movl 	%ecx, -20(%ebp)		# save any changes of counterj to stack
	decl	%ecx				# j-- to make counteri useful
	pushl	%ecx				## store j in stack, j = -24(%ebp)
	movl	-16(%ebp), %eax		# eax = a = i
	movl	-24(%ebp), %ebx		# ebx = b = j
	movl	16(%ebp), %edx		# edx = height
	decl	%edx				# edx-- == height - 1
	cmpl	%edx, %eax			# compare a:height - 1
	jge		.NoFixA				# a >= height - 1 == !(a < height - 1)
	incl	%eax				# a++
.NoFixA:
	movl	12(%ebp), %edx		# edx = width
	decl	%edx				# edx-- == height - 1
	cmpl	%edx, %ebx			# compare i:height - 1
	jge		.NoFixB				# b >= height - 1 == !(b < width - 1)
	incl	%ebx				# b++
.NoFixB:
	# creation of counters
	incl	%eax				# increment a to create countera
	incl	%ebx				# increment b to create counterb
	# save the eax, ecx, and edx:
	pushl	%eax			# eax = -28(%ebp)
	pushl	%ecx			# ebx = -32(%ebp)
	pushl	%edx			# edx = -36(%ebp)
	# .checkNeighbors loops around to the neighbors
	# and calls .neighborExists at each location
	# checkNeighbors takes in arguments: countera, counterb, i, j, board**
	# and returns the neighborCount
	pushl	8(%ebp)				# board**
	pushl	-24(%ebp)			# j
	pushl	-16(%ebp)			# i
	pushl	%ebx				# counterb # store counterb = -32(%ebp)
	pushl	%eax				# countera # store countera = -28(%ebp)
	call .checkNeighbors
	movl 	%eax, -8(%ebp)		# store return value to neighborCount
	# make top of stack at the last register save (edx)
	leal	-36(%ebp), %esp		# move top of stack
	# .updatePosition which takes arguments: &(board[i][j]), neighborCount
	# get the address to the location of the board where a change will be made
	movl	8(%ebp), %ebx			# ebx = **board, loading board into ebx
	movl	-16(%ebp), %ecx			# ecx = i
	movl	(%ebx, %ecx, 4), %edx	# edx = ebx + ecx * 4 = board + a*4
	movl 	-24(%ebp), %ecx			# ecx = j
	leal	(%edx, %ecx, 4), %edx	# edx = (address of) ebx + ecx * 4 = &(board + a*4)
	# argument build for .upadateBoard
	pushl	-8(%ebp)			# neighborCount
	pushl	%edx				# &(board[i][j])
	call 	.updatePosition
	# restore original registers
	movl 	-28(%ebp), %eax		# restore eax
	movl 	-32(%ebp), %ecx		# restore ecx
	movl 	-36(%ebp), %edx		# restore edx
	# loop cleanup
	leal	-24(%ebp), %esp		# ensure j is at the top of the stack
	popl 	%ecx				# pop local var j
	movl	-20(%ebp), %ecx		# load counterj
	loop 	.WidthLoop1
.EndWidthLoop1:
	leal	-16(%ebp), %esp		# ensure i is at the top of the stack
	popl	%ecx				# pop local var i
	movl 	-12(%ebp), %ecx		# load counteri
	loop	.HeightLoop1
.EndHeightLoop1:	#done loop 1
	# "clean up" stack
	leal	-12(%ebp), %esp		# make top of stack counteri
	# height loop 2 setup
	movl	16(%ebp), %ecx		# ecx = height
	movl 	%ecx, -12(%ebp)		# reset counteri = height
	cmpl	$0, %ecx			# compare counteri:0
	jl		.EndHeightLoop2		# counteri < 0 == !(counteri >= 0)
.HeightLoop2:
	movl 	%ecx, -12(%ebp)		# save any changes of counteri to stack
	decl	%ecx				# i-- to make counteri useful
	pushl	%ecx				## store i in stack, i = -16(%ebp)
	# width loop setup
	movl	12(%ebp), %ecx		# j = width
	pushl	%ecx				## store counterj = -20(%ebp)
	cmpl	$0, %ecx			# compare counterj:0
	jl		.EndWidthLoop2		# counterj < 0 == !(counterj >= 0)
.WidthLoop2:
	movl 	%ecx, -20(%ebp)		# save any changes of counterj to stack
	decl	%ecx				# j-- to make counteri useful
	pushl	%ecx				## store j in stack, j = -24(%ebp)
	# save the eax, ecx, and edx:
	pushl	%eax			# eax = -28(%ebp)
	pushl	%ecx			# ebx = -32(%ebp)
	pushl	%edx			# edx = -36(%ebp)
	# get &board[i][j]
	movl	8(%ebp), %ebx			# ebx = **board, loading board into ebx
	movl	-16(%ebp), %ecx			# ecx = i
	movl	(%ebx, %ecx, 4), %edx	# edx = ebx + ecx * 4 = board + a*4
	movl 	-24(%ebp), %ecx			# ecx = j
	leal	(%edx, %ecx, 4), %edx	# edx = (address of) ebx + ecx * 4 = &(board + a*4)
	# argument build for .upadateBoard
	pushl	%edx				# &board[i][j]
	call	.cleanPosition
	# restore original registers
	movl 	-28(%ebp), %eax		# restore eax
	movl 	-32(%ebp), %ecx		# restore ecx
	movl 	-36(%ebp), %edx		# restore edx
	# loop cleanup
	leal	-24(%ebp), %esp		# ensure j is at the top of the stack
	popl 	%ecx				# pop local var j
	movl	-20(%ebp), %ecx		# load counterj
	loop 	.WidthLoop2
.EndWidthLoop2:
	leal	-16(%ebp), %esp		# ensure i is at the top of the stack
	popl	%ecx				# pop local var i
	movl 	-12(%ebp), %ecx		# load counteri
	loop	.HeightLoop2
.EndHeightLoop2:
	movl	-4(%ebp), %ebx		# restore ebx
	movl	%ebp, %esp			# point to ebp
	popl	%ebp
	ret

# checkNeighbors takes in arguments: countera, counterb, i, j, board**
.checkNeighbors:
	pushl	%ebp
	movl	%esp, %ebp
	pushl	%ebx				# save ebx since callee 
	pushl	$0					# init neighborCount = 0 = -8(%ebp)
	# loop a setup
	movl 	8(%ebp), %ecx		# make countera the counter ecx
	cmpl	$0, %ecx			# compare countera:0
	jle		.EndLoopA			# if countera <= 0 == !(countera > 0) don't loop
	cmpl	16(%ebp), %ecx		# compare a:i
	jl		.EndLoopA			# if countera < i == !(countera >= i) don't loop
.LoopA:
	movl 	%ecx, 8(%ebp)		# save any changes of countera to stack
	cmpl	16(%ebp), %ecx		# compare countera:i
	jl		.EndLoopA			# if countera < i == !(countera >= j) break
	# don't push ecx before jump or will continue to push to stack forever
	decl	%ecx				# a-- to make countera useful
	pushl	%ecx				## store 'a' = -12(%ebp)
	pushl	12(%ebp)			## store a copy of counterb: startB = -16(%ebp)
	# loop b setup
	movl	12(%ebp), %ecx		# make counterb the counter ecx
	cmpl	$0, %ecx			# compare counterb:0
	jle		.EndLoopB			# if counterb <= 0 == !(counterb > 0) don't loop
	cmpl	20(%ebp), %ecx		# compare counterb:j
	jl		.EndLoopB			# if counterb < j == !(counterb >= j) don't loop
.LoopB:
	movl 	%ecx, 12(%ebp)		# save any changes of counterb to stack
	cmpl	20(%ebp), %ecx		# compare counterb:j
	jl 		.EndLoopB			# if counterb < j == !(counterb >= j) break
	decl	%ecx				# b-- to make counterb useful
	pushl	%ecx				## store b = -20(%ebp)
	# save the eax, ecx, and edx:
	pushl	%eax			# eax = -24(%ebp)
	pushl	%ecx			# ebx = -28(%ebp)
	pushl	%edx			# edx = -32(%ebp)
	# .neighborExists checks if the position a, b in board is alive
	# takes in arguments: a, b, i, j, board**
	pushl	24(%ebp)		# board
	pushl	20(%ebp)		# j
	pushl	16(%ebp)		# i
	pushl	-20(%ebp)		# b
	pushl	-12(%ebp)		# a
	call 	.neighborExists
	movl 	-8(%ebp), %edx	# load current neighborExists into edx
	addl	%eax, %edx		# add the returned neighborExists into edx
	movl 	%edx, -8(%ebp)	# store new neighborExists into stack
	# restore registers
	movl 	-24(%ebp), %eax	# restore eax
	movl 	-28(%ebp), %ecx	# restore ecx
	movl 	-32(%ebp), %edx	# restore edx
	leal	-20(%ebp), %esp		# ensure b is at the top of the stack
	popl	%ecx				# pop local var b
	movl 	12(%ebp), %ecx		# load counterb
	loop	.LoopB
.EndLoopB:
 	leal	-16(%ebp), %esp		# ensure startB is at the top of the stack
	popl	12(%ebp)			# reset counterb = startB
	popl	%ecx				# pop local var a
	movl 	8(%ebp), %ecx		# load countera
	loop	.LoopA
.EndLoopA:
	movl 	-8(%ebp), %eax		# set the return value to neighborCount
	movl	-4(%ebp), %ebx		# restore ebx
	movl	%ebp, %esp			# point to ebp
	popl	%ebp
	ret

# checkNeighbors takes in arguments: a, b, i, j, board**
.neighborExists:
	pushl	%ebp
	movl	%esp, %ebp
	pushl	%ebx				# save ebx since callee 
	pushl	$0					# init neighborExists = 0 = -8(%ebp)
	# checking !(a == i && b == j)
	movl	8(%ebp), %edx		# edx = a
	cmpl	16(%ebp), %edx		# compare a:i
	sete 	%al					# set al if equal
	movzbl	%al, %eax			# zero out rest of %eax
	movl 	12(%ebp), %edx		# edx = b
	cmpl	20(%ebp), %edx		# compare b:j
	sete	%bl					# set bl if equal
	movzbl	%bl, %ebx			# zero out rest of ebx
	andl	%ebx, %eax			# and both operations, and store in eax: (... && ...)
	cmpl	$0, %eax			# compare result:0
	jne 	.NotExists			# if eax == 0, checking self. can't be own neighbor so not a neighbor
	# checking (board[a][b] == 1 || board[a][b] == -1 )
	movl	24(%ebp), %ebx		# ebx = **board, loading board into ebx
	# getting board[a][b]
	movl	8(%ebp), %ecx		# ecx = a
	movl	(%ebx, %ecx, 4), %edx	# edx = (address of) ebx + ecx * 4 = &(board + a*4)
	movl 	12(%ebp), %ecx		# ecx = b
	movl 	(%edx, %ecx, 4), %edx	#edx = edx + ecx*4 = *((board + a*4) + b*4)
	cmpl	$1, %edx			# compare board[a][b]:1
	sete	%bl					# set bl if equal
	movzbl	%bl, %ebx			# zero out rest of b
	cmpl	$-1, %edx			# compare board[a][b]:-11
	sete	%cl					# set cl if equal
	movzbl	%cl, %ecx			# zero out rest of ecx
	orl		%ecx, %ebx			# orl the results of comparisons
	cmpl	$0, %ebx			# compare ebx:0
	je		.NotExists	# if ebx == 0, then no neighbors exist
	movl	$1, -8(%ebp)
.NotExists:
	movl 	-8(%ebp), %eax		# set the return value to neighborCount
	movl	-4(%ebp), %ebx		# restore ebx
	movl	%ebp, %esp			# point to ebp
	popl	%ebp
	ret

# .updatePosition takes in arguments: &(board[i][j]), neighborCount
.updatePosition:
	pushl	%ebp
	movl	%esp, %ebp
	pushl	%ebx				# save ebx since callee
	movl 	12(%ebp), %ecx		# ecx = neighborCount
	movl 	8(%ebp), %ebx		# ebx = &(board[i][j])
	movl 	(%ebx), %edx		# edx = board[i][j]
	cmpl	$1, %edx			# compare board[i][j]:1
	jne		.valueNotOne		# if board[i][j] == 1, continue here else jump
	cmpl	$2, %ecx			# compare neighborCount:2
	jge		.countNotLess2		# if neighborCount >= 2 == !(neighborCount < 2)
	movl 	$-1, (%ebx)			# change value on board to -1
	jmp 	.updateDone
.countNotLess2:
	cmpl	$3, %ecx			# compare neighborCount:3
	jle		.updateDone			# if neighborCount <= 3 ==!(neighborCount > 3), no change needs to be done
	movl 	$-1, (%ebx)			# change value on board to -1
	jmp 	.updateDone
.valueNotOne:
	cmpl	$3, %ecx			# compare neighborCount:3
	jne		.updateDone			# if neighborCount != 3, no change needed
	movl 	$-2, (%ebx)			# change value on board to -2
.updateDone:
	movl	-4(%ebp), %ebx		# restore ebx
	movl	%ebp, %esp			# point to ebp
	popl	%ebp
	ret

# .cleanPosition takes in argument: &(board[i][j])
.cleanPosition:
	pushl	%ebp
	movl	%esp, %ebp
	pushl	%ebx				# save ebx since callee
	# get info from board
	movl 	8(%ebp), %ebx		# ebx = &board[i][j] (address of)
	movl 	(%ebx), %edx		# edx = board[i][j] (value at)
	cmpl	$0, %edx			# comparing board[i][j]:0
	jge		.cleanDone			# if board[i][j] >= 0, no need to change anything
	cmpl	$-1, %edx			# comparing board[i][j]:-1
	jne		.valueNotNegOne		# if board[i][j] != -1 go to next check
	movl 	$0, (%ebx)			# if -1, then set board[i][j] = 0
	jmp 	.cleanDone
.valueNotNegOne:
	cmpl	$-2, %edx			# comparing board[i][j]:-1
	jne		.cleanDone			# if board[i][j] != -2 nothing else to check
	movl 	$1, (%ebx)			# if -2, then set board[i][j] = 1
.cleanDone:
	movl	-4(%ebp), %ebx		# restore ebx
	movl	%ebp, %esp			# point to ebp
	popl	%ebp
	ret
