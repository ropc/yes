	.file	"mystery.c"
	.text
.globl bar
	.type	bar, @function
bar:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp
	cmpl	$1, 8(%ebp)
	jg	.L2
	movl	$0, %eax
	jmp	.L3
.L2:
	movl	$2, -4(%ebp)
	jmp	.L4
.L6:
	movl	8(%ebp), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	idivl	-4(%ebp)
	movl	%edx, %eax
	testl	%eax, %eax
	jne	.L5
	movl	$0, %eax
	jmp	.L3
.L5:
	addl	$1, -4(%ebp)
.L4:
	movl	-4(%ebp), %eax
	cmpl	8(%ebp), %eax
	jl	.L6
	movl	$1, %eax
.L3:
	leave
	ret
	.size	bar, .-bar
.globl foo
	.type	foo, @function
foo:	# esp sould be malloc'd space
	pushl	%ebp
	movl	%esp, %ebp
	pushl	%ebx
	subl	$36, %esp			# -8(%ebp) is the start of argv[1]
	movl	$0, -12(%ebp)		# 0 to second character of argv[1]
	movl	$0, -12(%ebp)		# wut
	jmp	.L9
.L14:
	movl	-12(%ebp), %eax		# load counter/ position
	addl	8(%ebp), %eax		# add that to &(input + position) (string)
	movzbl	(%eax), %eax		# get char at the next position and store in eax, ascii char so zero out useless bits
	cmpb	$126, %al 			# (check if ascii) compare to 126:'~', if it is a tidle, stop changing the rest of the string
	je	.L16
.L10:
	movl	-12(%ebp), %eax		# load counter/ position
	movl	%eax, %edx			# store in edx too
	sarl	$31, %edx			# edx = counter/position / 2 ^31
	shrl	$31, %edx			# edx = (unsigned int) edx / 2 ^31 # shifts make it (force) 0
	addl	%edx, %eax			# eax = edx (0) + eax
	andl	$1, %eax			# if odd, eax = 1, even eax = 0
	subl	%edx, %eax			# eax = edx (which is 0) - eax
	cmpl	$1, %eax			# if eax == 1 == odd
	jne	.L12					# if even jump,
	movl	-12(%ebp), %eax		# load counter
	movl	%eax, %ebx			# store in ebx too
	addl	8(%ebp), %ebx		# ebx = &input+position
	movl	-12(%ebp), %eax		# load counter again
	addl	8(%ebp), %eax		# eax = input+position
	movzbl	(%eax), %eax		# zero out rest of a and put *(input+position) in eax
	movsbl	%al, %eax			# again, zero out
	movl	%eax, (%esp)		# move the loaded char (al) into top of stack
	call	toupper				# make the loaded char uppercase
	movb	%al, (%ebx)			# save the uppercase char returned to malloc'd string
.L12:
	movl	-12(%ebp), %eax		# load counter into eax
	addl	$1, %eax			# add 1 to counter
	movl	%eax, (%esp)		# put counter in stack (argument for bar)
	call	bar					# returns 1 if prime, else returns 0
	testl	%eax, %eax			# not sure how the test works here but
	je	.L13					# will only call tolower if position+1 is prime
	movl	-12(%ebp), %eax		# load counter/ position
	movl	%eax, %ebx			# copy to ebx too
	addl	8(%ebp), %ebx		# ebx = &(input + position)
	movl	-12(%ebp), %eax		# load counter again...
	addl	8(%ebp), %eax		# eax = &(input + position)
	movzbl	(%eax), %eax		# eax = input[position]
	movsbl	%al, %eax			# zero out rest of eax since char == 1 byte
	movl	%eax, (%esp)		# put char on stack for to lower
	call	tolower				# make char lowercase
	movb	%al, (%ebx)			# store returned lowercase char into malloc'd string
.L13:							# move on to next position
	addl	$1, -12(%ebp)
.L9:
	movl	-12(%ebp), %ebx		# ebx = counter/ position
	movl	8(%ebp), %eax		# string after :
	movl	%eax, (%esp)		# put the string as argument for strlen
	call	strlen
	cmpl	%eax, %ebx			# strlen returns a size_t (unsigned) compare to 0
	jb	.L14					# 0 - strlen(input) # if 
	jmp	.L15					# jump when done
.L16:
	nop				# does nothing but increments program counter
.L15:
	addl	$36, %esp
	popl	%ebx
	popl	%ebp
	ret
	.size	foo, .-foo
	.section	.rodata
	.align 4
.LC0:
	.string	"Incorrect number of command line arguments given"
.LC1:
	.string	"Input:%s"
	.align 4
.LC2:
	.string	"Incorrect format for command line argument"
.LC3:
	.string	"Output: \"%s\"\n"
	.text
.globl main
	.type	main, @function
main:
	pushl	%ebp
	movl	%esp, %ebp
	andl	$-16, %esp
	subl	$32, %esp
	cmpl	$2, 8(%ebp)		# compare argc:2
	je	.L18
	movl	$.LC0, (%esp)
	call	puts
	movl	$1, %eax
	jmp	.L19
.L18:
	movl	12(%ebp), %eax	# eax = &argv
	addl	$4, %eax		# eax = &argv[1]
	movl	(%eax), %eax	# eax = argv[1]
	movl	%eax, (%esp)
	call	strlen
	movl	%eax, %edx		# wut
	movl	%edx, %eax		# wut
	sall	$2, %eax		# eax = strlen(argv[1]) * 4
	addl	%edx, %eax		# eax = strlen(argv[1])*4 + strlen(argv[1]) = strlen(argv[1]) * 5
	movl	%eax, (%esp)	# store size is (%esp)
	call	malloc
	movl	%eax, 28(%esp)	# store location of malloc'd sapce in 28(%esp)
	movl	$.LC1, %edx		# store correct format in edx
	movl	12(%ebp), %eax	# eax = &argv[0]
	addl	$4, %eax
	movl	(%eax), %eax	# eax = argv[1]
	movl	28(%esp), %ecx	# load malloc'd space in ecx
	movl	%ecx, 8(%esp)	# put malloc'd space in 8(%esp) (3rd argument), %s is put in here if sscanf succeeds
	movl	%edx, 4(%esp)	# put correct format in 4(%esp) (2nd argument)
	movl	%eax, (%esp)	# put argv[1] in %esp (1st argument)
	call	__isoc99_sscanf
	cmpl	$1, %eax		# if the string is in the correct format, jump
	je	.L20
	movl	$.LC2, (%esp)	# else, output the error LC2, and return 1
	call	puts
	movl	$1, %eax
	jmp	.L19
.L20:	# string is in the correct format
	movl	28(%esp), %eax	# put malloc'd space in eax (copied string)
	movl	%eax, (%esp)	# malloc'd space, put on top of stack
	call	foo				# puts new string in malloc'd space at 28(%esp), returns eax = strlen()
	movl	$.LC3, %eax		# setup for output
	movl	28(%esp), %edx
	movl	%edx, 4(%esp)
	movl	%eax, (%esp)
	call	printf			# display output string in terminal
	movl	28(%esp), %eax
	movl	%eax, (%esp)
	call	free			# calls free on top of stack
	jmp	.L17
.L19:	#end program
.L17:
	leave
	ret
	.size	main, .-main
	.ident	"GCC: (GNU) 4.4.7 20120313 (Red Hat 4.4.7-4)"
	.section	.note.GNU-stack,"",@progbits
