#ifndef CACHE_H
#define CACHE_H
#include <stdlib.h>

struct line {
	char valid;
	unsigned int tag;
	/* char *dataBlock; */
	/* int blockSize; */
	struct line *next, *prev;
};

struct set {
	int setSize;
	struct line **lines;
	struct line *top, *bottom;
};

typedef struct cache {
	char type;
	char isWriteThrough;
	int numSets;
	int tagBits;
	int setBits;
	int offsetBits;
	struct set **sets;
} Cache;

int LineFill(struct line *l, unsigned int tag);

/**
 * Returns 1 if line given is valid and tag is the same
 * returns 0 otherwise
 */
int LineCheck(struct line *l, unsigned int tag);

int SetCheck(struct set *s, unsigned int tag);

int CacheCheck(Cache *c, const unsigned int addr);

Cache *CreateCache(const char type, char isWriteThrough, unsigned int sets, unsigned int setSize, unsigned int blockSize);

struct set *CreateSet(unsigned int setSize);

struct line *CreateLine();

void DestroyCache(Cache *c);

void DestroySet(struct set *s);

void DestroyLine(struct line *l);

int isPower2(const unsigned int num);

void showbits(unsigned int x);

int fillBits(unsigned int num);

int numBits(unsigned int num);

#endif