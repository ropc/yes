#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "cache.h"

typedef int (*CacheFunct)(Cache *c, unsigned int tag);

int main(int argc, char const *argv[])
{
	unsigned int cacheSize = 0;
	unsigned int type = 0;
	unsigned int sets = 0;
	unsigned int setSize = 0;
	unsigned int blockSize = 0;
	unsigned int instruction = 0;
	unsigned int memory = 0;
	unsigned int memReads = 0;
	unsigned int memWrites = 0;
	unsigned int hits = 0;
	unsigned int misses = 0;
	unsigned char isWriteThrough = 0;
	unsigned char readWrite = 0;
	FILE *fp;
	Cache *cache;
	char line[200];
	


	if (argc >= 2 && strcmp(argv[1], "-h") == 0)
	{
		printf("Usage: c-sim [-h] <cache size> <associativity> <block size> <write policy> <trace file>\n");
		printf("Cache size:\tshould be a power of 2 that specifies the total size of the cache.\n");
		printf("Associativity:\tcan be 'direct', 'assoc', or 'assoc:n'. For direct map, fully associative, and n-way associative cache, respectively.\n");
		printf("Block size:\tis a power of 2 that specifies the size of the cache block.\n");
		printf("Write policy:\tshould be 'wt' for write through cache.\n");
		printf("Tracefile:\tshould be the name of the file that contains a memory access trace.\n");
		printf("-h:\tto see this message again.\n");
		return 0;
	}
	else if (argc != 6)
	{
		printf("ERROR: Incorrect number of arguments, run with '-h' for help\n");
		return 1;
	}

	/* correct number of arguments given */
	
	cacheSize = atoi(argv[1]);
	if (!isPower2(cacheSize))
	{
		printf("ERROR: Cache size not a power of 2\n");
		return 1;
	}

	if (strcmp(argv[2], "direct") == 0)
	{
		setSize = 1;
		type = 1;
	}
	else if (strcmp(argv[2], "assoc") == 0)
	{
		sets = 1;
		type = 2;
	}
	else if (strncmp(argv[2], "assoc:", 6) == 0)
	{
		type = 3;
		sscanf(argv[2], "assoc:%u", &setSize);
	}
	else
	{
		printf("ERROR: Unknown associativity\n");
		return 1;
	}

	blockSize = atoi(argv[3]);
	if (!isPower2(blockSize))
	{
		printf("ERROR: Block size not a power of 2\n");
		return 1;
	}

	if (blockSize > cacheSize)
	{
		printf("Block size cannot be larger than total cache size\n");
		return 1;
	}

	if (strcmp(argv[4], "wt") == 0)
		isWriteThrough = 1;
	else
	{
		printf("ERROR: Unknown write policy: %s\n", argv[4]);
		return 1;
	}

	fp = fopen(argv[5], "r");
	if (fp == NULL)
	{
		printf("ERROR: %s: %s\n", strerror(errno), argv[5]);
		return 1;
	}

	/* arguments ok */

	/**
	 * figure out how many sets for direct/n-way assoc
	 * figure out set size for fully assoc
	 */
	if (type != 2)
		sets = cacheSize / (setSize * blockSize);
	else
		setSize = cacheSize / blockSize;

	cache = CreateCache(type, isWriteThrough, sets, setSize, blockSize);
	/* printf("Cache sets: %u, Cache setSize: %u\n", sets, setSize); */

	while (fgets(line, 200, fp) != NULL)
	{
		int isHit;
		if (strncmp(line, "#eof", 4) == 0)
			break;

		sscanf(line, "0x%8x: %c 0x%8x\n", &instruction, &readWrite, &memory);
		/* printf("%u - %u\n", instruction, memory); */

		/* read instruction */
		/*
		isHit = CacheCheck(cache, instruction);
		
		if (isHit == 1)
			hits++;
		else if (isHit == 0)
		{
			misses++;
			memReads++;
		}
		else
			printf("problem reading cache\n");
		*/

		/* read or write memory*/
		if (readWrite == 'r' || readWrite == 'R')
		{
			isHit = CacheCheck(cache, memory);
			if (isHit == 1)
				hits++;
			else if (isHit == 0)
			{
				misses++;
				memReads++;
			}
			else
				printf("problem reading cache\n");
		}
		else if (readWrite == 'w' || readWrite == 'W')
		{
			if (cache->isWriteThrough)
				memWrites++;

			isHit = CacheCheck(cache, memory);
			if (isHit == 1)
				hits++;
			else if (isHit == 0)
			{
				misses++;
				memReads++;
			}
			else
				printf("problem writing to cache/memory\n");
		}
		else
			printf("unknown action\n");
		/* printf("%u - %c - %u\n", instruction, readWrite, memory); */

	}
	printf("Memory reads: %u\nMemory Writes: %u\nCache hits: %u\nCache misses: %u\n",
		memReads, memWrites, hits, misses);
	/* printf("sets: %d set size: %d cache size: %d\n", sets, setSize, cacheSize); */

	DestroyCache(cache);

	fclose(fp);

	return 0;
}