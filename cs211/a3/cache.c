#include <stdlib.h>
#include <stdio.h>
#include "cache.h"

void showbits(unsigned int x)
{
	int i;
	for (i = (sizeof(char) * 8) - 1; i >= 0; i--)
	{
		if ((x >> i) & 1)
			printf("1");
		else
			printf("0");
	}
	printf(" ");
}

int fillBits(unsigned int num)
{
	int newNum = 0;
	while (num > 0)
	{
		newNum = newNum | (1 << (num - 1));
		num--;
	}
	return newNum;
}

int numBits(unsigned int num)
{
	int count = 0;
	if (num == 0)
		return 0;

	while (count < sizeof(unsigned int) * 8)
	{
		if ((num >> count) & 1)
			break;
		count++;
	}

	return count;
}

int LineFill(struct line *l, unsigned int tag)
{
	int overwrite = 0;

	if (l == NULL)
		return -1;

	if (!l->valid)
	{
		l->valid = 1;
		l->tag = tag;
	}
	else
	{
		l->tag = tag;
		overwrite = 1;
	}

	return overwrite;
}

/**
 * Returns 1 if line given is valid and tag is the same
 * returns 0 otherwise
 */
int LineCheck(struct line *l, unsigned int tag)
{
	int isHit = 0;

	if (l == NULL)
		return -1;

	/* if (l->valid && (l->tag <= tag && tag <= (l->tag + l->blockSize))) */
	if (l->valid && l->tag == tag)
	{
		/* printf("%09x is within %d of %x, diff: %u\n", l->tag, l->blockSize, tag, tag - l->tag); */
		isHit = 1;
	}
	/* else if (l->valid)
		printf("%u is not within %d of %u, diff: %u\n", l->tag, l->blockSize, tag, tag - l->tag); */

	return isHit;
}

int SetCheck(struct set *s, unsigned int tag)
{
	int isHit = 0;

	if (s->setSize == 1)	/* direct map cache */
	{
		if (LineCheck(s->lines[0], tag))
			isHit = 1;
		else
		{
			/* if (LineWrite(s->lines[0], tag))
				printf("overwrite\n"); */
			LineFill(s->lines[0], tag);
		}
	}
	else
	{
		/**
		 * fully associative cache or n-way associative
		 * caches work the same way within one set
		 */
		struct line *cursor = s->top;
		/* try to find in stack */
		for (; cursor != NULL; cursor = cursor->next)
		{
			if (LineCheck(cursor, tag))
			{
				isHit = 1;
				break;
			}
		}

		/* if the cursor is not null, then it is a hit */
		if (cursor != NULL)
		{
			/* if the cursor is already at the top of the stack, no need to move */
			if (cursor != s->top)
			{
				/* move the current line up to the top of the stack */
				/* making the surrounding lines skip over cursor */
				/* if (cursor->prev != NULL) */
					cursor->prev->next = cursor->next;

				if (cursor->next != NULL)
					cursor->next->prev = cursor->prev;

				if (cursor == s->bottom)
					s->bottom = cursor->prev;

				cursor->next = s->top;
				cursor->prev = s->top->prev;

				if (s->top->prev != NULL)
					s->top->prev->next = cursor;

				s->top->prev = cursor;
				s->top = cursor;
			}
		}
		else
		{
			/* printf("cursor is null for tag %u\n", tag); */
			if (s->top == s->bottom && !s->top->valid)
			{
				if (LineFill(s->top, tag) == -1)
					return -1;
			}
			else if (s->top->prev != NULL)
			{
				/* if can put more things at the top of the stack */
				cursor = s->top->prev;
				if (LineFill(cursor, tag) == -1)
					return -1;

				s->top = cursor;
			}
			else
			{
				/* if need to overwrite */
				cursor = s->bottom;
				if (LineFill(cursor, tag) == -1)
					return -1;

				s->bottom = cursor->prev;
				s->bottom->next = NULL;
				cursor->prev = NULL;
				cursor->next = s->top;
				s->top->prev = cursor;
				s->top = cursor;
			}
		}
	}
	return isHit;
}

int CacheCheck(Cache *c, const unsigned int addr)
{
	int isHit = 0;
	struct set *set = NULL;
	int setNum;
	unsigned int tag;

	/* printf("trying to read %u\n", addr); */

	if (c->type == 2)
		setNum = 0;	/* fully associative cache has only one set */
	else
		setNum = (addr >> c->offsetBits) & fillBits(c->setBits);

	if (setNum > c->numSets - 1)
	{
		printf("trying to access set %d out of %d, max:%d\n", setNum, c->numSets, fillBits(c->setBits));
		return -1;
	}
	/* printf("num sets: %u, set bits: %u\n", c->numSets, c->setBits); */
	set = c->sets[setNum];
	if (set == NULL)
		return -1;

	tag = (addr >> (c->offsetBits + c->setBits)) & fillBits(c->tagBits);
	/*
	printf("addr: %08x : ", addr);
	showbits(addr);
	printf(", tag: ");
	showbits(tag);
	printf("\ntagbits: %d\t setBits: %d\toffsetbits: %d\n", c->tagBits, c->setBits, c->offsetBits);
	*/
	isHit = SetCheck(set, tag);
	
	return isHit;
}

struct line *CreateLine()
{
	struct line *newLine = (struct line*)malloc(sizeof(struct line));
	newLine->valid = 0;
	newLine->tag = 0;
	/* newLine->dataBlock = (char*)calloc(blockSize, sizeof(char)); */
	/* newLine->blockSize = blockSize; */
	newLine->next = NULL;
	newLine->prev = NULL;
	
	return newLine;
}

struct set *CreateSet(unsigned int setSize)
{
	int i;
	struct set *newSet = (struct set*)malloc(sizeof(struct set));
	struct line *prev = NULL;
	newSet->setSize = setSize;
	newSet->top = NULL;
	newSet->bottom = NULL;
	newSet->lines = (struct line**)malloc(sizeof(struct line*) * setSize);

	for (i = 0; i < setSize; i++)
	{
		newSet->lines[i] = CreateLine();
		if (prev != NULL)
			prev->next = newSet->lines[i];

		newSet->lines[i]->prev = prev;
		prev = newSet->lines[i];
	}

	newSet->top = newSet->lines[setSize - 1];
	newSet->bottom = newSet->lines[setSize - 1];

	return newSet;
}

Cache *CreateCache(const char type, char isWriteThrough, unsigned int sets, unsigned int setSize, unsigned int blockSize)
{
	int i;
	Cache *newCache = (Cache*)malloc(sizeof(Cache));
	newCache->type = type;
	newCache->isWriteThrough = isWriteThrough;
	newCache->setBits = numBits(sets);
	/* printf("%d num bits: %d\n", sets, numBits(sets)); */
	newCache->offsetBits = numBits(blockSize);
	newCache->tagBits = 32 - newCache->setBits - newCache->offsetBits;
	newCache->numSets = sets;
	newCache->sets = (struct set**)malloc(sizeof(struct set*) * sets);
	for (i = 0; i < sets; i++)
		newCache->sets[i] = CreateSet(setSize);

	return newCache;
}

void DestroyLine(struct line *l)
{
	if (l == NULL)
		return;

	/* if (l->dataBlock != NULL)
		free(l->dataBlock); */

	free(l);
}

void DestroySet(struct set *s)
{
	if (s == NULL)
		return;

	if (s->lines != NULL)
	{
		int i;
		for (i = 0; i < s->setSize; i++)
			DestroyLine(s->lines[i]);
		free(s->lines);
	}

	free(s);
}

void DestroyCache(Cache *c)
{
	if (c == NULL)
		return;

	if (c->sets != NULL)
	{
		int i;
		for (i = 0; i < c->numSets; i++)
			DestroySet(c->sets[i]);
		free(c->sets);
	}
	free(c);
}

int isPower2(const unsigned int num)
{
	int isPower2 = 0;
	if (num == 1)
		isPower2 = 1;
	else
		isPower2 = !(num & (num - 1));

	return isPower2;
}
