/**
 * Date Created: 7/18/2014
 * Editors:
 * Software to be ran with: CasperJS
 * Description: Peapod DB collector
 */


//-- Creating a casperjs object--// 
var casper = require('casper').create(
{
    verbose: true,
    //logLevel: "debug",
    clientScripts: ['jquery-1.11.1.js', 'local.js']
});

var xp = require('casper').selectXPath;
var utils = require('utils');
var fs = require('fs');



var username = '';
var password = '';

//---------Check if there is a given url to access---------//
var mainurl = 'https://cas.rutgers.edu/login?service=https%3A%2F%2Fsims.rutgers.edu%2Fwebreg%2Fj_acegi_cas_security_check';
//var mainzip = '';
// if ((casper.cli.has("URL")) && (casper.cli.has("ZIP")))
// {
// 	mainurl = casper.cli.get("URL");
// 	mainzip = casper.cli.raw.get("ZIP");
// 	console.log("using: " + mainurl + ' ' + mainzip);
// } 
// else
// {
// 	casper.echo('please provide a url: --URL="http://www.example.com" and a zip --ZIP=08823');
// 	casper.exit();
// }


//---------Load the given url to casperjs---------//

casper.start(mainurl, function()
{
	this.echo("casper.start initiated");
});

casper.then(function() {
	credentials = this.evaluate(function(){
				    return [usernameX, passwordX];
				});
	username = credentials[0];
	password = credentials[1];
	this.echo('got cred.')
});


//---------set the size of the screen for the pictures that will be taken---------/
casper.viewport(1200,720);


//---------Retrives the console.log of the browser to our console---------/
var remoteMsgActive = true;
casper.on('remote.message', function(message){
	if (remoteMsgActive == true){
		console.log("------------------PhantomJS------------------");
	    console.log(message);
	    console.log("--------------------EndJS--------------------");
	}
});


//this part fills in the zipcode and clicks the button. This click somehow
//makes the server 'expect' you so that you dont get an error
casper.then(function(){	
	
	//enter in the zipcode
	this.fill('form[action="/login?service=https%3A%2F%2Fsims.rutgers.edu%2Fwebreg%2Fj_acegi_cas_security_check"]', {
        'username':    username,
        'password': password    
    }, false);
	
	//click the button
	this.click({
		type: 'xpath',
		path:'//*[@id="fm1"]/fieldset/div[5]/input[4]'
	});

	casper.echo('logged in');
		
});

casper.then(function(){	
	
	//click the button
	this.click({
		type: 'xpath',
		path:'//*[@id="submit"]'
	});

	casper.echo('select semester');
		
});

casper.then(function(){	
	
	// casper.open('https://sims.rutgers.edu/webreg/courseLookup.htm?execution=e3s1');
	//click the button
	this.click({
		type: 'xpath',
		path:'//*[@id="lookup"]/a'
	});

	casper.echo('view course list main');
		
});

casper.then(function(){	
	
	this.evaluate(function() {
		var elem = document.getElementById("linkid");
		if (typeof elem.onclick == "FormService.clickChildNode(this.parentNode)") {
			elem.onclick.apply(elem);
		}
		$(document).ready(function() {
		    //set initial state.
		    $('#textbox1').val($(this).is(':checked'));

		    $('#checkbox1').change(function() {
		        if($(this).is(":checked")) {
		            var returnVal = confirm("Are you sure?");
		            $(this).attr("checked", returnVal);
		        }
		        $('#textbox1').val($(this).is(':checked'));        
		    });
		});
	});
	//click the button
	// this.click({
	// 	type: 'xpath',
	// 	path:'//*[@id="locationsOnCampusList"]/li[1]/input'
	// });

	// this.click({
	// 	type: 'xpath',
	// 	path:'//*[@id="levelsList"]/li[1]/span'
	// });

	casper.echo('view course list');
		
});

casper.then(function() 
{
	
	this.capture('yay.png', 
			{
				top: 0,
				left: 0,
				width: 1200,
				height: 2000
			});

});



// var multiCity;
// var cities = [];
// //branch path, if zip immediately gives login page, proceed. Otherwise iterate the run for each town.
// //furthermore, the store may not exist in that area at all, in which case just exit.
// casper.then(function(){
// 	casper.withFrame('topZipEntry', function(){
		
// 		if (casper.exists(xp('//*[@id="newCust_getStarted"]/form/div[2]/input[1]')) == true){
// 		    if (this.getElementAttribute(xp('//*[@id="newCust_getStarted"]/form/div[2]/input[1]'), 'value') == "Submit Email"){
// 		        casper.exit();
// 		    }
// 		}
		
// 		if (casper.exists('#topZipSelectContainer') == true){
// 			console.log("--------------true--------------");
// 			multiCity = true;
// 			cities = this.evaluate(function(){
// 			    temp = $("#topZipSelectContainer li");
// 			    out = [];
// 			    for (n=0;n<temp.length;n++){
// 			        if (temp.eq(n).text() != "Choose a City"){
// 			            out.push(temp.eq(n).attr('id'));
// 			        }
// 			    }
// 			    return out;
// 			});
// 		}
// 		else{
// 			console.log("--------------false--------------");
// 			multiCity = false;
// 		}
		
// 	})
// })

// casper.then(function(){
//     if (multiCity == false){
//         readStore();
//     }else{
//         initHomePage();
//     }
// });

// //06905

// //"temporary" solution: do the same thing as before from the beginning, but with a specific city in mind (for multi city zip codes)
// //repeat until no cities remain
// function initHomePage(){
    
//     casper.then(function(){casper.open(mainurl)}); //open the homepage again, starting with a fresh slate
    
//     casper.then(function(){
	
// 	    //switch to zip frame
// 	    casper.withFrame('topZipEntry', function(){
		
// 		    //enter in the zipcode
// 		    this.fill("form[name='zipEntryForm']", {
// 	            'zipcode':    mainzip,
	            
// 	        }, false);	
		
// 		    //click the continue button		    
// 		    this.click("input[name='Continue']");	
// 		});
// 	});
	
// 	casper.then(function(){
// 	    casper.withFrame('topZipEntry', function(){
	        
		
// 	        this.click("#_input");
// 	        //now actually select the right city, and remove it from the queue. 
// 	        this.click("#"+cities.pop());
	        
// 	        //continue again, which should give us the corrrect session id. 
// 	        this.click("input[name='Continue']");
// 	    });
	    
// 	    //and init the huge main processing part
// 	    readStore();
// 	})
    
//     //THEN, ONCE A COMPLETE READ IS DONE...
    
//     //proceed back to the home page
//     casper.then(function(){	
// 	    casper.open(mainurl);
//     })
    
//     //click the button to let you select another city
//     casper.then(function(){	
// 	    casper.withFrame('frame4', function(){
// 		    this.click(xp('//*[@id="welcome"]/a[1]'));
// 	    })
//     })
    
//     casper.then(function(){
    
//         //If there are still some in the array remaining, re run this function.
//         if (cities.length != 0){
//             initHomePage();
//         }
    
//     });
// };


casper.run();

