import sys
import urllib2
import json
import time
from Emailcore import Emailcore
from ruclass import Section
from pprint import pprint

# py ru.py 198 17495 17496 05432 17492 08704 08723

email = Emailcore()
indexes = []
# need better performance
classes = {}

# for preventing spam
wasOpen = {}

# print sys.argv
done = False

def sendEmail(section):
	email.ConnectToServer()
	if section.isOpen == True:
		status = "OPEN"
	else:
		status = "CLOSED"
	email.ESSubject = email.defaultSubject + status + " -- " + str(section.name)
	email.ESBody = email.defaultBody + status \
		+ "\nFor index: " + str(section.indexNumber) \
		+ "\nof " + section.name + "\n" + section.time
	email.setEmailContent()
	email.SendEmail()

if len(sys.argv) >= 2:
	deptNum = sys.argv[1]
else:
	response = urllib2.urlopen('https://sis.rutgers.edu/soc/subjects.json?semester=92014&campus=NB&level=U')
	html = response.read()
	departmentJ = json.loads(html)
	for department in departmentJ:
		print department['description'], ":", department['code']

	deptNum = raw_input('Enter the department number: ')

if len(sys.argv) >= 3:
	indexes.append(sys.argv[2])
else:
	deptUrl = 'https://sis.rutgers.edu/soc/courses.json?subject=' \
		+ deptNum + '&semester=92014&campus=NB&level=U'
	response = urllib2.urlopen(deptUrl)
	html = response.read()
	classesJ = json.loads(html)
	pprint(classesJ[:2])
	quickDict = {}
	for i, classJ in enumerate(classesJ):
		quickDict[int(classJ['courseNumber'])] = i
		print classJ['title'], '\tcourse code:', classJ['courseNumber']
	courseNum = input('Enter the course code: ')

	wantedClass = classesJ[quickDict[courseNum]]

	print wantedClass['title'], len(wantedClass['sections']), 'sections'

	for section in wantedClass['sections']:
		print section['index'], "-- is open:", section['openStatus']
		for meetingTime in section['meetingTimes']:
			print '\t', meetingTime['meetingDay'] + ":\t", meetingTime['startTime'], '-', meetingTime['endTime']

	indexes.append(raw_input('Enter index number: '))

if len(sys.argv) > 3:
	#print len(sys.argv)
	for indexNum in sys.argv[2:]:
		indexes.append(indexNum)
		#print indexNum

for index in indexes:
	classes[index] = Section(index)
	wasOpen[index] = False

#time.sleep(25200)

while not done:
	try:
		deptUrl = 'https://sis.rutgers.edu/soc/courses.json?subject=' + deptNum + '&semester=92014&campus=NB&level=U'
		response = urllib2.urlopen(deptUrl)
		html = response.read()
		coursesJ = json.loads(html)
		Section.classesJ = coursesJ

		print

		for key in classes.keys():
			#print 'here'
			if classes[key].exists == False:
				classes[key].createPath()
			if classes[key].exists == True:
				print classes[key].indexNumber, classes[key].name,
				isOpen = classes[key].checkOpenStatus()
				print "\tOpen Status:", isOpen

				if isOpen != wasOpen[key]:
					# sendEmail(classes[key])
					wasOpen[key] = isOpen
	except:
		print "getting info failed"
	time.sleep(60)

