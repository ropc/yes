import sys
import urllib2
import json
import time
from Emailcore import Emailcore
from ruclass import Section
from pprint import pprint

# py ru.py 198 08704 17491 08723

email = Emailcore()
indexes = []
# need better performance
classes = {}

# for preventing spam
emailSent = {}

# print sys.argv
done = False

def sendEmail(wantedIndex):
	email.ConnectToServer()
	email.ESBody += "\nFor index: " + str(wantedIndex)
	email.setEmailContent()
	email.SendEmail()

if len(sys.argv) >= 2:
	deptNum = sys.argv[1]
else:
	response = urllib2.urlopen('https://sis.rutgers.edu/soc/subjects.json?semester=92014&campus=NB&level=U')
	html = response.read()
	departmentJ = json.loads(html)
	for department in departmentJ:
		print department['description'], ":", department['code']

	deptNum = raw_input('Enter the department number: ')

if len(sys.argv) >= 3:
	indexes.append(sys.argv[2])
else:
	deptUrl = 'https://sis.rutgers.edu/soc/courses.json?subject=' + deptNum + '&semester=92014&campus=NB&level=U'
	response = urllib2.urlopen(deptUrl)
	html = response.read()
	classesJ = json.loads(html)
	pprint(classesJ[:2])
	quickDict = {}
	for i, classJ in enumerate(classesJ):
		quickDict[int(classJ['courseNumber'])] = i
		print classJ['title'], '\tcourse code:', classJ['courseNumber']
	courseNum = input('Enter the course code: ')

	wantedClass = classesJ[quickDict[courseNum]]

	print wantedClass['title'], len(wantedClass['sections']), 'sections'

	for section in wantedClass['sections']:
		print section['index'], "-- is open:", section['openStatus']
		for meetingTime in section['meetingTimes']:
			print '\t', meetingTime['meetingDay'] + ":\t", meetingTime['startTime'], '-', meetingTime['endTime']

	indexes.append(raw_input('Enter index number: '))

if len(sys.argv) > 3:
	#print len(sys.argv)
	for indexNum in sys.argv[2:]:
		indexes.append(indexNum)
		#print indexNum

for index in indexes:
	classes[index] = Section(index)

#time.sleep(25200)

while not done:
	try:
		deptUrl = 'https://sis.rutgers.edu/soc/courses.json?subject=' + deptNum + '&semester=92014&campus=NB&level=U'
		response = urllib2.urlopen(deptUrl)
		html = response.read()
		coursesJ = json.loads(html)
		Section.classesJ = coursesJ

		print

		for key in classes.keys():
			#print 'here'
			if classes[key].exists == True:
				print classes[key].indexNumber,
				if classes[key].checkOpenStatus():
					print "\tOPEN"
					sendEmail(key)
				else:
					print "\tCLOSED"
			else:
				if classes[key].createPath():
					print classes[key].indexNumber,
					if classes[key].checkOpenStatus():
						print "\tOPEN"
						sendEmail(key)
					else:
						print "\tCLOSED"
				# else:
				# 	del classes[key]
		#	print classes


		# for classJ in classesJ:
		# 	for section in classJ['sections']:
		# 		for wantedIndex in indexes:
		# 			if section['index'] == wantedIndex:
		# 				print section['openStatus']
		# 				if section['openStatus'] == True:
		# 					print 'OPEN'
		# 					done = True
	except:
		print "getting info failed"
	time.sleep(60)

