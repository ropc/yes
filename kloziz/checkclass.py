#! /usr/bin/env python
import requests, time, sys, signal
from datetime import datetime
from pprint import pprint
# from marrow.mailer import Mailer, Message
from course import Course, Section
from Emailcore import Emailcore

def sigalrm_handler(signal, frame):
    print('alarm received!')

def getJson(**kwargs):
    if len(kwargs):
        payload = kwargs
    else:
        payload = {'subject': 198, 'semester': 92015}

    # these are pretty much constant
    payload['campus'] = 'NB'
    payload['level'] = 'U'

    deptUrl = 'https://sis.rutgers.edu/soc/courses.json'

    try:
        response = requests.get(deptUrl, params=payload)
        j = response.json()
    except KeyboardInterrupt:
        sys.exit()
    except:
        print("error connecting/ loading json")
        j = None

    return j

sections = {}

if len(sys.argv) > 1:
    courses = []
    for arg in sys.argv[1:]:
        info = arg.split(":")
        if len(info) == 3:
            sections[info[2]] = (Section(info[0], info[1], info[2]))
        elif len(info) == 2:
            courses.append(Course(info[0], info[1]))
        else:
            print("Could not parse {:}".format(arg))
else:
    courses = [Course(198, 206), Course(198, 314), Section(198, 314, '12850')]

memo = {}
# print(courses, courses[0].subject, courses[0].course)

# set the signal handler
signal.signal(signal.SIGALRM, sigalrm_handler)

while True:
    t = datetime.now()
    
    # figures out how much time it needs to wait until it should start
    # checking for classes being open again, since webreg opens and closes
    sec = (6 * 3600) + (26 * 60)
    if t.hour <= 6 and t.minute < 25:
        print('ohai')
        sec -= (t.hour * 3600) + (t.minute * 60) + t.second
    elif (t.weekday() == 5 or t.weekday() == 6) and (t.hour >= 18 and t.minute > 30):
        print('here')
        sec += ((23 - t.hour) * 3600) + ((59 - t.minute) * 60) + (59 - t.second)
    if sec != (6 * 3600) + (26 * 60):
        signal.alarm(sec)
        print('set alarm for {} hours {} min and {} sec from now ({}:{}:{}) at {}:{}'.format(
            sec//3600, (sec//60) % 60, sec % 60, (sec//3600 + t.hour) % 24,((sec//60) + t.minute) % 60,
            (sec + t.second) % 60), t.hour, t.minute)
        signal.pause()

    jclasses = None
    while jclasses is None:
        jclasses = getJson()

    # update time
    t = t.now()

    #pprint(jclasses[8])
    for course in courses:
        info = course.getInfo(jclasses)

        # import pprint
        # pprint.pprint(info['sections'])

        for section in course.sections.values():
            sections[section.index] = section

    for section in sections.values():
        section.getInfo(jclasses)
        # isDifferent = False
        if section.index in memo and section.isOpen != memo[section.index]:
            # isDifferent = True
            if section.isOpen:
                status = section.courseStr() + " OPEN"
            else:
                status = section.courseStr() + " CLOSED"
            email = Emailcore()
            email.ESBody += status + ' since ' + t
            email.ESSubject += status
            email.setEmailContent()
            email.ConnectToServer()
            email.SendEmail()

        memo[section.index] = section.isOpen

        print("{0} | {1.title} {1.index} {1.prof} {1.isOpen}".format(t, section))

    print("###########################")

    time.sleep(30)
