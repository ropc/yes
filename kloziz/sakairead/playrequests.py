import os.path, json, getpass
import requests, lxml, re
from bs4 import BeautifulSoup

if os.path.isfile('sakaiconfig.json'):
    with open('sakaiconfig.json', 'r') as f:
        config = json.load(f)
else:
    config = {}
    config['username'] = input("Your netID: ")
    config['password'] = getpass.getpass()
    config['numCourses'] = int(input("Number of Courses: "))
    with open('sakaiconfig.json', 'w') as f:
        json.dump(config, f)

# for semi-simplicity
def bs(arg):
    return BeautifulSoup(arg, 'lxml')


cas = 'https://cas.rutgers.edu/login'

loginurl = 'https://sakai.rutgers.edu/portal/xlogin'

s = requests.Session()
s.headers = {'User-Agent':'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_2_1 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148 Safari/6533.18.5'}
loginpage = s.get(loginurl, params={'force.login':'yes&pda'})

soup = bs(loginpage.content)
#print(soup.prettify())
data = {}
for field in soup.body.form.find_all():
    name = field.get('name')
    if name is not None:
        data[name] = field.get('value')

#data['username'] = 'rap256'
#data['password'] = 'Perucho2412'
data['eid'] = config['username']
data['pw'] = config['password']


#print(kwargs)
r = s.post(loginurl, data=data)

if not r.ok:
    raise NameError('Failed to post')

soup2 = bs(r.content)
#print(soup2.prettify())

sakaiurl = 'https://sakai.rutgers.edu/portal/site'
r = s.get(sakaiurl, data=data)
sakai = bs(r.content)
#print(sakai.prettify())
for link in sakai.find(id="pda-portlet-site-menu").find_all('a')[1:config['numCourses'] + 1]:
    #print(link.get('href'), link.get_text())
    print(link.get_text())
    page = s.get(link.get('href'))
    classPage = bs(page.content)
    assignUrl = classPage.find(id='pda-portlet-page-menu')
    assignUrl = assignUrl.find(class_=re.compile('assignment')).a.get('href')
    if assignUrl is not None:
        #print(assignUrl)
        req = s.get(assignUrl)
        if not r.ok:
            raise NameError('Failed to get assignment info')

        assignPage = bs(req.content)
        print(assignPage.table.find_all('tr'))
