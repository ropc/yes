class Semester:
	totalCredits = 66

	@classmethod
	def printFinalInfo(cls):
		if cls.totalCredits >= 120:
			str = "CAN graduate"
		else:
			str = "CANNOT graduate"

		print("\t\tTotal number of credits:", cls.totalCredits, str)

	def __init__(self, name):
		self.name = name
		self.courses = []
		self.credits = 0

	def addCourse(self, course):
		if isinstance(course, Course):
			self.courses.append(course)
			self.credits += course.credits
			Semester.totalCredits += course.credits

	def printInfo(self):
		print(self)
		for course in self.courses:
			print("\t" + course.__str__())

	def __str__(self):
		retstr = self.name + ": " + (str)(self.credits) + " credits"
		return retstr


class Course:
	def __init__(self, name, credits):
		self.name = name
		self.credits = credits

	def __str__(self):
		retstr = self.name + ": " + (str)(self.credits)
		return retstr

f14 = Semester("Fall 2014")
f14.addCourse(Course("Discrete 1", 4))
f14.addCourse(Course("Systems Programming", 4))
f14.addCourse(Course("Computer Architecture", 4))
f14.addCourse(Course("STS", 1))

f14.printInfo()
Semester.printFinalInfo()

s15 = Semester("Spring 2015")
s15.addCourse(Course("Discrete 2", 4))
s15.addCourse(Course("Principles of Programming Languages", 4))
s15.addCourse(Course("Internet Technology", 4))
s15.addCourse(Course("Software Methodology", 4))

s15.printInfo()
Semester.printFinalInfo()

f15 = Semester("Fall 2015")
f15.addCourse(Course("Algorithms", 4))
f15.addCourse(Course("Intro to AI", 4))
f15.addCourse(Course("Software Engineering", 4))
f15.addCourse(Course("Operating Systems Design", 4))

f15.printInfo()
Semester.printFinalInfo()

s16 = Semester("Spring 2016")
s16.addCourse(Course("Formal Langugaes and Automata", 4))
s16.addCourse(Course("some physics class?", 3))
s16.addCourse(Course("some philosophy class?", 3))
s16.addCourse(Course("some physics class?", 3))

s16	.printInfo()
Semester.printFinalInfo()
