#! /usr/bin/env python
import sys
from courses import Course, Section

for arg in sys.argv[1:]:
    info = arg.split(":")
    if len(info) == 3:
        object = Section(info[0], info[1], info[2])
    elif len(info) == 2:
        object = Course(info[0], info[1])
    else:
        print("Could not parse {:}".format(arg))
        continue
    print(arg, object)
