class Section(object):
	"""
	this Section object allows for interaction
	with each section of class independently
	"""
	# used to store the most up-to-date json object
	classesJ = None
	def __init__(self, newIndexNumber):
		self.indexNumber = newIndexNumber
		# pathTo is a tuple
		self.pathTo = ()
		#self.classesJ = newClassesJ
		self.exists = False
		#self.createPath()
		self.isOpen = False
		self.name = ''
		self.time = ''

	# this is terrible performance
	def createPath(self):
		self.exists = False
		for i, classJ in enumerate(self.classesJ):
			for j, section in enumerate(classJ['sections']):
				if section['index'] == self.indexNumber:
					self.pathTo = [i,j]
					self.exists = True
					self.name = classJ['subject'] + ':' + classJ['courseNumber'] \
						+ '\t' + classJ['title']
					for meetingTime in section['meetingTimes']:
						self.time += '\n' + str(meetingTime['meetingDay']) \
							+ ":\t" + str(meetingTime['startTime']) + ' - ' \
							+ str(meetingTime['endTime'])

		if self.exists == False:
			print "section could not be found"

		return self.exists

	def checkOpenStatus(self):
		if self.exists:
			try:
				section = self.classesJ[self.pathTo[0]]['sections'][self.pathTo[1]]
				if section['index'] == self.indexNumber:
					self.isOpen = section['openStatus']
					return self.isOpen
				else:
					print "index not found"
					self.createPath()
			except:
				# if it couldn't find it, recreate the path
				# then check if the class is open again
				self.createPath()
				self.checkOpenStatus()
		else:
			print "class does not exist"
			self.createPath()
