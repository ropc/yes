import signal
from datetime import datetime

def sigalrm_handler(signal, frame):
    print('alarm received!')

#print('starting')

#signal.signal(signal.SIGALRM, sigalrm_handler)

#print('signal handler set')

#signal.alarm(3)

#print('set alarm')

#signal.pause()


#t = datetime(2015, 1, 24, 15, 30, 44)
t = datetime.now()
print(t)
# figures out how much time it needs to wait until it should start
# checking for classes being open again, since webreg opens and closes
sec = (6 * 3600) + (26 * 60)
if t.hour <= 6 and t.minute < 25:
    print('ohai')
    sec -= (t.hour * 3600) + (t.minute * 60) + t.second
elif (t.weekday() == 5 or t.weekday() == 6) and (t.hour >= 18 and t.minute > 30):
    print('here')
    sec += ((23 - t.hour) * 3600) + ((59 - t.minute) * 60) + (59 - t.second)
if sec != (6 * 3600) + (26 * 60):
    signal.alarm(sec)
    print('set alarm for {} hours {} min and {} sec from now ({}:{}:{})'.format(
        sec//3600, (sec//60) % 60, sec % 60, (sec//3600 + t.hour) % 24, ((sec//60) + t.minute) % 60, (sec + t.second) % 60))
    signal.pause()